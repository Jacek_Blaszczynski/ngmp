/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// NUnit tests for NGmp.Math.FloatMP class.
	/// </summary>
	[TestFixture]
	public class TestFloatMP
	{
		public TestFloatMP()
		{
		}


		#region Constructors

		[Test]
		public void  ctor_Int32_A() 
		{
			FloatMP a = new FloatMP(Int32.MaxValue);
			Assert.AreEqual(Int32.MaxValue.ToString(), a.ToString(), "FloatMP#0001A: Constructor new FloatMP(int) failed.");
			a.Dispose();
		}

		[Test]
		public void  ctor_Int32_B() 
		{
			FloatMP a = new FloatMP(Int32.MinValue);
			Assert.AreEqual(Int32.MinValue.ToString(), a.ToString(), "FloatMP#0001B: Constructor new FloatMP(int) failed.");
			a.Dispose();
		}

		[Test]
		public void  ctor_UInt32_A() 
		{
			FloatMP a = new FloatMP(UInt32.MaxValue);
			Assert.AreEqual(UInt32.MaxValue.ToString(), a.ToString(), "FloatMP#0002A: Constructor new FloatMP(uint) failed.");
			a.Dispose();
		}

		[Test]
		public void  ctor_UInt32_B() 
		{
			FloatMP a = new FloatMP(1u);
			Assert.AreEqual(1.ToString(), a.ToString(), "FloatMP#0002B: Constructor new FloatMP(uint) failed.");
			a.Dispose();
		}

		[Test]
		public void ctor_UInt32_bool_A()
		{
			FloatMP a = new FloatMP(1024u, false);
			Assert.AreEqual(1024, a.Precision, "FloatMP#0006A: Constructor new FloatMP(uint) failed.");
			a.Dispose();

		}

		[Test]
		public void ctor_UInt32_bool_B()
		{
			FloatMP a = new FloatMP(1024u, true);
			Assert.AreEqual(1024, FloatMP.GetDefaultPrecision(), "FloatMP#0006Ba: Constructor new FloatMP(uint) failed.");
			Assert.AreEqual(1024, FloatMP.DefaultPrecision, "FloatMP#0006Bb: Constructor new FloatMP(uint) failed.");
			a.Dispose();
		}

		[Test]
		public void ctor_String() 
		{
			FloatMP a = new FloatMP("1234567890");
			FloatMP b = new FloatMP(1234567890);
			Assert.IsTrue(a == b, "FloatMP#0007A: Constructor new FloatMP(String) failed.");
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void ctor_String_B()
		{
			FloatMP.SetDefaultPrecision(2048);
			FloatMP a = new FloatMP("123456789012345678901234567890e1000000000");
			FloatMP b = new FloatMP(1234567890);
			Assert.IsTrue(a > b, "FloatMP#0007B: Constructor new FloatMP(String) failed.");
			Console.WriteLine("FloatMP a: " + a.ToString());
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void ctor_String_C()
		{
			FloatMP.SetDefaultPrecision(2048);
			FloatMP a = new FloatMP("-123456789012345678901234567890e-1000000000");
			FloatMP b = new FloatMP(1234567890);
			Assert.IsTrue(a < b, "FloatMP#0007C: Constructor new FloatMP(String) failed.");
			Console.WriteLine("FloatMP a: " + a.ToString());
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void ctor_String_D()
		{
			FloatMP.SetDefaultPrecision(2048);
			FloatMP a = new FloatMP("-123456789012345678901234567891e" + int.MinValue);
			FloatMP b = new FloatMP(1234567890);
			Console.WriteLine("FloatMP a: " + a.ToString() + " expected value: "
				+ "-0.123456789012345678901234567891e" + (int.MinValue + 30));
			Assert.IsTrue(a < b, "FloatMP#0007D: Constructor new FloatMP(String) failed.");
			Assert.AreEqual(a.ToString(), "-0.123456789012345678901234567891e" + (int.MinValue + 30).ToString());
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void ctor_String_E()
		{
			FloatMP.SetDefaultPrecision(2048);
			FloatMP a = new FloatMP("123456789012345678901234567891e" + (int.MaxValue - 30));
			FloatMP b = new FloatMP(1234567890);
			Console.WriteLine("FloatMP a: " + a.ToString() + " expected value: "
				+ "0.123456789012345678901234567891e" + (int.MaxValue));
			Assert.IsTrue(a > b, "FloatMP#0007E: Constructor new FloatMP(String) failed.");
			Assert.AreEqual(a.ToString(), "0.123456789012345678901234567891e" + int.MaxValue.ToString());
			a.Dispose();
			b.Dispose();

		}

		[Test]
		public void ctor_String_Int32_A() 
		{
			FloatMP a = new FloatMP("11111111111111111111111111111111", 2);
			FloatMP b = new FloatMP(uint.MaxValue);
			Assert.IsTrue(a == b, "FloatMP#0008A: Constructor new FloatMP(String) failed.");
			a.Dispose();
			b.Dispose();
		}

		[Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void ctor_String_Int32_B() 
		{
			FloatMP a = new FloatMP("11111111111111111111111111111111", -37);
			FloatMP b = new FloatMP(uint.MaxValue);
			Assert.IsTrue(a == b, "FloatMP#0008B: Constructor new FloatMP(String) failed.");
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void ctor_FloatMP()
		{
			FloatMP a = new FloatMP("-111111111111111111111111111111111111111111111111111111111e111000111", -2);
			FloatMP b = new FloatMP(a);
			Assert.IsTrue(a == b, "FloatMP#0009A: Constructor new FloatMP(FloatMP) failed.");
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void ctor_RatnlMP()
		{
			uint oldPrecision = FloatMP.DefaultPrecision;
			FloatMP.DefaultPrecision = 256;
			RatnlMP a = new RatnlMP("-11111111111111111111111111111111111111111111111111111111111111111111111");
			FloatMP b = new FloatMP(a);
			Assert.IsTrue(a == (RatnlMP) b, "FloatMP#0010A: Constructor new FloatMP(RatnlMP) failed.");
			a.Dispose();
			b.Dispose();
			FloatMP.DefaultPrecision = oldPrecision;
		}

		[Test]
		public void ctor_IntMP()
		{
			uint oldPrecision = FloatMP.DefaultPrecision;
			FloatMP.DefaultPrecision = 256;
			IntMP a = new IntMP("-11111111111111111111111111111111111111111111111111111111111111111111111");
			FloatMP b = new FloatMP(a);
			Assert.IsTrue(a == (IntMP) b, "FloatMP#0011A: Constructor new FloatMP(IntMP) failed.");
			a.Dispose();
			b.Dispose();
			FloatMP.DefaultPrecision = oldPrecision;
		}

		[Test]
		public void  ctor_Double_A() 
		{
			FloatMP a = new FloatMP(double.MinValue);

			Assert.IsTrue(double.MinValue == a, "FloatMP#005A: Constructor new FloatMP(double) failed.");

			a.Dispose();
		}

		[Test]
		public void  ctor_Double_B() 
		{
			FloatMP a = new FloatMP(double.MaxValue);

			Assert.IsTrue(double.MaxValue == a, "FloatMP#005B: Constructor new FloatMP(double) failed.");

			a.Dispose();
		}

		[Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void  ctor_Double_C() 
		{
			FloatMP a = new FloatMP(double.NaN);

			Assert.AreEqual(double.NaN.ToString() , a.ToString(), "FloatMP#005C: Constructor new FloatMP(double) failed.");

			a.Dispose();
		}

		[Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void  ctor_Double_D() 
		{
			FloatMP a = new FloatMP(float.NaN);

			Assert.AreEqual(double.NaN.ToString() , a.ToString(), "FloatMP#005D: Constructor new FloatMP(double) failed.");

			a.Dispose();
		}

		[Test, ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void  ctor_Double_E() 
		{
			FloatMP a = new FloatMP(float.NegativeInfinity);

			Assert.AreEqual(double.NaN.ToString() , a.ToString(), "FloatMP#005E: Constructor new FloatMP(double) failed.");

			a.Dispose();
		}


		#endregion Constructors

		#region Serialization
		[Test]
		public void SerializationBinary () 
		{
			FloatMP.SetDefaultPrecision(512);
			FloatMP a = new FloatMP (123.4567890e-54d);
			FileStream fs = new FileStream(Environment.CurrentDirectory + @"\TestFloatMP.bin", 
				FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			IFormatter format = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			try 
			{
				format.Serialize(fs, a);
				fs.Close();

				fs = new FileStream(Environment.CurrentDirectory + @"\TestFloatMP.bin", 
					FileMode.Open, FileAccess.Read, FileShare.None);

				FloatMP b = (FloatMP) format.Deserialize(fs);

				Assert.AreEqual (a.ToString(), b.ToString(), 
					"FloatMP#003: FloatMP Binary Serialization/Deserialization of 123.4567890e-54d failed.");
				a.Dispose();
				b.Dispose();
			}
			finally 
			{
				if (fs != null) fs.Close();
				if (File.Exists(Environment.CurrentDirectory + @"\TestFloatMP.bin"))
					File.Delete(Environment.CurrentDirectory + @"\TestFloatMP.bin");
			}		
		}

		[Test]
		public void SerializationXml() 
		{
			FloatMP a = new FloatMP (123.4567890e-54d);
			FileStream fs = new FileStream(Environment.CurrentDirectory + @"\TestFloatMP.xml", 
				FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			IFormatter format = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
			try 
			{
				format.Serialize(fs, a);
				fs.Close();

				fs = new FileStream(Environment.CurrentDirectory + @"\TestFloatMP.xml", 
					FileMode.Open, FileAccess.Read, FileShare.None);

				FloatMP b = (FloatMP) format.Deserialize(fs);

				
				Assert.AreEqual(a.ToString(), b.ToString(), 
					"FloatMP#003: FloatMP Xml Serialization/Deserialization of 123.4567890e-54d failed.");
#if CONSOLE
				if (a != b )
					Console.WriteLine("FloatMP xml serialization failed. {0} != {1}", a.ToString(), b.ToString());
#endif
				a.Dispose();
				b.Dispose();
			}
			finally 
			{
				if (fs != null) fs.Close();
				if (File.Exists(Environment.CurrentDirectory + @"\TestFloatMP.xml"))
					File.Delete(Environment.CurrentDirectory + @"\TestFloatMP.xml");
			}		
		}


		#endregion Serialization

		#region Operators

		[Test]
		public void Operator_Equal_A() 
		{
			FloatMP a = new FloatMP(100);
			FloatMP b = new FloatMP(100);

			Assert.IsTrue( a == b, "FloatMP#004A: FloatMP equal operator of 100 == 100 failed.");

			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void Operator_Equal_B() 
		{
			FloatMP a = new FloatMP(Int32.MinValue);
			FloatMP b = new FloatMP(Int32.MinValue);

			Assert.IsTrue( a == b, "FloatMP#004B: FloatMP equal operator of 100 == 100 failed.");

			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void Operator_Equal_C() 
		{
			FloatMP a = new FloatMP(double.MinValue);
			FloatMP b = new FloatMP(double.MinValue);

			Assert.IsTrue( a == b, "FloatMP#004C: FloatMP equal operator of 100 == 100 failed.");

			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void Operator_Equal_D() 
		{
			FloatMP a = new FloatMP(double.MinValue);
			double b = double.MinValue;

			Assert.IsTrue( a == b, "FloatMP#004D: FloatMP equal operator of double.MinValue == FloatMP(double.MinValue) failed.");

			a.Dispose();
		}

		[Test]
		public void Operator_Equal_E() 
		{
			FloatMP a = new FloatMP(double.MinValue);
			double b = double.NegativeInfinity;

			Assert.IsFalse( a == b, "FloatMP#004E: FloatMP equal operator of double.NegativeInfinity == FloatMP(double.MinValue) failed.");

			a.Dispose();
		}


		#endregion Operators
	}
}
