/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// Summary description for TestUtilities.
	/// </summary>
	[TestFixture]
	public class TestUtilities
	{
		public TestUtilities()
		{
		}

		[Test]
		public void BitsPerLimb() 
		{
			Assert.IsTrue( Utilities.BitsPerLimb == 32 || Utilities.BitsPerLimb == 64, 
				"Utilities#0001: Utilities.BitsPerLimb failed.");

		}

		[Test]
		public void GmpVersion() 
		{
			Assert.IsTrue( Utilities.GmpVersion.IndexOf("4.1") >= 0, 
				"Utilities#0001: Utilities.BitsPerLimb failed.");

		}

		[Test]
		public void GetProcessorFrequency() 
		{
			ulong freq = Utilities.GetProcessorFrequency(0);
#if CONSOLE
			Console.WriteLine("Processor frequency is {0}", freq);
#endif
			Assert.IsTrue( freq > 100000000 && freq < 10000000000U, 
				"Utilities#0001: Utilities.GetProcessorFrequncy(0) failed.");

		}

		[Test]
		public void GetProcessorTicks() 
		{
			ulong start, end;
			start = Utilities.GetProcessorTicks();
			end = Utilities.GetProcessorTicks();
#if CONSOLE
			Console.WriteLine("Processor ticks period is {0}", end - start);
#endif
			// Reflection used by nunit works really slow
			Assert.IsTrue( (end - start) > 15 && (end-start) < 1000000, 
				"Utilities#0004: Utilities.GetProcessorTicks() failed.");

		}

		[Test]
		public void GetProcessorTicksLow() 
		{
			ulong start, end;
			start = Utilities.GetProcessorTicksLow();
			end = Utilities.GetProcessorTicksLow();
#if CONSOLE
			Console.WriteLine("Processor ticks period is {0}", end - start);
#endif
			// Reflection used by nunit works really slow
			Assert.IsTrue( (end - start) > 15 && (end-start) < 1000000, 
				"Utilities#0004: Utilities.GetProcessorTicksLow() failed.");

		}

	}
}
