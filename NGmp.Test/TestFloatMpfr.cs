/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// Summary description for TestFloatMpfr.
	/// </summary>
	[TestFixture]
	public class TestFloatMpfr
	{
		public TestFloatMpfr() {}


		static TestFloatMpfr() 
		{
			FloatMpfr.DefaultPrecision = 1024;
		}

		#region Constructors and Dispose Methods

		[Test]
		public  void ctor () 
		{
			FloatMpfr a = new FloatMpfr();
			Assert.AreEqual(FloatMpfr.GetDouble(a, RoundMode.Nearest), Double.NaN, "FloatMpfr#0001: Constructor new FloatMpfr() failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int32_bool_A () 
		{
			FloatMpfr a = new FloatMpfr(100, false);
			Assert.AreEqual(FloatMpfr.GetPrecision(a), 100u, "FloatMpfr#0002: Constructor new FloatMpfr(int, bool) failed.");
			a.Dispose();
		}

		[Test]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public  void ctor_Int32_bool_B () 
		{
			FloatMpfr a = new FloatMpfr(-100, false);
			a.Dispose();
		}

		[Test]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public  void ctor_Int32_bool_C () 
		{
			FloatMpfr a = new FloatMpfr(1, false);
			a.Dispose();
		}

		[Test]
		public  void ctor_Int32_bool () 
		{
			uint precision = FloatMpfr.DefaultPrecision;
			FloatMpfr a = new FloatMpfr(100000000, true);
			Assert.AreEqual(FloatMpfr.GetDefaultPrecision(), 100000000u, "FloatMpfr#0003: Constructor new FloatMpfr(int, bool) failed.");
			FloatMpfr.DefaultPrecision = precision;
			a.Dispose();
		}

		[Test]
		public  void SetDefaultPrec () 
		{
			uint precision = FloatMpfr.DefaultPrecision;
			FloatMpfr.SetDafaultPrecision(628);
			Assert.AreEqual(FloatMpfr.GetDefaultPrecision(), 628u, "FloatMpfr#0004: Constructor new FloatMpfr(int, bool) failed.");
			FloatMpfr.SetDafaultPrecision(128);
			Assert.AreEqual(FloatMpfr.GetDefaultPrecision(), 128u, "FloatMpfr#0005: Constructor new FloatMpfr(int, bool) failed.");
			FloatMpfr.DefaultPrecision = precision;
		}

		[Test]
		public  void ctor_IntMP () 
		{
			IntMP a = new IntMP("123456789012345678901234567890123456789012345678901234567890");
			FloatMpfr b = new FloatMpfr(a);
			Assert.IsTrue(FloatMpfr.IsInteger(b), "FloatMpfr#0006: Constructor new FloatMpfr(int, bool) failed.");
			Assert.IsTrue( b == (FloatMpfr) a, "FloatMpfr#0007: Constructor new FloatMpfr(int, bool) failed.");
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public  void ctor_UInt32_A () 
		{
			FloatMpfr b = new FloatMpfr(UInt32.MaxValue);
			//Console.WriteLine("ctor_UInt32_A b: " + b.ToString(10) + " = " + "Uint32.MaxValue: " + uint.MaxValue);
			Assert.IsTrue(FloatMpfr.IsInteger(b), "FloatMpfr#0008: Constructor new FloatMpfr(int, bool) failed.");
			Assert.IsTrue( b == UInt32.MaxValue, "FloatMpfr#0008: Constructor new FloatMpfr(int, bool) failed.");
			b.Dispose();
		}

		[Test]
		public  void ctor_UInt32_B () 
		{
			FloatMpfr b = new FloatMpfr(UInt32.MinValue);
			//Console.WriteLine("ctor_UInt32_B b: " + b.ToString(10) + " = " + "Uint32.MinValue: " + uint.MinValue);
			Assert.IsTrue(FloatMpfr.IsInteger(b), "FloatMpfr#0008Ba: Constructor new FloatMpfr(int, bool) failed.");
			Assert.IsTrue( b == UInt32.MinValue, "FloatMpfr#0008Bb: Constructor new FloatMpfr(int, bool) failed.");
			b.Dispose();
		}

		[Test]
		public  void ctor_UInt64_A () 
		{
			FloatMpfr b = new FloatMpfr(ulong.MaxValue);
			//Console.WriteLine("ctor_UInt64_A b: " + b.ToString(10) + " = " + "Uint64.MaxValue: " + ulong.MaxValue);
			Assert.IsTrue(ulong.MaxValue == b, "FloatMpfr#0008C: Constructor new FloatMpfr(ulong) failed.");
			b.Dispose();
		}

		[Test]
		public  void ctor_String_A () 
		{
			FloatMpfr b = new FloatMpfr("-11111111111111111100000000000000000000000011111111111111111111111000000000000000000101e-1073000000", 2);
			FloatMpfr a = new FloatMpfr("-1111111111111111110000000000000000000000001111111111111111111111100000000000000000101e-1073000000", 2);
			//Console.WriteLine("ctor_String_B a: " + a.ToString(10));
			//Console.WriteLine("ctor_String_B b: " + b.ToString(10));
			Assert.IsTrue(a > b , "FloatMpfr#0009A: Constructor new FloatMpfr(uint) failed.");
			b.Dispose();
		}

		[Test]
		public  void ctor_String_B () 
		{
			// We need higher precision to test small differences on large mantissa
			FloatMpfr.SetDafaultPrecision(1024);
			int emax = FloatMpfr.MaxExponent;
			FloatMpfr g = new FloatMpfr("11111111111111111100000000000000000000000011111111111111111111111000000000000000001e" + (emax - 100), 2);
			FloatMpfr b = new FloatMpfr("11111111111111111100000000000000000000000011111111111111111111111000000000000000000e" + (emax - 100), 2);
			FloatMpfr a = new FloatMpfr("11111111111111111100000000000000000000000011111111111111111111111000000000000000000e" + (emax - 100), 2);
			Assert.IsTrue( a == b, "FloatMpfr#0009Ba: Constructor new FloatMpfr(String) failed.");
			//Console.WriteLine("ctor_String_B a: " + a.ToString(2));
			//Console.WriteLine("ctor_String_B b: " + b.ToString(10));
			//Console.WriteLine("ctor_String_B g: " + g.ToString(10));
			Assert.IsTrue(g > a, "FloatMpfr#0009Bb: Constructor new FloatMpfr(String) failed.");
			Assert.IsTrue( b < g , "FloatMpfr#0009Bc: Constructor new FloatMpfr(String) failed.");
			
			b.Dispose();
			a.Dispose();
			//f.Dispose();
			g.Dispose();
		}


		#endregion Constructors and Dispose Methods

	}
}
