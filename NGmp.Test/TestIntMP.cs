/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Remoting;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// NUnit tests for NGmp.Math.IntMP class.
	/// </summary>
	[TestFixture]
	public class TestIntMP
	{
		public TestIntMP()
		{

		}


		#region Constructor Tests

		[Test]
		public  void ctor() 
		{
			IntMP a = new IntMP();
			Assert.AreEqual("0", a.ToString(), "IntMP#0001: Constructor new IntMP() failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int32_A() 
		{
			IntMP a = new IntMP(Int32.MinValue);
			Assert.AreEqual(Int32.MinValue.ToString(), a.ToString(), "IntMP#0002: Constructor new IntMP(int) Int32.MinValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int32_B() 
		{
			IntMP a = new IntMP(Int32.MaxValue);
			Assert.AreEqual(Int32.MaxValue.ToString(), a.ToString(), "IntMP#0003: Constructor new IntMP(int) Int32.MaxValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int32_C() 
		{
			IntMP a = new IntMP(0);
			Assert.AreEqual(0.ToString(), a.ToString(), "IntMP#0004: Constructor new IntMP(int) 0 failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int32_D() 
		{
			IntMP t = new IntMP(Int32.MinValue);
			IntMP a = new IntMP(t);
			Assert.AreEqual(t.ToString(), a.ToString(), "IntMP#0005: Constructor new IntMP(IntMP) 0 failed.");
			t.Dispose();
			a.Dispose();
		}

		[Test]
		public  void ctor_String() 
		{
			IntMP t = new IntMP("-12345678901234567890123456789012345678900000000000000000000000000000000000000000");
			IntMP a = new IntMP(t);
			Assert.AreEqual(t.ToString(), a.ToString(), "IntMP#0006: Constructor new IntMP(IntMP) 0 failed.");
			t.Dispose();
			a.Dispose();
		}

		[Test]
		public  void ctor_RatnlMP() 
		{
			RatnlMP r = new RatnlMP(123456789, 1);
			IntMP a = new IntMP(r);
			Assert.AreEqual("123456789", a.ToString(), "IntMP#0007: Constructor new IntMP(RatnlMP) xxx failed.");
			a.Dispose();
			r.Dispose();
		}

		[Test]
		public  void ctor_FloatMP()
		{
			FloatMP f = new FloatMP(123456789);
			IntMP a = new IntMP(f);
			Assert.AreEqual(f.ToString(), a.ToString(), "IntMP#0008: Constructor new IntMP(FloatMpfr) xxx failed.");
			a.Dispose();
			f.Dispose();
		}

		[Test]
		public  void ctor_Uint32_A()
		{
			IntMP a = new IntMP(UInt32.MaxValue);
			Assert.AreEqual(a.ToString(), UInt32.MaxValue.ToString(), "IntMP#0009: Constructor new IntMP(uint) UInt32.MaxValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Uint32_B()
		{
			IntMP a = new IntMP(UInt32.MinValue);
			Assert.AreEqual(a.ToString(), UInt32.MinValue.ToString(), "IntMP#0010: Constructor new IntMP(uint) UInt32.MinValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Double_A()
		{
			IntMP a = new IntMP(Double.MaxValue);
			Assert.AreEqual(Double.MaxValue, ConvertMP.ToDouble(a), "IntMP#0011A: Constructor new IntMP(double) 1.2345678901234e20d failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Double_B()
		{
			IntMP a = new IntMP(Double.MinValue);
			Assert.AreEqual(Double.MinValue, ConvertMP.ToDouble(a), "IntMP#0011B: Constructor new IntMP(double) 1.2345678901234e20d failed.");
			a.Dispose();
		}

		[Test] [ExpectedException(typeof(OverflowException))]
		public  void ctor_Double_C()
		{
			IntMP a = new IntMP(Double.NegativeInfinity);
			Assert.AreEqual(Double.NegativeInfinity, ConvertMP.ToDouble(a), "IntMP#0011C: Constructor new IntMP(double) 1.2345678901234e20d failed.");
			a.Dispose();
		}

		[Test] [ExpectedException(typeof(OverflowException))]
		public  void ctor_Double_D()
		{
			IntMP a = new IntMP(Double.NaN);
			Assert.AreEqual(Double.NaN, ConvertMP.ToDouble(a), "IntMP#0011D: Constructor new IntMP(double) 1.2345678901234e20d failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Double_E()
		{
			IntMP a = new IntMP(0.0e10d);
			Assert.AreEqual(0.0e10d, ConvertMP.ToDouble(a), "IntMP#0011E: Constructor new IntMP(double) 1.2345678901234e20d failed.");
			a.Dispose();
		}
		
		[Test]
		public  void ctor_String_0011()
		{
			IntMP a = new IntMP("1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000");
			Assert.AreEqual(a.ToString(), "1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000",
				"IntMP#0012: Constructor new IntMP(string) 1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000 failed.");
			a.Dispose();
		}
		
		[Test]
		public  void ctor_String_Int32_A()
		{
			IntMP a = new IntMP("1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000", 2);
			Assert.AreEqual(a.ToString(2), "1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000",
				"IntMP#0013: Constructor new IntMP(string, int) 1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000 failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_String_Int32_B()
		{
			IntMP a = new IntMP("-1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000", 2);
			Assert.AreEqual(a.ToString(2), "-1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000",
				"IntMP#0014: Constructor new IntMP(string, int) -1111111111111111111111111111111111111111100000000000000000000000000111111111111111111110000000 failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_UInt64_A()
		{
			IntMP a = new IntMP(UInt64.MaxValue);
			Assert.AreEqual(a.ToString(), UInt64.MaxValue.ToString(),
				"IntMP#0015: Constructor new IntMP(ulong) UInt64.MaxValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_UInt64_B()
		{
			IntMP a = new IntMP(UInt64.MinValue);
			Assert.AreEqual(a.ToString(), UInt64.MinValue.ToString(),
				"IntMP#0016: Constructor new IntMP(ulong) UInt64.MinValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int64_A()
		{
			IntMP a = new IntMP(Int64.MinValue);
			Assert.AreEqual(a.ToString(), Int64.MinValue.ToString(),
				"IntMP#0017: Constructor new IntMP(long) Int64.MinValue failed.");
			a.Dispose();
		}

		[Test]
		public  void ctor_Int64_B()
		{
			IntMP a = new IntMP(Int64.MaxValue);
			Assert.AreEqual(a.ToString(), Int64.MaxValue.ToString(),
				"IntMP#0015: Constructor new IntMP(long) Int64.MaxValue failed.");
			a.Dispose();
		}


		#endregion Constructor Tests

		#region Constructor/Serialization/Remoting
		[Test]
		public  void SerializationRemoting()
		{
			AppDomain app = null;
			try
			{
				app = AppDomain.CreateDomain("RemoteMath");
				IntMP a = new IntMP("123456789012345678901234567890");
				object[] args = new object[1];
				args[0] = (object)a;
				
				// Works only when NGmp is installed in GAC or NUnit working directory 
				// as usual NUnit assembly search path does not include path to test project locations
				ObjectHandle handle = app.CreateInstance(
					"NGmp, Version=0.5.0.10, Culture=neutral, PublicKeyToken=7d2a15aba6177132",
					"NGmp.Math.IntMP", false,
					BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public,
					null, args, null, null, null);
				IntMP b = (IntMP)handle.Unwrap();

				// This implementation does not work despite that it should simply encapsulate 
				// above calls to CreateInstance and Unwrap !!!!!!

				//IntMP b = (IntMP)app.CreateInstanceFromAndUnwrap(
				//    "NGmp, Version=0.5.0.10, Culture=neutral, PublicKeyToken=7d2a15aba6177132",
				//    "NGmp.Math.IntMP", false,
				//    BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public,
				//    null, args, null, null, null);

				Console.WriteLine("IntMP remoting test: a {0} == b {1}", a, b);
				Assert.AreEqual(a.ToString(), b.ToString(),
					"IntMP#0016: Constructor new IntMP(SerializationInfo, StreamingContext) 123456789012345678901234567890 failed.");
				a.Dispose();
				b.Dispose();
			}
			finally
			{
				if (app != null) AppDomain.Unload(app);
			}
			
		}

		[Test]
		public  void SerializationBinary() 
		{
			IntMP a = new IntMP ("-123456789012345678901234567890");
			FileStream fs = new FileStream(Environment.CurrentDirectory + @"\testfile.bin", 
				FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			IFormatter format = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			try 
			{
				format.Serialize(fs, a);
				fs.Close();

				fs = new FileStream(Environment.CurrentDirectory + @"\testfile.bin", 
					FileMode.Open, FileAccess.Read, FileShare.None);

				IntMP b = (IntMP) format.Deserialize(fs);

				Assert.AreEqual (a.ToString(), b.ToString(), 
					"IntMP#0017: Serialization/Deserialization of IntMP(SerializationInfo, StreamingContext) -123456789012345678901234567890 failed.");
				a.Dispose();
				b.Dispose();
			}
			finally 
			{
				if (fs != null) fs.Close();
				if (File.Exists(Environment.CurrentDirectory + @"\testfile.bin"))
					File.Delete(Environment.CurrentDirectory + @"\testfile.bin");
			}
		}

		[Test]
		public  void SerializationXml() 
		{
			IntMP a = new IntMP ("-1234567890123456789012345678901234567890123456789012345678901234567890");
			FileStream fs = new FileStream(Environment.CurrentDirectory + @"\testfile.xml", 
				FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			IFormatter format = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
			try 
			{
				format.Serialize(fs, a);
				fs.Close();

				fs = new FileStream(Environment.CurrentDirectory + @"\testfile.xml", 
					FileMode.Open, FileAccess.Read, FileShare.None);

				IntMP b = (IntMP) format.Deserialize(fs);

				Assert.AreEqual (a.ToString(), b.ToString(), 
					"IntMP#0018: XML Serialization/Deserialization of IntMP(SerializationInfo, StreamingContext) -123456789012345678901234567890 failed.");
				a.Dispose();
				b.Dispose();
			}
			finally 
			{
				if (fs != null) fs.Close();
				if (File.Exists(Environment.CurrentDirectory + @"\testfile.xml"))
					File.Delete(Environment.CurrentDirectory + @"\testfile.xml");
			}
		}


		#endregion Constructor/Serialization/Remoting

		#region Dispose
		[Test]
		public  void Disposed_A()
		{
			IntMP a = new IntMP("1234567890123456789012345678901234567890");
			a.Dispose();

			Assert.IsTrue (a.IsDisposed, 
				"IntMP#0019: IntMP.Dispose() failed.");
			Assert.AreEqual(IntPtr.Zero, a.MPNumber, "IntMP#0020: IntMP.Dispose() failed.");
		}

		[Test][ExpectedException(typeof(AccessViolationException))]
		public  void Disposed_B()
		{
			IntMP a = new IntMP("1234567890123456789012345678901234567890");
			a.Dispose();
			IntMP b = new IntMP(a);
		}

		#endregion Dispose

		#region Object Overrides

		[Test][ExpectedException(typeof(ArgumentException))]
		public  void Equals_A() 
		{
			IntMP a = new IntMP("12345678901234567890");
			Int32[] i = new int[] {0,1,2};
			a.Equals((object) i);
			a.Dispose();
		}

		[Test]
		public  void Equals_B() 
		{
			IntMP a = new IntMP("12345678901234567890");
			IntMP b = new IntMP("111111111111111111111111111111111111111111111");
			Assert.IsFalse(a.Equals(b), "IntMP#0021: IntMP.Equals() failed." );
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public  void Equals_C() 
		{
			IntMP a = new IntMP("111111111111111111111111111111111111111111111");
			IntMP b = new IntMP("111111111111111111111111111111111111111111111");
			Assert.IsTrue(a.Equals(b), "IntMP#0022: IntMP.Equals() failed." );
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public void GetHashCode_A() 
		{
			IntMP a = new IntMP("111111111111111111111111111111111111111111111");
			IntMP b = new IntMP("111111111111111111111111111111111111111111111");
			Assert.AreEqual(a.GetHashCode(), b.GetHashCode(), "IntMP#0023: IntMP.GetHashCode() failed." );
			a.Dispose();
			b.Dispose();
		}

		[Test]
		public  void GetHashCode_B() 
		{
			IntMP a = new IntMP("111111111111111111111111111111111111111111111");
			IntMP b = new IntMP("11111111111111111111111111111111111111111111");
			Assert.IsTrue((a.GetHashCode() != b.GetHashCode()), "IntMP#0024: IntMP.GetHashCode() failed." );
			a.Dispose();
			b.Dispose();
		}


		[Test]
		public  void ToString_A() 
		{
			IntMP a = new IntMP("111111111111111111111111111111111111111111111", 2);
			Assert.IsTrue((a.ToString() != a.ToString(2)), "IntMP#0025: IntMP.ToString() failed.");
			Assert.IsTrue((a.ToString(2) == a.ToString(2)), "IntMP#0026: IntMP.ToString(int) failed.");
			Assert.IsTrue((a.ToString(2) == "111111111111111111111111111111111111111111111"), "IntMP#0028: IntMP.ToString(int) failed.");
			a.Dispose();
		}

		[Test]
		public void ToString_B()
		{
			IntMP a = new IntMP("111111111111111111111111111111111111111111111", 10);
			//Console.WriteLine("ToString_B a.ToString(): " + a.ToString());
			//Console.WriteLine("ToString_B a.ToString(2): " + a.ToString(2));
			//Console.WriteLine("ToString_B a.ToString(10): " + a.ToString(10) + " original string : " + "111111111111111111111111111111111111111111111");
			Assert.IsTrue((a.ToString() != a.ToString(2)), "IntMP#0025b: IntMP.ToString() failed.");
			Assert.IsTrue((a.ToString(2) == a.ToString(2)), "IntMP#0026b: IntMP.ToString(int) failed.");
			Assert.IsFalse((a.ToString(2) == "111111111111111111111111111111111111111111111"), "IntMP#0028b: IntMP.ToString(int) failed.");
			Assert.IsTrue((a.ToString(10) == "111111111111111111111111111111111111111111111"), "IntMP#0028ba: IntMP.ToString(int) failed.");
			a.Dispose();
		}

		#endregion Object Overrides

		#region IComparable

		[Test]
		public  void CompareTo_A() 
		{
			IntMP a = new IntMP("111111111111111111111111111111111111111111111");
			IntMP b = new IntMP("11111111111111111111111111111111111111111111");
			IntMP c = new IntMP("1111111111111111111111111111111111111111111");
			ArrayList list = new ArrayList();
			list.Add(a);
			list.Add(b);
			list.Add(c);
			Assert.IsTrue((((IntMP)list[0]).ToString() == a.ToString()), "IntMP#0027: IntMP.CompareTo() failed." );
			list.Sort();
			Assert.IsTrue((((IntMP)list[0]).ToString() == c.ToString()), "IntMP#0028: IntMP.CompareTo() failed." );
			a.Dispose();
			b.Dispose();
			c.Dispose();
		}

		[Test]
		public  void CompareTo_B() 
		{
			IntMP a = new IntMP("1234");
			Assert.IsTrue( a.CompareTo(1234) == 0, "IntMP#0031: IntMP.CompareTo() failed." );
			Assert.IsTrue( a.CompareTo(12345) < 0, "IntMP#0032: IntMP.CompareTo() failed." );
			Assert.IsTrue( a.CompareTo(123) > 0, "IntMP#0033: IntMP.CompareTo() failed." );
			a.Dispose();
		}

		#endregion IComparable

		#region ICloneable

		[Test]
		public  void Clone() 
		{
			IntMP a = new IntMP("1234567890123456789012345678901234567890");
			IntMP b = (IntMP) a.Clone();
			Assert.IsTrue( a.ToString() == b.ToString(), "IntMP#0029: IntMP.Clone() failed." );
			Assert.IsTrue((object) a != (object) b, "IntMP#0030: IntMP.Clone() failed." );
		}

		#endregion ICloneable

		#region Statics

		#region Miscellaneous

		[Test]
		public  void Odd()
		{
			IntMP a = new IntMP("12345678901234567890123456789012345678901");
			Assert.IsTrue(IntMP.Odd(a),"IntMP#0035: IntMP.Odd(IntMP) failed."); 
		}

		[Test]
		public  void Even()
		{
			IntMP a = new IntMP("12345678901234567890123456789012345678902");
			Assert.IsTrue(IntMP.Even(a),"IntMP#0036: IntMP.Even(IntMP) failed.");
		}

		#endregion Miscallenous


		#endregion Statics

		
	}
}
