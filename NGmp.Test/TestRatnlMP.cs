/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Remoting;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// NUnit tests for NGmp.Math.RatnlMP class.
	/// </summary>
	[TestFixture]
	public class TestRatnlMP
	{
		public TestRatnlMP()
		{
		}

		#region Constructors

		[Test]
		public void  ctor() 
		{
			RatnlMP a = new RatnlMP();
			Assert.AreEqual("1", a.Denominator.ToString(), "RatnlMP#0001A: Constructor new RatnlMP() failed.");
			Assert.AreEqual("0", a.Numerator.ToString(), "RatnlMP#0001B: Constructor new RatnlMP() failed.");
			a.Dispose();
		}

		[Test]
		public void  ctor_Int32() 
		{
			RatnlMP a = new RatnlMP(Int32.MinValue,3);
			Assert.AreEqual("3", a.Denominator.ToString(), "RatnlMP#0002A: Constructor new RatnlMP(int,int) failed.");
			Assert.AreEqual(Int32.MinValue.ToString(), a.Numerator.ToString(), "RatnlMP#0002B: Constructor new RatnlMP(int,int) failed.");
			a.Dispose();
		}

		[Test]
		public void  ctor_UInt32() 
		{
			RatnlMP a = new RatnlMP(UInt32.MaxValue,2);
			Assert.AreEqual("2", a.Denominator.ToString(), "RatnlMP#0003A: Constructor new RatnlMP(uint,uint) failed.");
			Assert.AreEqual(UInt32.MaxValue.ToString(), a.Numerator.ToString(), "RatnlMP#0003B: Constructor new RatnlMP(uint,uint) failed.");
			a.Dispose();
		}

		#endregion Constructors

		#region Serialization

		[Test]
		public void SerializationBinary() 
		{
			RatnlMP a = new RatnlMP ("-12345678901234567890123456789/31");
			FileStream fs = new FileStream(Environment.CurrentDirectory + @"\testfile.bin", 
				FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			IFormatter format = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
			try 
			{
				format.Serialize(fs, a);
				fs.Close();

				fs = new FileStream(Environment.CurrentDirectory + @"\testfile.bin", 
					FileMode.Open, FileAccess.Read, FileShare.None);

				RatnlMP b = (RatnlMP) format.Deserialize(fs);
#if CONSOLE
				Console.WriteLine("Test for RatnlMP binary serialization:\nexpected: {0}\nactual:{1}", a.ToString(), b.ToString());
#endif
				Assert.IsTrue (a == b , 
					"IntMP#0017: Serialization/Deserialization of IntMP(SerializationInfo, StreamingContext) -123456789012345678901234567890 failed.");
				a.Dispose();
				b.Dispose();
			}
			finally 
			{
				if (fs != null) fs.Close();
				if (File.Exists(Environment.CurrentDirectory + @"\testfile.bin"))
					File.Delete(Environment.CurrentDirectory + @"\testfile.bin");
			}
		}

		[Test]
		public  void SerializationXml() 
		{
			RatnlMP a = new RatnlMP ("-1234567890123456789012345678901234567890123456789012345678901234567890/31");
			FileStream fs = new FileStream(Environment.CurrentDirectory + @"\testfile.xml", 
				FileMode.Create, FileAccess.ReadWrite, FileShare.None);
			IFormatter format = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
			try 
			{
				format.Serialize(fs, a);
				fs.Close();

				fs = new FileStream(Environment.CurrentDirectory + @"\testfile.xml", 
					FileMode.Open, FileAccess.Read, FileShare.None);

				RatnlMP b = (RatnlMP) format.Deserialize(fs);
#if CONSOLE
				Console.WriteLine("Test for RatnlMP xml serialization:\nexpected: {0}\nactual:{1}", a.ToString(), b.ToString());
#endif
				Assert.IsTrue (a == b, 
					"RatnlMP#0018: XML Serialization/Deserialization of RatnlMP(SerializationInfo, StreamingContext) -123456789012345678901234567890 failed.");
				a.Dispose();
				b.Dispose();
			}
			finally 
			{
				if (fs != null) fs.Close();
				if (File.Exists(Environment.CurrentDirectory + @"\testfile.xml"))
					File.Delete(Environment.CurrentDirectory + @"\testfile.xml");
			}
		}

		[Test]
		public void SerializationRemoting() 
		{
			AppDomain app = null;
			try
			{
				app = AppDomain.CreateDomain("RemoteMath");
				RatnlMP a = new RatnlMP("123456789012345678901234567890/7", false);
				object[] args = new object[1];
				args[0] = (object)a;

				// Works only when NGmp is installed in GAC or NUnit working directory 
				// as usual NUnit assembly search path does not include path to test project locations
				ObjectHandle handle = app.CreateInstance(
					"NGmp, Version=0.5.0.10, Culture=neutral, PublicKeyToken=7d2a15aba6177132",
					"NGmp.Math.RatnlMP", false,
					BindingFlags.CreateInstance | BindingFlags.Instance | BindingFlags.Public,
					null, args, null, null, null);
				RatnlMP b = (RatnlMP)handle.Unwrap();

				Console.WriteLine("RatnlMP remoting test: a {0} == b {1}", a, b);
				Assert.AreEqual(a.ToString(), b.ToString(), "RatnlMP#0019: Remoting test failed.");
				a.Dispose();
				b.Dispose();
			}
			finally
			{
				if (app != null) AppDomain.Unload(app);
			}
		}


		#endregion Serialization
	}
}
