/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// Summary description for TestRandomMP.
	/// </summary>
	[TestFixture]
	public class TestRandomMP
	{
		public TestRandomMP()
		{
			//
			// TODO: Add constructor logic here
			//
		}


		[Test]
		public void  ctor() 
		{
			RandomMP rand = new RandomMP();
			IntMP intr1 = rand.Next(1000);
			IntMP intr2 = rand.Next(1000);
			Assert.IsTrue (intr1 != intr2, "RandomMP#0001: Constructor new RandomMP() failed.");
#if CONSOLE
			Console.WriteLine("First random IntMP: \t{0}", intr1.ToString());
			Console.WriteLine("Second random IntMP:\t{0}", intr2.ToString());
#endif
			rand.Dispose();
			intr1.Dispose();
			intr2.Dispose();

		}

		[Test]
		public void  ctor_Uint() 
		{
			RandomMP rand = new RandomMP(128);
			IntMP intr1 = rand.Next(1000);
			IntMP intr2 = rand.Next(1000);
			Assert.IsTrue (intr1 != intr2, "RandomMP#0002: Constructor new RandomMP(uint) failed.");
#if CONSOLE			
			Console.WriteLine("First random IntMP: \t{0}", intr1.ToString());
			Console.WriteLine("Second random IntMP:\t{0}", intr2.ToString());
#endif
			rand.Dispose();
			intr1.Dispose();
			intr2.Dispose();

		}

		[Test]
		public void  ctor_Uint_IntMP() 
		{
			RandomMP rand = new RandomMP(128, new IntMP(DateTime.Now.Ticks));
			IntMP intr1 = rand.Next(1000);
			IntMP intr2 = rand.Next(1000);
			Assert.IsTrue (intr1 != intr2, "RandomMP#0003: Constructor new RandomMP(uint, IntMP) failed.");
#if CONSOLE
			Console.WriteLine("First random IntMP: \t{0}", intr1.ToString());
			Console.WriteLine("Second random IntMP:\t{0}", intr2.ToString());
#endif
			rand.Dispose();
			intr1.Dispose();
			intr2.Dispose();

		}

		[Test]
		public void  NextOnesAndZeros_UInt() 
		{
			RandomMP rand = new RandomMP(128, new IntMP(DateTime.Now.Ticks));
			IntMP intr1 = rand.NextOnesAndZeros(1000);
			IntMP intr2 = rand.NextOnesAndZeros(1000);
			Assert.IsTrue (intr1 != intr2, "RandomMP#0003: Constructor new RandomMP(uint, IntMP) failed.");
#if CONSOLE
			Console.WriteLine("First 1 & 0 IntMP: \t{0}", intr1.ToString(2));
			Console.WriteLine("Second 1 & 0 IntMP:\t{0}", intr2.ToString(2));
#endif
			rand.Dispose();
			intr1.Dispose();
			intr2.Dispose();

		}

		[Test]
		public void  NextFloatMP_UInt()
		{
			FloatMP.SetDefaultPrecision(1024);
			RandomMP rand = new RandomMP(128, new IntMP(DateTime.Now.Ticks));
			FloatMP intr1 = rand.NextFloatMP(1024);
			FloatMP intr2 = rand.NextFloatMP(1024);
			Assert.IsTrue (intr1 != intr2, "RandomMP#0004: Constructor new RandomMP(uint, IntMP) and successive call to NextFloatMP(uint) failed.");
#if CONSOLE
			Console.WriteLine("First random FloatMP: \t{0}", intr1.ToString());
			Console.WriteLine("Second random FloatMP:\t{0}", intr2.ToString());
#endif
			rand.Dispose();
			intr1.Dispose();
			intr2.Dispose();

		}

	}
}
