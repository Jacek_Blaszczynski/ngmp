/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using NGmp.Math;
using NUnit.Framework;

namespace Test.NGmp.Math
{
	/// <summary>
	/// Summary description for TestConvertMP.
	/// </summary>
	[TestFixture]
	public class TestConvertMP
	{
		public TestConvertMP()
		{
		}

		[Test]
		public void ToBoolean_IntMP()
		{
			IntMP a = new IntMP(-10);
			IntMP b = new IntMP();

			bool ab = ConvertMP.ToBoolean(a);
			bool bb = ConvertMP.ToBoolean(b);
			Assert.AreEqual(true, ab, "ConvertMP#0001A: ToBoolean(IntMP) failed.");
			Assert.AreEqual(false, bb, "ConvertMP#0001Ab: ToBoolean(IntMP) failed.");

		}

		[Test, /*Ignore("Fix StackOverflowException raised by this number")*/]
		public void ToBoolean_FloatMP()
		{
			FloatMP a = new FloatMP("-10000000000001111111111111000000000011111111111000000000e-1234567890");
			FloatMP b = new FloatMP();

			bool ab = ConvertMP.ToBoolean(a);
			bool bb = ConvertMP.ToBoolean(b);
			Assert.AreEqual(true, ab, "ConvertMP#0002A: ToBoolean(FloatMP) failed.");
			Assert.AreEqual(false, bb, "ConvertMP#0002Ab: ToBoolean(FloatMP) failed.");

		}

		[Test]
		public void ToBoolean_RatnlMP()
		{
			RatnlMP a = new RatnlMP("-10000000000000000000111111111111111111111/11111111111111222222222223333333333333333333");
			RatnlMP b = new RatnlMP();

			bool ab = ConvertMP.ToBoolean(a);
			bool bb = ConvertMP.ToBoolean(b);
			Assert.AreEqual(true, ab, "ConvertMP#0003A: ToBoolean(RatnlMP) failed.");
			Assert.AreEqual(false, bb, "ConvertMP#0003Ab: ToBoolean(RatnlMP) failed.");

		}

		[Test, Ignore("Fix StackOverflowException raised by this number")]
		public void ToBoolean_FloatMpfr()
		{
			FloatMpfr a = new FloatMpfr("-10000000000001111111111111000000000011111111111000000000e-1234567890");
			FloatMpfr b = new FloatMpfr();

			bool ab = ConvertMP.ToBoolean(a);
			bool bb = ConvertMP.ToBoolean(b);
			Assert.AreEqual(true, ab, "ConvertMP#0004A: ToBoolean(FloatMpfr) failed.");
			Assert.AreEqual(false, bb, "ConvertMP#0004Ab: ToBoolean(FloatMpfr) failed.");

		}

		[Test]
		public void ToByte_IntMP_A() 
		{
			IntMP a = new IntMP(byte.MinValue);
			IntMP b = new IntMP(byte.MaxValue);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(byte.MinValue, ab, "ConvertMP#0005A: ToByte(IntMP) failed.");
			Assert.AreEqual(byte.MaxValue, bb, "ConvertMP#0005Ab: ToByte(IntMP) failed.");
		}

		[Test, ExpectedException(typeof(System.OverflowException))]
		public void ToByte_IntMP_B() 
		{
			IntMP a = new IntMP(-128);
			IntMP b = new IntMP(258);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(-128, ab, "ConvertMP#0005B: ToByte(IntMP) failed.");
			Assert.AreEqual(258, bb, "ConvertMP#0005Bb: ToByte(IntMP) failed.");
		}

		[Test]
		public void ToByte_FloatMP_A() 
		{
			FloatMP a = new FloatMP(byte.MinValue);
			FloatMP b = new FloatMP(byte.MaxValue);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(byte.MinValue, ab, "ConvertMP#0006A: ToByte(FloatMP) failed.");
			Assert.AreEqual(byte.MaxValue, bb, "ConvertMP#0006Ab: ToByte(FloatMP) failed.");
		}

		[Test, ExpectedException(typeof(System.OverflowException))]
		public void ToByte_FloatMP_B() 
		{
			FloatMP a = new FloatMP(-128);
			FloatMP b = new FloatMP(258);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(-128, ab, "ConvertMP#0006B: ToByte(FloatMP) failed.");
			Assert.AreEqual(258, bb, "ConvertMP#0006Bb: ToByte(FloatMP) failed.");
		}

		[Test]
		public void ToByte_RatnlMP_A() 
		{
			RatnlMP a = new RatnlMP(byte.MinValue, 1);
			RatnlMP b = new RatnlMP(byte.MaxValue, 1);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(byte.MinValue, ab, "ConvertMP#0007A: ToByte(RatnlMP) failed.");
			Assert.AreEqual(byte.MaxValue, bb, "ConvertMP#0007Ab: ToByte(RatnlMP) failed.");
		}

		[Test, ExpectedException(typeof(System.OverflowException))]
		public void ToByte_RatnlMP_B() 
		{
			RatnlMP a = new RatnlMP(-128, 1);
			RatnlMP b = new RatnlMP(258, 1);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(-128, ab, "ConvertMP#0006B: ToByte(RatnlMP) failed.");
			Assert.AreEqual(258, bb, "ConvertMP#0006Bb: ToByte(RatnlMP) failed.");
		}

		[Test]
		public void ToByte_FloatMpfr_A() 
		{
			FloatMpfr a = new FloatMpfr(byte.MinValue);
			FloatMpfr b = new FloatMpfr(byte.MaxValue);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(byte.MinValue, ab, "ConvertMP#0006A: ToByte(FloatMpfr) failed.");
			Assert.AreEqual(byte.MaxValue, bb, "ConvertMP#0006Ab: ToByte(FloatMpfr) failed.");
		}

		[Test, ExpectedException(typeof(System.OverflowException))]
		public void ToByte_FloatMpfr_B() 
		{
			FloatMpfr a = new FloatMpfr(-128);
			FloatMpfr b = new FloatMpfr(258);

			byte ab = ConvertMP.ToByte(a);
			byte bb = ConvertMP.ToByte(b);
			Assert.AreEqual(-128, ab, "ConvertMP#0006B: ToByte(FloatMpfr) failed.");
			Assert.AreEqual(258, bb, "ConvertMP#0006Bb: ToByte(FloatMpfr) failed.");
		}
	}
}
