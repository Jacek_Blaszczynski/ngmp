/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

#include "gmpglue.h"

using namespace std;

static const char version[] = "0.5.0.10";

char* get_gmpglue_version(void) {
	return (char*) version;
};

int __gmp_bits_per_limb_g(void) {
	return mp_bits_per_limb;
};

char* __gmp_version_g(void) {
	return (char*) gmp_version;
};

void * (*mp_alloc_func_ptr) (size_t);
void * (*mp_realloc_func_ptr) (void *, size_t, size_t);
void   (*mp_free_func_ptr) (void *, size_t);

/* 
Despite the fact that it would be easy to retrieve sign
from managed equivalent of mpz_t struct this call is much faster
than call to System.Runtime.InteropServices.Marhal.PtrToStructure
requiring marshaling of native to managed mpz_t - 
needs some more checks though.
*/
int __gmpz_sign(mpz_t data) {
	return mpz_sgn(data);
};

int __gmpz_odd_p(mpz_t data){
	return mpz_odd_p(data);
};

int __gmpz_even_p(mpz_t data){
	return mpz_even_p(data);
};

/* The same problem as above. */
int __gmpf_sign(mpf_t data) {
	return mpf_sgn(data);
};

/* The same problem as above. */
int __gmpq_sign(mpq_t data) {
	return mpq_sgn(data);
}

unsigned int get_processor_ticks(void) {
	
#ifdef WIN32

#ifdef x64
#ifndef  INTELC
	///
	/// TODO: Create external routines using yasm
	///

	return 0;

#else

	__asm {

		; Read and save TSC immediately after a tick

		rdtsc 

		; loDword is stored in eax
		; hiDword is stored in edx
		; we do not need to access them since we can return directly eax
		; from __asm part by doing nothing
		; this may raise warnings in some compilers but ingore them
	}

#endif
#endif

#else
#ifdef GCC

	unsigned int result;

	__asm__ __volatile__ ( 
		"rdtsc \n\t"
		: "=a" (result)
		: : "%edx"); 

	return result;

#endif
#endif
};

unsigned long long get_processor_ticks_p(void) {


#ifdef WIN32

	Ticks data;

#ifdef x64
#ifndef INTELC
	///
	/// TODO: Create external routines using yasm
	///

	return 0;
#else


	__asm {

		; Read and save TSC immediately after a tick

		rdtsc 
		mov		data.loDword, eax
		mov		data.hiDword, edx

		; loDword is stored in eax
		; hiDword is stored in edx
	}

	return data.tick;

#endif
#endif

#else
#ifdef GCC

	unsigned long long result;

		__asm__ __volatile__ ( 
		"rdtsc \n\t"
		: "=A" (result)
		); 

	return result;

#endif
#endif

};

unsigned long long get_processor_freq(unsigned int miliseconds) {

	if (miliseconds == 0) miliseconds = 10;

	unsigned int divider = 1000 / miliseconds;
	unsigned long long result = 0;

#ifdef WIN32

#ifdef x64
#ifndef INTELC
	///
	/// TODO: Create external routines using yasm
	///
#else

	// Check for RDTSC availability

	__asm {
		;
		mov		eax, 0x01
		cpuid
		bt		edx, 0x04				; Flags Bit 4 is TSC support
		jnc		we_are_done				; jump if TSC not supported
	}

#endif
#endif

	LARGE_INTEGER freq, st, en;
	unsigned long long startRDTSC, endRDTSC;

	QueryPerformanceFrequency( &freq);

	freq.QuadPart = freq.QuadPart/divider;

	QueryPerformanceCounter( &en);

	QueryPerformanceCounter( &st);

	startRDTSC = get_processor_ticks_p();

	for ( ; (en.QuadPart - st.QuadPart) <= freq.QuadPart ; ) {
		QueryPerformanceCounter(&en);
	}

	endRDTSC = get_processor_ticks_p();

	result = (endRDTSC - startRDTSC) * divider;

#else
#ifdef GCC

	struct timeval startV, endV;
	unsigned long long start, end;

	clock_t startC, endC, clocksC;
	clocksC = CLOCKS_PER_SEC/divider;

	int scaling = 10000000 / divider;
	
	int priority = getpriority(PRIO_PROCESS, 0);
	
	if (setpriority(PRIO_PROCESS, 0, -20) == -1) throw -100;
	
	//endC =clock();
	//startC = clock();

	gettimeofday(&endV, NULL);
	gettimeofday(&startV, NULL);

	// Wait for start of first period
	for ( ; endV.tv_sec - startV.tv_sec <  1 ; ) gettimeofday(&endV, NULL);

	start = get_processor_ticks();
	
	for ( ; endV.tv_sec - startV.tv_sec <  2 ; ) gettimeofday(&endV, NULL);
	
	end = get_processor_ticks();
	
	result = std::abs((long int) ((end - start) * divider));
	
	if (setpriority(PRIO_PROCESS, 0, priority) == -1) throw -100;

#endif
#endif

we_are_done:

	return result;
}




