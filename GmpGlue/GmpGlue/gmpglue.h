/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

// gmpglue shared library (dll) creates bridge between 
// CLI P\Invoke world and gmp library. 
// It enables retrieval of global variables
// inaccessible through method calls and provides utilities 
// for precise performance measurments not available 
// in existing CLI implementations.

#ifdef WIN32
#include "Windows.h"
#else
#include <stdio.h>
#include <iostream>
#include <sys/resource.h>
#include <sys/time.h>
#endif

#ifdef MPIR
#include <mpir.h>
#else
#include <gmp.h>
#endif


#ifdef WIN32
#ifdef GMPGLUE_EXPORTS
#define GMPGLUE_API __declspec(dllexport)
#else
#define GMPGLUE_API __declspec(dllimport)
#endif
#else
#ifdef GCC
#define GMPGLUE_API 
#else
#define GMPGLUE_API
#endif
#endif




typedef union _Ticks {

	struct {
	unsigned int loDword;
	unsigned int hiDword;
	};
	unsigned long long tick;
} Ticks;


extern "C" {

GMPGLUE_API char* get_gmpglue_version(void);

GMPGLUE_API int __gmp_bits_per_limb_g(void);

GMPGLUE_API char* __gmp_version_g(void);

GMPGLUE_API int __gmpz_sign(mpz_t data);

GMPGLUE_API int __gmpz_odd_p(mpz_t data);

GMPGLUE_API int __gmpz_even_p(mpz_t data);

GMPGLUE_API int __gmpf_sign(mpf_t data);

GMPGLUE_API int __gmpq_sign(mpq_t data);

GMPGLUE_API unsigned int get_processor_ticks(void);

GMPGLUE_API unsigned long long get_processor_ticks_p(void);

GMPGLUE_API unsigned long long get_processor_freq(unsigned int divider);

GMPGLUE_API mpz_t* __gmpz_init_mpz();

GMPGLUE_API mpz_t* __gmpz_init_set_mpz(mpz_t source);

GMPGLUE_API mpz_t* __gmpz_init_set_ui_mpz(unsigned long int value);

GMPGLUE_API mpz_t* __gmpz_init_set_si_mpz(long int value);

GMPGLUE_API mpz_t* __gmpz_init_set_d_mpz(double value);

GMPGLUE_API mpz_t* __gmpz_init_set_str_mpz(char * value, int radix);

// TODO: implement memory access

GMPGLUE_API extern void * (*mp_alloc_func_ptr) (size_t);
GMPGLUE_API extern void * (*mp_realloc_func_ptr) (void *, size_t, size_t);
GMPGLUE_API extern void   (*mp_free_func_ptr) (void *, size_t);

GMPGLUE_API void * mp_alloc(size_t size);

GMPGLUE_API void * mp_realloc(void *, size_t, size_t);

GMPGLUE_API void   mp_free(void *, size_t);

}

