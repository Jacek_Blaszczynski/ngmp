/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;


namespace NGmp.Math
{

	#region Class FloatMpfr

	/// <summary>
	/// Class FloatMpfr provides arbitrary precision floating point number
	/// arithmetic over FloatMpfr numbers. Class provides CLI framework bindings
	/// for MPFR library: http://www.mpfr.org/. Fully supported version = 2.0.3.
	/// For later versions FloatMpfr provides a subset of available interface.
	/// Using this library requires having installed shared (DLL) version 
	/// of MPFR library, which is not provided in official 2.0.3 release. 
	/// </summary>
	[Serializable]
	[SuppressUnmanagedCodeSecurity]
	public sealed class FloatMpfr : 
		IDisposable, 
		ICloneable, 
		IComparable, 
		ISerializable, 
		IConvertible,				// Duplicate due to IConvertibleMP
		IConvertibleMP				
#if NET_2_0
		,
		IComparable<FloatMpfr>,
		IEquatable<FloatMpfr>
#endif
	{

		#region Fields

		private static RoundMode nDefaultRoundMode = RoundMode.Nearest;
		private static int mMpfrCoreNativeSize = Marshal.SizeOf(typeof(mpfr_t));

		private static int nMinPrecision = 2;
		private static int nMaxPrecision = Int32.MaxValue;

		internal IntPtr number;
		private bool disposed =false;

		#endregion Fields

		#region Constructors & Dispose Methods
		
		#region Constructors

		public FloatMpfr() 
		{
			// It's necessary to call mpfr_init with pointer to allocated memory 
			// chunk fitting mpfr_t size - mpfr_t is declared as a single item array
			this.number = Marshal.AllocHGlobal(mMpfrCoreNativeSize);
			mpfr_init(this.number);
		}

		public FloatMpfr(int prec, bool defPrec) 
		{
			if (prec < FloatMpfr.MinPrecision || prec > FloatMpfr.MaxPrecision)
				throw new ArgumentOutOfRangeException("prec", prec, "Precision is less or more than allowed values range.");
			this.number = Marshal.AllocHGlobal(mMpfrCoreNativeSize);
			if (defPrec) 
			{
				FloatMpfr.SetDafaultPrecision((uint) prec);
				mpfr_init(this.number);
			}
			else 
			{
				mpfr_init2(this.number, (uint) prec);
			}
		}

		public FloatMpfr(IntMP floatmp) : this()
		{
			mpfr_set_z(this.number, floatmp.number, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(IntMP floatmp, int prec) : this(prec, false)
		{
			mpfr_set_z(this.number, floatmp.number, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(IntMP floatmp, RoundMode round) : this()
		{
			mpfr_set_z(this.number, floatmp.number, round);
		}

		public FloatMpfr(IntMP floatmp, int prec, RoundMode round) : this(prec, false)
		{
			mpfr_set_z(this.number, floatmp.number, round);
		}

		public FloatMpfr(RatnlMP floatmp) : this() 
		{
			mpfr_set_q(this.number, floatmp.number, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(RatnlMP floatmp, int prec) : this(prec, false) 
		{
			mpfr_set_q(this.number, floatmp.number, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(RatnlMP floatmp, RoundMode round) : this()
		{
			mpfr_set_q(this.number, floatmp.number, round);
		}

		public FloatMpfr(RatnlMP floatmp, int prec, RoundMode round) : this(prec, false)
		{
			mpfr_set_q(this.number, floatmp.number, round);
		}

		public FloatMpfr(FloatMP floatmp) : this()
		{
			mpfr_set_f(this.number, floatmp.number, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(FloatMP floatmp, int prec) : this(prec, false)
		{
			mpfr_set_f(this.number, floatmp.number, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(FloatMP floatmp, RoundMode round) : this()
		{
			mpfr_set_f(this.number, floatmp.number, round);
		}

		public FloatMpfr(FloatMP floatmp, int prec, RoundMode round) : this(prec, false)
		{
			mpfr_set_f(this.number, floatmp.number, round);
		}

		public FloatMpfr(uint floatmp) : this()
		{
			mpfr_set_ui(this.number, floatmp, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(uint floatmp, int prec) : this(prec, false)
		{
			mpfr_set_ui(this.number, floatmp, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(uint floatmp, RoundMode round) : this() 
		{
			mpfr_set_ui(this.number, floatmp, round);
		}

		public FloatMpfr(uint floatmp, int prec, RoundMode round) : this(prec, false) 
		{
			mpfr_set_ui(this.number, floatmp, round);
		}

		public FloatMpfr(int floatmp) : this()
		{
			mpfr_set_si(this.number, floatmp, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(int floatmp, int prec) : this(prec, false)
		{
			mpfr_set_si(this.number, floatmp, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(int floatmp, RoundMode round) : this()
		{
			mpfr_set_si(this.number, floatmp, round);
		}

		public FloatMpfr(int floatmp, int prec, RoundMode round) : this(prec, false)
		{
			mpfr_set_si(this.number, floatmp, round);
		}

		public FloatMpfr(double floatmp) : this()
		{
			mpfr_set_d(this.number, floatmp, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(double floatmp, int prec) : this(prec, false)
		{
			mpfr_set_d(this.number, floatmp, FloatMpfr.DefaultRoundMode);
		}

		public FloatMpfr(double floatmp, RoundMode round) : this()
		{
			mpfr_set_d(this.number, floatmp, round);
		}

		public FloatMpfr(double floatmp, int prec, RoundMode round) : this(prec, false)
		{
			mpfr_set_d(this.number, floatmp, round);
		}

		public FloatMpfr(String floatmp) : this(floatmp, 10, FloatMpfr.DefaultRoundMode)
		{
		}

		public FloatMpfr(String floatmp, int radix) : this(floatmp, radix, FloatMpfr.DefaultRoundMode)
		{
		}

		public FloatMpfr(String floatmp, int radix, RoundMode round): this()
		{
			if (0 != mpfr_set_str(this.number, floatmp, radix, round))
				throw new ArgumentException(String.Format(
					"Could not parse string \"{0}\" in radix {1} with RoundMode {2}",
					floatmp, radix, round));
		}

		public FloatMpfr(SerializationInfo info, StreamingContext context) : this()
		{
			this.Precision = info.GetUInt32("precision");
			FloatMpfr.Set(this, info.GetString("value"), 10, (RoundMode) info.GetValue("roundmode", typeof(RoundMode)));
		}

		#endregion Constructors

		#region IDisposable Implementation

		// Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!this.disposed)
			{
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if(disposing)
				{
					// Dispose managed resources.
				}
			 
				// Call the appropriate methods to clean up 
				// unmanaged resources here.
				// If disposing is false, 
				// only the following code is executed.
				if (this.number != IntPtr.Zero) 
				{
					mpfr_clear(this.number);
					this.number = IntPtr.Zero;       
				}
			}
			disposed = true;         
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~FloatMpfr()      
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}

		#endregion IDisposable Implementation

		#endregion Constructors & Dispose Methods

		#region Properties

		public bool IsDisposed
		{
			get {return this.disposed;}
		}

		public static int MinExponent
		{
			get { return mpfr_get_emin(); }
			set
			{
				if (value >= mpfr_get_emin_min() && value <= mpfr_get_emin_max())
					mpfr_set_emin(value);
				else throw new ArgumentOutOfRangeException(String.Format(
					"Value of MinExponent is outside of accepted range {0} - {1}",
					mpfr_get_emin_min(), mpfr_get_emin_max()));
			}
		}

		public static int MaxExponent
		{
			get { return mpfr_get_emax(); }
			set
			{
				if (value >= mpfr_get_emax_min() && value <= mpfr_get_emax_max())
					mpfr_set_emax(value);
				else throw new ArgumentOutOfRangeException(String.Format(
					"Value of MinExponent is outside of accepted range {0} - {1}",
					mpfr_get_emax_min(), mpfr_get_emax_max()));
			}
		}

		public IntPtr MPNumber
		{
			get {return this.number;}
			set {this.number = value;}
		}

		public uint Precision 
		{
			get {return mpfr_get_prec(this.number);}
			set {mpfr_set_prec(this.number, value);}
		}

		public static int MinPrecision
		{
			get {return nMinPrecision;}
		}

		public static int MaxPrecision
		{
			get {return nMaxPrecision;}
		}

		public static uint DefaultPrecision
		{
			get{ return mpfr_get_default_prec();}
			set{ mpfr_set_default_prec(value);}
		}
		public static RoundMode DefaultRoundMode
		{
			get {return FloatMpfr.nDefaultRoundMode;}
		}


		#endregion Properties

		#region MPFR P/Invoke Prototypes

		#region Initialization & Memory Mngmnt

		/// <summary>
		/// Initialize x and set its value to NaN.
		/// </summary>
		/// <param name="x"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_init(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x);

		/// <summary>
		/// Initialize x, set its precision to be exactly prec bits and its value to NaN.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="prec"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_init2(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x,
			uint prec);

		/// <summary>
		/// Free the space occupied by x. Make sure to call this function 
		/// for all [In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr variables when you are done with them. 
		/// </summary>
		/// <param name="x"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_clear (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x);

		/// <summary>
		/// Set the default precision to be exactly prec bits. 
		/// </summary>
		/// <param name="prec"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_set_default_prec (uint prec); 

		/// <summary>
		/// Returns the default MPFR precision in bits. 
		/// </summary>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint mpfr_get_default_prec (); 

		/// <summary>
		/// Return the precision actually used for assignments of x, 
		/// i.e. the number of bits used to store its mantissa.
		/// </summary>
		/// <param name="x"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint mpfr_get_prec (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr x); 

		/// <summary>
		/// Reset the precision of x to be exactly prec bits, 
		/// and set its value to NaN. The previous value stored in x is lost.
		/// In case you want to keep the previous value stored in x, 
		/// use mpfr_prec_round instead.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="prec"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_set_prec (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			uint prec); 

		/// <summary>
		/// Reset the precision of x to be exactly prec bits. 
		/// The only difference with mpfr_set_prec is that prec is assumed 
		/// to be small enough so that the mantissa fits into the current 
		/// allocated memory space for x. Otherwise the behavior is undefined. 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="prec"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_set_prec_raw (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			uint prec);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_set_default_rounding_mode (RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_prec_round (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			uint prec, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
#if !DEBUG
		[Obsolete("This function is obsolete. Please use mpfr_prec_round instead.", true)] 
#endif
		internal static extern int mpfr_round_prec (mpfr_t x, RoundMode round, uint prec);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
#if !DEBUG
		[Obsolete("This function is obsolete. Please use CLI enum functionality instead.", true)] 
#endif
		internal static extern StringBuilder mpfr_print_rnd_mode (RoundMode round);

		#endregion Initialization & Memory Mngmnt

		#region Assignment

		/// <summary>
		/// Set the value of result from operand, rounded towards the given direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro: use mpfr_set4(x, y, round, mpfr_sign(y)) instead", true)]
		internal static extern int mpfr_set (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		/// <summary>
		/// Set the value of result from operand, rounded towards the given direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set4 (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round,
			int sign);
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_ui (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.U4)] uint operand, 
			[In, MarshalAs(UnmanagedType.I2)] RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_si (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.I4)] int operand, 
			[In, MarshalAs(UnmanagedType.I2)] RoundMode round);
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_d (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_z (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		// Unfortunately we have no long double available in .NET - how to fix it???
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_ld (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_f (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		/// <summary>
		/// Set the variable x to infinity. x is set to plus infinity iff sign is nonnegative.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="sign"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_set_inf (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			int sign); 

		/// <summary>
		/// Set the variable x to NaN (Not-a-Number).
		/// </summary>
		/// <param name="x"></param>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_set_nan (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x); 

		/// <summary>
		/// Set x to the value of the whole string s in base 
		/// base (between 2 and 36), rounded in direction round. 
		/// See the documentation of mpfr_inp_str for a detailed 
		/// description of the valid string formats. This function 
		/// returns 0 if the entire string up to the final \0 is 
		/// a valid number in base base; otherwise it returns -1. 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="s"></param>
		/// <param name="radix"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern int mpfr_set_str (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			[In, Out,MarshalAs(UnmanagedType.LPStr)] String str /* char *s */, 
			int radix, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_swap (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr y); 

		#endregion Assignment

		#region Init & Assignment

		/// <summary>
		/// Initialize result and set its value from operand, rounded in the direction round. 
		/// The precision of result will be taken from the active default precision, 
		/// as set by mpfr_set_default_prec.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int operand, 
			RoundMode round);
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_d (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand, 
			RoundMode round); 

		/// <summary>
		/// Initialize x and set its value from the string s in base base, 
		/// rounded in the direction round. See mpfr_set_str.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="s"></param>
		/// <param name="radix">base (between 2 and 36)</param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_str (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			[In, MarshalAs(UnmanagedType.LPTStr)] String s /*const char *s*/, 
			int radix, 
			RoundMode round);

		// Unfortunately we have no long double available in .NET - how to fix it???
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_ld (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			double operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_z (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_init_set_f (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);


		#endregion Init & Assignment

		#region Conversion

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[return: MarshalAs(UnmanagedType.R8)]
		internal static extern double mpfr_get_d (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		/// <summary>
		/// Return d and set exp such that 0.5 more/eqal abs(d) less then 1 
		/// and d times 2 raised to exp equals operand rounded to 
		/// double precision, using the given rounding mode. 
		/// </summary>
		/// <param name="exp"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[return: MarshalAs(UnmanagedType.R8)]
		internal static extern double mpfr_get_d_2exp (int exp, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_get_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint mpfr_get_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		/// <summary>
		/// Convert operand to a string of digits in base base, with rounding 
		/// in direction round. The base may vary from 2 to 36. The generated string 
		/// is a fraction, with an implicit radix point immediately to the left of the first digit.
		/// </summary>
		/// <param name="str"></param>
		/// <param name="expptr"></param>
		/// <param name="radix"></param>
		/// <param name="n"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern void mpfr_get_str (
			[Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder str, 
			out int expptr, 
			int radix, 
			/*size_t*/ uint n, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 


		// Unfortunately we have no long double available in .NET - how to fix it???
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double mpfr_get_ld (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);
 
		/// <summary>
		/// Convert operand to a double, using the default MPFR rounding mode 
		/// (see function mpfr_set_default_rounding_mode). This function is obsolete.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This function is obsolete.")]
		[return: MarshalAs(UnmanagedType.R8)]
		internal static extern double mpfr_get_d1 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		/// <summary>
		/// Put the scaled mantissa of operand (regarded as an integer, with the precision of operand) 
		/// into z, and return the exponent exp (which may be outside 
		/// the current exponent range) such that operand exactly equals z multiplied by 
		/// two exponent exp. If the exponent is not representable 
		/// in the int type, the behavior is undefined. 
		/// </summary>
		/// <param name="z"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_get_z_exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr z, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		#endregion Conversion

		#region Arithmetic

		/// <summary>
		/// Set result to operand1 + operand2 rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_add (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round);

		/// <summary>
		/// Set result to operand1 + operand2 rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="?"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_add_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round);
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_add_z (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_add_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sub (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sub_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_ui_sub (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sub_z (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sub_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_mul (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_mul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_mul_z (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_mul_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_mul_2ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_mul_2si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This is macro: use mpfr_mul_2ui (result, operand1, operand2, round) instead", true)]
		internal static extern int mpfr_mul_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_neg (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This is macro: use mpfr_set4(result, operand, round, 1) instead", true)]
		internal static extern int mpfr_abs (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_div (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_ui_div (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_div_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_div_z (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_div_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This is macro: use mpfr_div_2ui (result, operand1, operand2, round) instead", true)]
		internal static extern int mpfr_div_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_div_2ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_div_2si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_pow (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_pow_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_ui_pow_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			uint operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_ui_pow (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sqrt (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sqrt_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand, 
			RoundMode round);

		/// <summary>
		/// Set result to the cubic root (defined over the real numbers) of operand rounded in the direction round. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cbrt (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		/// <summary>
		/// Set result to the natural logarithm of operand rounded in the direction round. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_log (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_log2 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_log10 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_exp2 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cos (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sin (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_tan (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sin_cos (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr sineResult, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr cosineResult, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_acos (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_asin (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_atan (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cosh (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_sinh (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_tanh (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_acosh (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_asinh (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_atanh (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fac_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_log1p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_expm1 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_gamma (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_zeta (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_erf (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fma (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand3, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_agm (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_const_log2 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_const_pi (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_const_euler (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			RoundMode round); 

		#endregion Arithmetic

		#region Comparison

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This is macro: use mpfr_cmp3(operand1, operand2, 1) instead", true)]
		internal static extern int mpfr_cmp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cmp3 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2,
			int s); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cmp_d (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			double operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro using mpfr_cmp_ui_2exp insted", true)]
		internal static extern int mpfr_cmp_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro using mpfr_cmp_si_2exp(x, y, 0) insted", true)]
		internal static extern int mpfr_cmp_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cmp_ui_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2, 
			int e); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cmp_si_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2, 
			int e); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_cmpabs (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro using mpfr_cmp_ui_2exp(x, 0, 0) insted", true)]
		internal static extern int mpfr_sgn (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_eq (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			uint operand3); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_equal_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_reldiff (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_nan_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_inf_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_number_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_greater_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_greaterequal_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_less_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_lessequal_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_lessgreater_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_unordered_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2); 

		#endregion Comparison

		#region IO

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern uint mpfr_out_str (
			IntPtr stream, 
			int radix, 
			uint n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round);

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern uint mpfr_inp_str (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			IntPtr stream, 
			int radix, 
			RoundMode round); 

		#endregion IO

		#region Random

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_urandomb (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.LPStruct)] RandState state); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This function is deprecated; mpfr_urandomb should be used instead.")]
		internal static extern void mpfr_random (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_random2 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint size, 
			int exp); 

		#endregion Random

		#region Miscallenous

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fits_ulong_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fits_slong_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fits_uint_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fits_sint_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fits_ushort_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_fits_sshort_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro: use mpfr_rint(x, y, RoundMode.PlusInfinity) insted", true)]
		internal static extern int mpfr_ceil ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro: use mpfr_rint(x, y, RoundMode.MinusInfinity) insted", true)]
		internal static extern int mpfr_floor ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro: use mpfr_rint(x, y, RoundMode.Zero) insted", true)]
		internal static extern int mpfr_trunc ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_integer_p ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("It is macro: use mpfr_rint(x, y, RoundMode.Nearest) insted", true)]
		internal static extern int mpfr_round (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_rint ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_frac ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_nexttoward ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr y); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_nextabove ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_nextbelow ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_min ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_max ([In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			RoundMode round); 


		#endregion Miscallenous

		#region Exception Handling

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_get_emin ();

		[DllImport(Utilities.mpf_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern int mpfr_get_emin_min();

		[DllImport(Utilities.mpf_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern int mpfr_get_emin_max();

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_get_emax ();

		[DllImport(Utilities.mpf_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern int mpfr_get_emax_min(); 

		[DllImport(Utilities.mpf_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern int mpfr_get_emax_max(); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_emin (int exp); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_set_emax (int exp); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_check_range (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr x, 
			int t, 
			RoundMode round); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_clear_underflow (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_clear_overflow ();
 
		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_clear_nanflag (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_clear_inexflag (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void mpfr_clear_flags (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_underflow_p (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_overflow_p (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_nanflag_p (); 

		[DllImport(Utilities.mpf_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int mpfr_inexflag_p (); 

		#endregion Exception Handling
	
		#endregion MPFR P/Invoke Prototypes

		#region Object Overrides

		public override bool Equals(object obj)
		{
			return base.Equals (obj);
		}

		public override int GetHashCode()
		{
			mpfr_t value = (mpfr_t) Marshal.PtrToStructure(this.number, typeof(mpfr_t));
			int hash = IntMP.GetHashCode(value._mpfr_size, value._mpfr_d);
			// Somehow oversimplified implementation
			hash ^= value._mpfr_exp;
			hash ^= (int) value._mpfr_prec;
			return hash;
		}

		public override string ToString ()
		{
			return this.ToString(10, 0, RoundMode.Nearest);
		}

		public string ToString (int radix) 
		{
			return this.ToString(radix, 0, RoundMode.Nearest);
		}

		public string ToString (int radix, RoundMode round) 
		{
			return this.ToString(radix, 0, round);
		}

		public string ToString (int radix, uint n, RoundMode round) 
		{
			if (mpfr_nan_p(this.number) != 0) return "NaN";
			else if (mpfr_inf_p(this.number) != 0)
			{
				FloatMpfr f = new FloatMpfr(0);
				if (mpfr_greater_p(this.number, f.number) != 0) return "+Infinity";
				else return "-Infinity";
			}
			StringBuilder str = new StringBuilder(10000);
			int expptr = 0;
			mpfr_get_str(str, out expptr, radix, n, this.number, round);
			if (str != null)
			{
				int absexpptr = System.Math.Abs(expptr);
				int x = 0;
				if (str[0] == '-') x = 1;
				//				else x = 1;
				if ((absexpptr+x) < str.Length) str = str.Insert(absexpptr + x, '.');
				else if ((absexpptr+x) > str.Length) 
				{
					if (x == 1) str.Insert(1, "0.");
					else str.Insert(0, "0.");
					str.Append("e" + expptr);
				}
				return str.ToString();
			}
			else return null;
		}


		#endregion Object Overrides

		#region IComparable Members

		public int CompareTo(object obj)
		{
			if (obj is FloatMpfr) return mpfr_cmp3(this.number, ((FloatMpfr)obj).number, 1);
			else if (obj is uint) return mpfr_cmp_ui_2exp(this.number, (uint) obj, 0);
			else if (obj is int) return mpfr_cmp_si_2exp(this.number, (int) obj, 0);
			else if (obj is double) return mpfr_cmp_d(this.number, (double) obj);
			else throw new ArgumentException(@"CompareTo argument obj has wrong Type. FloatMpfr, int, uint, double expected", "obj");
		}


		#endregion

#if NET_2_0
		#region IComparable<> Members

		public int CompareTo(FloatMpfr value)
		{
			return mpfr_cmp3(this.number, value.number, 1);
		}

		#endregion IComparable<> Members
#endif

		#region IConvertible Members

		ulong System.IConvertible.ToUInt64(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		sbyte System.IConvertible.ToSByte(IFormatProvider provider)
		{
			return ConvertMP.ToSByte(this);
		}

		double System.IConvertible.ToDouble(IFormatProvider provider)
		{
			return ConvertMP.ToDouble(this);
		}

		DateTime System.IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		float System.IConvertible.ToSingle(IFormatProvider provider)
		{
			return ConvertMP.ToSingle(this);
		}

		bool System.IConvertible.ToBoolean(IFormatProvider provider)
		{
			return ConvertMP.ToBoolean(this);
		}

		int System.IConvertible.ToInt32(IFormatProvider provider)
		{
			return ConvertMP.ToInt32(this);
		}

		ushort System.IConvertible.ToUInt16(IFormatProvider provider)
		{
			return ConvertMP.ToUInt16(this);
		}

		short System.IConvertible.ToInt16(IFormatProvider provider)
		{
			return ConvertMP.ToInt16(this);
		}

		public string ToString(IFormatProvider provider)
		{
			return ConvertMP.ToString(this, provider);
		}

		byte System.IConvertible.ToByte(IFormatProvider provider)
		{
			return ConvertMP.ToByte(this);
		}

		char System.IConvertible.ToChar(IFormatProvider provider)
		{
			return ConvertMP.ToChar(this);
		}

		long System.IConvertible.ToInt64(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public System.TypeCode GetTypeCode()
		{
			return (System.TypeCode) 22;
		}

		decimal System.IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public object ToType(Type conversionType, IFormatProvider provider)
		{
			return this.GetType();
		}

		uint System.IConvertible.ToUInt32(IFormatProvider provider)
		{
			return ConvertMP.ToUInt32(this);
		}

		#endregion

		#region IConvertibleMP Members

		public IntMP ToIntMP(IFormatProvider provider)
		{
			// TODO:  Add FloatMpfr.ToIntMP implementation
			return null;
		}

		public FloatMP ToFloatMP(IFormatProvider provider)
		{
			// TODO:  Add FloatMpfr.ToFloatMP implementation
			return null;
		}

		public NGmp.Math.FloatMpfr ToFloatMpfr(IFormatProvider provider)
		{
			return this;
		}

		public RatnlMP ToRatnlMP(IFormatProvider provider)
		{
			// TODO:  Add FloatMpfr.ToRatnlMP implementation
			return null;
		}

		#endregion

		#region ICloneable Members

		public object Clone()
		{
			FloatMpfr result = new FloatMpfr( (int) mpfr_get_prec(this.number), false);
			int sign = mpfr_cmp_ui_2exp(this.number, 0, 0);
			mpfr_set4(result.number, this.number, FloatMpfr.DefaultRoundMode, sign);
			return result;
		}

		#endregion

#if NET_2_0
		#region IEquatable<> Members

		public bool Equals(FloatMpfr value)
		{
			return mpfr_cmp3(this.number, value.number, 1) == 0;
		}

		#endregion IEquatable<> Members
#endif

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (!disposed) 
			{
				info.AddValue("precision", this.Precision);
				info.AddValue("value", this.ToString());
				info.AddValue("roundmode", FloatMpfr.DefaultRoundMode);
			}
			else throw new ObjectDisposedException(this.GetType().ToString());
		}

		#endregion

		#region Static Methods

		#region Initialization

		/// <summary>
		/// Method SetDefaultPrec sets precision which will be used 
		/// as default value by library. The Precision is the number of bits 
		/// used to represent the mantissa of a floating-point number.
		/// The precision can be any integer between MPFR_PREC_MIN and MPFR_PREC_MAX. 
		/// In the current implementation, MPFR_PREC_MIN is equal to 2 and 
		/// MPFR_PREC_MAX is equal to ULONG_MAX/2.
		/// </summary>
		/// <param name="prec"></param>
		public static void SetDafaultPrecision (uint prec) 
		{
			mpfr_set_default_prec (prec);
		}

		public static uint GetDefaultPrecision ()
		{
			return mpfr_get_default_prec();
		}

		public static uint GetPrecision (FloatMpfr x) 
		{
			return mpfr_get_prec(x.number);
		}

		public static void SetPrecision (FloatMpfr x, uint prec)
		{
			mpfr_set_prec(x.number, prec);
		}

		public static void SetPrecisionRaw (FloatMpfr x, uint prec) 
		{
			mpfr_set_prec_raw(x.number, prec);
		}

		public static void SetDefaultRoundingMode(RoundMode round) 
		{
			FloatMpfr.nDefaultRoundMode = round;
			mpfr_set_default_rounding_mode(round);
		}

		/// <summary>
		/// Rounds x according to round with precision prec, which must be 
		/// an integer between MPFR_PREC_MIN and MPFR_PREC_MAX 
		/// (otherwise the behavior is undefined). 
		/// </summary>
		/// <param name="x"></param>
		/// <param name="prec"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int PrecisionRound (FloatMpfr x, uint prec, RoundMode round) 
		{
			return mpfr_prec_round(x.number, prec, round);
		}


		#endregion Initialization

		#region Assignment

		public static int Set (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			int sign = mpfr_cmp_ui_2exp(operand.number, 0, 0);
			return mpfr_set4(result.number, operand.number, round, sign);
		}

		public static int Set (FloatMpfr result, uint operand, RoundMode round) 
		{
			return mpfr_set_ui(result.number, operand, round);
		}

		public static int Set (FloatMpfr result, int operand, RoundMode round)
		{
			return mpfr_set_si(result.number, operand, round);
		}

		public static int Set (FloatMpfr result, double operand, RoundMode round)
		{
			return mpfr_set_d(result.number, operand, round);
		}

		public static int Set (FloatMpfr result, IntMP operand, RoundMode round) 
		{
			return mpfr_set_z (result.number, operand.number, round);
		}

		public static int Set (FloatMpfr result, RatnlMP operand, RoundMode round) 
		{
			return mpfr_set_q(result.number, operand.number, round);
		}

		public static int Set (FloatMpfr result, FloatMP operand, RoundMode round)
		{
			return mpfr_set_f(result.number, operand.number, round);
		}

		public static void SetInfinity (FloatMpfr x, int sign) 
		{
			mpfr_set_inf(x.number, sign);
		}

		public static void SetNaN (FloatMpfr x) 
		{
			mpfr_set_nan(x.number);
		}

		/// <summary>
		/// Set x to the value of the whole string s in base base 
		/// (between 2 and 36), rounded in direction round. 
		/// The string is of the form M@N or, if the base is 10 or less, 
		/// alternatively MeN or MEN, or, if the base is 16, alternatively MpB or MPB. 
		/// M is the mantissa in the specified base, N is the exponent written 
		/// in decimal for the specified base, and in base 16, B is the binary 
		/// exponent written in decimal (i.e. it indicates the power of 2 by which 
		/// the mantissa is to be scaled). The argument base may be in the range 2 to 36. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="str"></param>
		/// <param name="radix"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Set (FloatMpfr result, String str, int radix, RoundMode round) 
		{
			return mpfr_set_str(result.number, str, radix, round);
		}

		public static void Swap (FloatMpfr x, FloatMpfr y) 
		{
			mpfr_swap(x.number, y.number);
		}

		#endregion Assignment

		#region Conversion

		public static double GetDouble (FloatMpfr operand, RoundMode round) 
		{
			return mpfr_get_d (operand.number, round);
		}

		/// <summary>
		/// Return double and set exp such that 0.5 more/eqal abs(d) less then 1 
		/// and d times 2 raised to exp equals operand rounded to 
		/// double precision, using the given rounding mode.
		/// </summary>
		/// <param name="exp"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static double GetDouble2Exp (int exp, FloatMpfr operand, RoundMode round)
		{
			return mpfr_get_d_2exp(exp, operand.number, round);
		}

		public static int GetInt32 (FloatMpfr operand, RoundMode round) 
		{
			return mpfr_get_si(operand.number, round);
		}

		public static uint GetUInt32 (FloatMpfr operand, RoundMode round) 
		{
			return mpfr_get_ui(operand.number, round);
		}

		/// <summary>
		/// Put the scaled mantissa of operand (regarded as an integer, with the precision of operand) 
		/// into IntMP result, and return Int32 which is the exponent exp (which may be outside 
		/// the current exponent range) such that operand exactly equals result multiplied by 
		/// two exponent exp. If the exponent is not representable 
		/// in the Int32 type, the behavior is undefined. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns>Exponent exp.</returns>
		public static int GetIntMP2Exp (IntMP result, FloatMpfr operand) 
		{
			return mpfr_get_z_exp (result.number, operand.number);
		}



		public static FloatMpfr Parse(String s, System.Globalization.NumberStyles style, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//if (s == null) throw new System.ArgumentNullException("s", "Value of FloatMpfr underlying number cannot be null.");
			//FloatMpfr result = new FloatMpfr();
			//return result;
		}

		public static FloatMpfr Parse(String s, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return FloatMpfr.Parse(s, NumberStyles.Integer, provider);
		}

		public static FloatMpfr Parse(String s, NumberStyles style) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return FloatMpfr.Parse(s, style, null);
		}

		public static FloatMpfr Parse(string s) 
		{
			if (s == null) throw new System.ArgumentNullException("s", "Value of FloatMpfr underlying number cannot be null.");

			// TODO: s format style and format checks

			FloatMpfr result = new FloatMpfr();
			if (0 != mpfr_set_str(result.number, s, 10, FloatMpfr.DefaultRoundMode))
				throw new ArgumentException(String.Format(
					"Could not parse string \"{0}\" to FloatMpfr", s));
			return result;

		}

		#endregion Conversion

		#region Arithmetic 

		public static int Add (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round) 
		{
			return mpfr_add(result.number, operand1.number, operand2.number, round);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2) 
		{
			return mpfr_add(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round) 
		{
			return mpfr_add_ui(result.number, operand1.number, operand2, round);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, uint operand2) 
		{
			return mpfr_add_ui( result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, IntMP operand2, RoundMode round)
		{
			return mpfr_add_z(result.number, operand1.number, operand2.number, round);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, IntMP operand2) 
		{
			return mpfr_add_z(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2, RoundMode round)
		{
			return mpfr_add_q(result.number, operand1.number, operand2.number, round);
		}

		public static int Add (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2) 
		{
			return mpfr_add_q(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round)
		{
			return mpfr_sub(result.number, operand1.number, operand2.number, round);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2)
		{
			return mpfr_sub(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round)
		{
			return mpfr_sub_ui(result.number, operand1.number, operand2, round);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, uint operand2)
		{
			return mpfr_sub_ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Sub (FloatMpfr result, uint operand1, FloatMpfr operand2, RoundMode round)
		{
			return mpfr_ui_sub(result.number, operand1, operand2.number, round);
		}

		public static int Sub (FloatMpfr result, uint operand1, FloatMpfr operand2) 
		{
			return mpfr_ui_sub(result.number, operand1, operand2.number, RoundMode.Nearest);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, IntMP operand2, RoundMode round)
		{
			return mpfr_sub_z(result.number, operand1.number, operand2.number, round);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, IntMP operand2) 
		{
			return mpfr_sub_z(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2, RoundMode round)
		{
			return mpfr_sub_q(result.number, operand1.number, operand2.number, round);
		}

		public static int Sub (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2) 
		{
			return mpfr_sub_q(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round) 
		{
			return mpfr_mul( result.number, operand1.number, operand2.number, round);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2) 
		{
			return mpfr_mul( result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round)
		{
			return mpfr_mul_ui(result.number, operand1.number, operand2, round);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, uint operand2) 
		{
			return mpfr_mul_ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, IntMP operand2, RoundMode round)
		{
			return mpfr_mul_z(result.number, operand1.number, operand2.number, round);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, IntMP operand2) 
		{
			return mpfr_mul_z(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2, RoundMode round)
		{
			return mpfr_mul_q(result.number, operand1.number, operand2.number, round);
		}

		public static int Mul (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2) 
		{
			return mpfr_mul_q(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Mul2 (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round) 
		{
			return mpfr_mul_2ui(result.number, operand1.number, operand2, round);
		}

		public static int Mul2 (FloatMpfr result, FloatMpfr operand1, uint operand2) 
		{
			return mpfr_mul_2ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Mul2 (FloatMpfr result, FloatMpfr operand1, int operand2, RoundMode round) 
		{
			return mpfr_mul_2si(result.number, operand1.number, operand2, round);
		}

		public static int Mul2 (FloatMpfr result, FloatMpfr operand1, int operand2) 
		{
			return mpfr_mul_2si(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Mul2Exp (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round) 
		{
			return mpfr_mul_2ui(result.number, operand1.number, operand2, round);
		}

		public static int Mul2Exp (FloatMpfr result, FloatMpfr operand1, uint operand2) 
		{
			return mpfr_mul_2ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Neg (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_neg(result.number, operand.number, round);
		}

		public static int Neg (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_neg(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Abs (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_set4(result.number, operand.number, round, 1);
		}

		public static int Abs (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_set4(result.number, operand.number, RoundMode.Nearest, 1);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round)
		{
			return mpfr_div(result.number, operand1.number, operand2.number, round);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2)
		{
			return mpfr_div(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}		

		public static int Div (FloatMpfr result, uint operand1, FloatMpfr operand2, RoundMode round)
		{
			return mpfr_ui_div(result.number, operand1, operand2.number, round);
		}

		public static int Div (FloatMpfr result, uint operand1, FloatMpfr operand2)
		{
			return mpfr_ui_div(result.number, operand1, operand2.number, RoundMode.Nearest);
		}
		
		public static int Div (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round)
		{
			return mpfr_div_ui(result.number, operand1.number, operand2, round);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, uint operand2)
		{
			return mpfr_div_ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, IntMP operand2, RoundMode round)
		{
			return mpfr_div_z(result.number, operand1.number, operand2.number, round);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, IntMP operand2)
		{
			return mpfr_div_z(result.number, operand1.number, operand2.number,RoundMode.Nearest);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2, RoundMode round)
		{
			return mpfr_div_q(result.number, operand1.number, operand2.number, round);
		}

		public static int Div (FloatMpfr result, FloatMpfr operand1, RatnlMP operand2)
		{
			return mpfr_div_q(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Div2Exp (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round)
		{
			return mpfr_div_2ui(result.number, operand1.number, operand2, round);
		}

		public static int Div2Exp (FloatMpfr result, FloatMpfr operand1, uint operand2)
		{
			return mpfr_div_2ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Div2 (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round)
		{
			return mpfr_div_2ui(result.number, operand1.number, operand2, round);
		}

		public static int Div2 (FloatMpfr result, FloatMpfr operand1, uint operand2)
		{
			return mpfr_div_2ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Div2 (FloatMpfr result, FloatMpfr operand1, int operand2, RoundMode round)
		{
			return mpfr_div_2si(result.number, operand1.number, operand2, round);
		}

		public static int Div2 (FloatMpfr result, FloatMpfr operand1, int operand2)
		{
			return mpfr_div_2si(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Pow (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round)
		{
			return mpfr_pow(result.number, operand1.number, operand2.number, round);
		}

		public static int Pow (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2)
		{
			return mpfr_pow(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int Pow (FloatMpfr result, FloatMpfr operand1, uint operand2, RoundMode round)
		{
			return mpfr_pow_ui(result.number, operand1.number, operand2, round);
		}

		public static int Pow (FloatMpfr result, FloatMpfr operand1, uint operand2)
		{
			return mpfr_pow_ui(result.number, operand1.number, operand2, RoundMode.Nearest);
		}

		public static int Pow (FloatMpfr result, uint operand1, uint operand2, RoundMode round)
		{
			return mpfr_ui_pow_ui(result.number, operand1, operand2, round);
		}

		public static int Pow (FloatMpfr result, uint operand1, uint operand2)
		{
			return mpfr_ui_pow_ui(result.number, operand1, operand2, RoundMode.Nearest);
		}

		public static int Pow (FloatMpfr result, uint operand1, FloatMpfr operand2, RoundMode round)
		{
			return mpfr_ui_pow(result.number, operand1, operand2.number, round);
		}

		public static int Pow (FloatMpfr result, uint operand1, FloatMpfr operand2)
		{
			return mpfr_ui_pow(result.number, operand1, operand2.number, RoundMode.Nearest);
		}

		public static int Sqrt (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_sqrt(result.number, operand.number, round);
		}

		public static int Sqrt (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_sqrt(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Sqrt (FloatMpfr result, uint operand, RoundMode round) 
		{
			return mpfr_sqrt_ui(result.number, operand, round);
		}

		public static int Sqrt (FloatMpfr result, uint operand) 
		{
			return mpfr_sqrt_ui(result.number, operand, RoundMode.Nearest);
		}

		public static int Cbrt (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_cbrt(result.number, operand.number, round);
		}

		public static int Cbrt (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_cbrt(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Log (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_log(result.number, operand.number, round);
		}

		public static int Log (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_log(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Log2 (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_log2(result.number, operand.number, round);
		}
		
		public static int Log2 (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_log2(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Log10 (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_log10(result.number, operand.number, round);
		}

		public static int Log10 (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_log10(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Exp (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_exp(result.number, operand.number, round);
		}

		public static int Exp (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_exp(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Exp2 (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_exp2(result.number, operand.number, round);
		}

		public static int Exp2 (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_exp2(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Cos (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_cos(result.number, operand.number, round);
		}

		public static int Cos (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_cos(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Sin (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_sin(result.number, operand.number, round);
		}

		public static int Sin (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_sin(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Tan (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_tan(result.number, operand.number, round);
		}

		public static int Tan (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_tan(result.number, operand.number, RoundMode.Nearest);
		}

		public static int SinCos (FloatMpfr sineResult, FloatMpfr cosineResult, FloatMpfr operand, RoundMode round)
		{
			return mpfr_sin_cos(sineResult.number, cosineResult.number, operand.number, round);
		}

		public static int SinCos (FloatMpfr sineResult, FloatMpfr cosineResult, FloatMpfr operand)
		{
			return mpfr_sin_cos(sineResult.number, cosineResult.number, operand.number, RoundMode.Nearest);
		}

		public static int Acos (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_acos(result.number, operand.number, round);
		}

		public static int Acos (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_asin(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Asin (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_asin(result.number, operand.number, round);
		}

		public static int Asin (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_asin(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Atan (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_atan(result.number, operand.number, round);
		}

		public static int Atan (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_atan(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Cosh (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_cosh(result.number, operand.number, round);
		}

		public static int Cosh (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_cosh(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Sinh (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_sinh(result.number, operand.number, round);
		}

		public static int Sinh (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_sinh(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Tanh (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_tanh(result.number, operand.number, round);
		}

		public static int Tanh (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_tanh(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Acosh (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_acosh(result.number, operand.number, round);
		}

		public static int Acosh (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_acosh(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Asinh (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_asinh(result.number, operand.number, round);
		}

		public static int Asinh (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_asinh(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Atanh (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_atanh(result.number, operand.number, round);
		}

		public static int Atanh (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_atanh(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Fact (FloatMpfr result, uint operand, RoundMode round) 
		{
			return mpfr_fac_ui(result.number, operand, round);
		}

		public static int Fact (FloatMpfr result, uint operand) 
		{
			return mpfr_fac_ui(result.number, operand, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to the logarithm of one plus operand, rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Log1P (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_log1p(result.number, operand.number, round);
		}

		public static int Log1P (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_log1p(result.number, operand.number, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to the exponential of operand minus one, rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int ExpM1 (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_expm1(result.number, operand.number, round);
		}

		/// <summary>
		/// Set result to the exponential of operand minus one, rounded in the direction to nearest.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int ExpM1 (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_expm1(result.number, operand.number, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to the value of the Gamma function on operand, rounded in the direction round.  
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Gamma (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_gamma(result.number, operand.number, round);
		}

		/// <summary>
		/// Set result to the value of the Gamma function on operand, rounded in the direction to nearest.  
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int Gamma (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_gamma(result.number, operand.number, RoundMode.Nearest);
		}

		public static int Zeta (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_zeta(result.number, operand.number, round);
		}

		public static int Zeta (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_zeta(result.number, operand.number, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to the value of the error function on operand, rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Erf (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_erf(result.number, operand.number, round);
		}

		public static int Erf (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_erf(result.number, operand.number, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to operand1 times operand2 + operand3, rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="operand3"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Fma (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, FloatMpfr operand3, RoundMode round) 
		{
			return mpfr_fma(result.number, operand1.number, operand2.number, operand3.number, round);
		}

		public static int Fma (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, FloatMpfr operand3) 
		{
			return mpfr_fma(result.number, operand1.number, operand2.number, operand3.number, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to the arithmetic-geometric mean of operand1 and operand2, 
		/// rounded in the direction round. The arithmetic-geometric mean 
		/// is the common limit of the sequences u[n] and v[n], 
		/// where u[0]=operand1, v[0]=operand2, u[n+1] is the arithmetic mean 
		/// of u[n] and v[n], and v[n+1] is the geometric mean of u[n] and v[n]. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int ArithmGeomMean (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round) 
		{
			return mpfr_agm(result.number, operand1.number, operand2.number, round);
		}

		public static int ArithmGeomMean (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2) 
		{
			return mpfr_agm(result.number, operand1.number, operand2.number, RoundMode.Nearest);
		}

		public static int ConstLog2 (FloatMpfr result, RoundMode round) 
		{
			return mpfr_const_log2(result.number, round);
		}

		public static int ConstLog2 (FloatMpfr result) 
		{
			return mpfr_const_log2(result.number, RoundMode.Nearest);
		}

		public static int ConstPi (FloatMpfr result, RoundMode round) 
		{
			return mpfr_const_pi(result.number, round);
		}

		public static int ConstPi (FloatMpfr result) 
		{
			return mpfr_const_pi(result.number, RoundMode.Nearest);
		}

		public static int ConstEuler (FloatMpfr result, RoundMode round) 
		{
			return mpfr_const_euler(result.number, RoundMode.Nearest);
		}

		public static int ConstEuler (FloatMpfr result) 
		{
			return mpfr_const_euler(result.number, RoundMode.Nearest);
		}

		
		#endregion Arithmetic

		#region Comparison

		/// <summary>
		/// Compare operand1 and operand2. Return a positive value if operand1 > operand2, 
		/// zero if operand1 = operand2, and a negative value if operand2 > operand1. 
		/// Both operand1 and operand2 are considered to their full own precision, which may differ. 
		/// If one of the operands is NaN (Not-a-Number), the behavior is undefined. 
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static int Compare (FloatMpfr operand1, FloatMpfr operand2) 
		{
			return mpfr_cmp3(operand1.number, operand2.number, 1);
		}

		/// <summary>
		/// Compare operand1 and operand2. Return a positive value if operand1 > operand2, 
		/// zero if operand1 = operand2, and a negative value if operand2 > operand1. 
		/// Both operand1 and operand2 are considered to their full own precision, which may differ. 
		/// If one of the operands is NaN (Not-a-Number), the behavior is undefined.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static int Compare (FloatMpfr operand1, uint operand2) 
		{
			return mpfr_cmp_ui_2exp(operand1.number, operand2, 0);
		}

		/// <summary>
		/// Compare operand1 and operand2. Return a positive value if operand1 > operand2, 
		/// zero if operand1 = operand2, and a negative value if operand2 > operand1. 
		/// Both operand1 and operand2 are considered to their full own precision, which may differ. 
		/// If one of the operands is NaN (Not-a-Number), the behavior is undefined.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static int Compare (FloatMpfr operand1, double operand2) 
		{
			return mpfr_cmp_d(operand1.number, operand2);
		}

		/// <summary>
		/// Compare operand1 and operand2. Return a positive value if operand1 > operand2, 
		/// zero if operand1 = operand2, and a negative value if operand2 > operand1. 
		/// Both operand1 and operand2 are considered to their full own precision, which may differ. 
		/// If one of the operands is NaN (Not-a-Number), the behavior is undefined.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static int Compare (FloatMpfr operand1, int operand2) 
		{
			return mpfr_cmp_si_2exp(operand1.number, operand2, 0);
		}

		/// <summary>
		/// Compare operand1 and operand2 multiplied by two to the power e. 
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		public static int Compare (FloatMpfr operand1, uint operand2, int e) 
		{
			return mpfr_cmp_ui_2exp(operand1.number, operand2, e);
		}

		/// <summary>
		/// Compare operand1 and operand2 multiplied by two to the power e. 
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="e"></param>
		/// <returns></returns>
		public static int Compare (FloatMpfr operand1, int operand2, int e) 
		{
			return mpfr_cmp_si_2exp(operand1.number, operand2, e);
		}

		/// <summary>
		/// Compare |operand1| and |operand2|. Return a positive value if |operand1| > |operand2|, 
		/// zero if |operand1| = |operand2|, and a negative value if |operand2|> |operand1|. 
		/// If one of the operands is NaN (Not-a-Number), the behavior is undefined.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static int CompareAbs (FloatMpfr operand1, FloatMpfr operand2) 
		{
			return mpfr_cmpabs(operand1.number, operand2.number);
		}

		// Needs to be implemented - it's a MPFR macro
		public static int Sign (FloatMpfr operand) 
		{
			return mpfr_cmp_ui_2exp(operand.number, 0, 0);
		}

		/// <summary>
		/// Return true if operand1 and operand2 are both non-zero ordinary numbers 
		/// with the same exponent and the same first operand3 bits, both zero, 
		/// or both infinities of the same sign. Return false otherwise. 
		/// This function is defined for compatibility with FloatMP.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="operand3"></param>
		/// <returns></returns>
		public static bool Equal (FloatMpfr operand1, FloatMpfr operand2, uint operand3) 
		{
			if (mpfr_eq(operand1.number, operand2.number, operand3)!=0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand1 = operand2, false otherwise (i.e. operand1 and/or operand2 are NaN, or operand1 <> operand2).
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool Equal (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_equal_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Compute the relative difference between operand1 and operand2 and store the result in result. 
		/// This function does not guarantee the exact rounding on the relative difference; 
		/// it just computes |operand1-operand2|/operand1, using the rounding mode round for all operations and the precision of result.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="round"></param>
		public static void RelativeDifference (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round) 
		{
			mpfr_reldiff(result.number, operand1.number, operand2.number, round);
		}

		/// <summary>
		/// Return true if operand is Not-a-Number (NaN). Return false otherwise.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static bool IsNaN (FloatMpfr operand) 
		{
			if (mpfr_nan_p(operand.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return non-zero if operand is an infinity. Return false otherwise.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static bool IsInfinity (FloatMpfr operand) 
		{
			if (mpfr_inf_p(operand.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand is an ordinary number (i.e. neither Not-a-Number nor an infinity). Return false otherwise.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static bool IsNumber (FloatMpfr operand) 
		{
			if (mpfr_number_p(operand.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand1 > operand2, false otherwise.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool Greater (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_greater_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand1 >= operand2, false otherwise.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool GreaterEqual (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_greaterequal_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand1 is less than operand2, false otherwise.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool Less (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_less_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand1 is less or equal to operand2, false otherwise.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool LessEqual (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_lessequal_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}
		/// <summary>
		/// Return true if operand1 is less than operand2 or operand1 > operand2 
		/// (i.e. neither operand1, nor operand2 is NaN, and operand1 <> operand2), 
		/// false otherwise (i.e. operand1 and/or operand2 are NaN, or operand1 = operand2).
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool LessGreater (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_lessgreater_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Return true if operand1 or operand2 is a NaN (i.e. they cannot be compared), false otherwise
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool Unordered (FloatMpfr operand1, FloatMpfr operand2) 
		{
			if (mpfr_unordered_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		#endregion Comparison

		#region IO

		public static void Save (String fileName, FloatMpfr operand) 
		{
			Save (fileName, 10, 0, operand, FloatMpfr.DefaultRoundMode);
		}

		public static void Save (String fileName, int radix, FloatMpfr operand) 
		{
			Save (fileName, radix, 0, operand, FloatMpfr.DefaultRoundMode);
		}

		/// <summary>
		/// Save operand value in text file with fileName, as a string of digits in base radix, 
		/// rounded in direction rnd. The base may vary from 2 to 36. Print nunmberDigits significant 
		/// digits exactly, or if numberDigits is 0, the maximum number of digits accurately 
		/// representable by operand. In addition to the significant digits, a decimal point 
		/// at the right of the first digit and a trailing exponent in base 10, in the form eNNN, 
		/// are printed. If base is greater than 10, @ will be used instead of e as exponent delimiter. 
		/// </summary>
		/// <param name="fileName">Fully qualified path to file.</param>
		/// <param name="radix"></param>
		/// <param name="numberDigits"></param>
		/// <param name="operand"></param>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="IOException"></exception>
		public static void Save (String fileName, int radix, uint numberDigits, FloatMpfr operand) 
		{
			Save (fileName, radix, numberDigits, operand, FloatMpfr.DefaultRoundMode);

		}

		public static void Save (String fileName, int radix, uint numberDigits, 
			FloatMpfr operand, RoundMode round) 
		{
			if (fileName == null ) 
				throw new ArgumentNullException("fileName", 
					"Argument is null. Fully qualified path to file expected.");

			if (operand == null ) 
				throw new ArgumentNullException("operand", 
					"Argument is null. Expected valid FloatMP for saving.");
			
#if NET_2_0
			if (fileName.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
#else
			if (fileName.IndexOfAny(Path.InvalidPathChars) >= 0) 
#endif
				throw new ArgumentException("Invalid characters in path: " + fileName, "fileName");

			if (!Directory.Exists(Path.GetDirectoryName(fileName))) 
				throw new DirectoryNotFoundException(
					"Specified directory does not exist: " + Path.GetDirectoryName(fileName));

			if (radix < 2 || radix > 36 ) 
				throw new ArgumentOutOfRangeException("radix", radix, 
					"Argument radix is out of range. Accepted values are from 2 to 36 inclusive.");

			IntPtr pf = IntPtr.Zero;
			try 
			{
				pf = Utilities.fopen(fileName, "wt");
				if (pf != IntPtr.Zero)
				{
					if (mpfr_out_str(pf, radix, numberDigits, operand.number, round)==0)
						throw new IOException("Cannot write to file: " + fileName); 
				}
				else throw new System.IO.IOException("Could not create file: " + fileName);
			}
			finally 
			{
				if (pf != IntPtr.Zero) 
					if (Utilities.fclose(pf)!= 0) 
						throw new System.IO.IOException("Could not close file: " + fileName);
			}				
		}


		public static void Open (FloatMpfr result, String fileName) 
		{
			Open(result, fileName, -10, FloatMpfr.DefaultRoundMode);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="fileName">Fully qulified path to file.</param>
		/// <param name="radix">The argument base may be in the ranges 2 to 36.</param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		/// <exception cref="IOException"></exception>
		public static void Open (FloatMpfr result, String fileName, int radix)
		{
			Open(result, fileName, radix, FloatMpfr.DefaultRoundMode);
		}

		/// <summary>
		/// Input a string in base radix from text file fileName, rounded in direction round, 
		/// and put the read float in result. The string is of the form M@N or, 
		/// if the base is 10 or less, alternatively MeN or MEN, or, if the base is 16, 
		/// alternatively MpB or MPB. M is the mantissa in the specified base, N is the exponent 
		/// written in decimal for the specified base, and in base 16, 
		/// B is the binary exponent written in decimal (i.e. it indicates the power of 2 
		/// by which the mantissa is to be scaled). Special values can be read as follows 
		/// (the case does not matter): @NaN@, @Inf@, +@Inf@ and -@Inf@, possibly followed by other characters; 
		/// if the base is smaller or equal to 16, the following strings are accepted too: 
		/// NaN, Inf, +Inf and -Inf. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="fileName"></param>
		/// <param name="radix">The argument base may be in the range 2 to 36.</param>
		/// <param name="round"></param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		/// <exception cref="IOException"></exception>
		public static void Open (FloatMpfr result, String fileName, int radix, RoundMode round)
		{
			if (fileName == null ) 
				throw new ArgumentNullException("fileName", 
					"Argument is null. Fully qualified path to file expected.");

			if (result == null ) 
				throw new ArgumentNullException("result", 
					"Argument is null. Expected valid FloatMpfr.");

#if NET_2_0
			if (fileName.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
#else
			if (fileName.IndexOfAny(Path.InvalidPathChars) >= 0) 
#endif
				throw new ArgumentException("Invalid characters in path: " + fileName, "fileName");

			if (!File.Exists(fileName)) 
				throw new FileNotFoundException("Specified file does not exist.", fileName);

			if (radix < -36 || radix > 36 || (radix > -2 && radix < 2) )
				throw new ArgumentOutOfRangeException("radix",
					"Argument radix is out of range. Accepted values are from 2 to 36 or -36 to -2 inclusive.");

			IntPtr pf = IntPtr.Zero;
			try 
			{
				pf = Utilities.fopen(fileName, "rt");
				if (pf != IntPtr.Zero)
				{
					if (mpfr_inp_str(result.number, pf, radix, round)== 0)
						throw new IOException("Cannot read file: " + fileName); 
				}
				else throw new System.IO.IOException("Could not open file: " + fileName);
			}
			finally 
			{
				if (pf != IntPtr.Zero) 
					if (Utilities.fclose(pf)!= 0) 
						throw new System.IO.IOException("Could not close file: " + fileName);
			}				
		}



		#endregion IO

		#region Random

		/// <summary>
		/// Generate a uniformly distributed random float in the interval  1 > result >= 0. 
		/// Return 0, unless the exponent is not in the current exponent range, 
		/// in which case result is set to NaN and a non-zero value is returned.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="state"></param>
		/// <returns></returns>
		public static int Random (FloatMpfr result, RandState state) 
		{
			return mpfr_urandomb(result.number, state);
		}

		/// <summary>
		/// Generate a random float of at most size limbs, with long strings 
		/// of zeros and ones in the binary representation. The exponent of the number 
		/// is in the interval -exp to exp. This function is useful for testing 
		/// functions and algorithms, since this kind of random numbers have proven 
		/// to be more likely to trigger corner-case bugs. Negative random numbers 
		/// are generated when size is negative.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="size"></param>
		/// <param name="exp"></param>
		/// <returns></returns>
		public static void Random (FloatMpfr result, uint size, int exp) 
		{
			mpfr_random2(result.number, size, exp);
		}

		#endregion Random

		#region Miscallenous

		/// <summary>
		/// Return true if operand would fit in the respective CLI data type, 
		/// when rounded to an integer in the direction round.
		/// </summary>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static bool FitsUInt32 (FloatMpfr operand, RoundMode round) 
		{
			if (mpfr_fits_ulong_p(operand.number, round) != 0) return true;
			else return false;
		}

		public static bool FitsInt32 (FloatMpfr operand, RoundMode round) 
		{
			if (mpfr_fits_slong_p(operand.number, round) != 0) return true;
			else return false;
		}

		public static bool FitsInt16 (FloatMpfr operand, RoundMode round) 
		{
			if (mpfr_fits_sshort_p(operand.number, round) != 0) return true;
			else return false;
		}

		public static bool FitsUInt16 (FloatMpfr operand, RoundMode round) 
		{
			if (mpfr_fits_ushort_p(operand.number, round) != 0) return true;
			else return false;
		}

		/// <summary>
		/// Set result to operand rounded to an integer. Ceil rounds to the next higher or equal representable integer.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int Ceil (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_rint(result.number, operand.number, RoundMode.PlusInfinity);
		}

		/// <summary>
		/// Set result to operand rounded to an integer. Floor rounds to the next lower or equal representable integer.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int Floor (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_rint(result.number, operand.number, RoundMode.MinusInfinity);
		}

		/// <summary>
		/// Set result to operand rounded to an integer. Trunc rounds to the next representable integer towards zero.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int Trunc (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_rint(result.number, operand.number, RoundMode.Zero);
		}

		/// <summary>
		/// Set result to operand rounded to an integer. Round rounds to the nearest 
		/// representable integer, rounding halfway cases away from zero
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int Round (FloatMpfr result, FloatMpfr operand) 
		{
			return mpfr_rint(result.number, operand.number, RoundMode.Nearest);
		}

		/// <summary>
		/// Set result to operand rounded to an integer. Round rounds to the nearest 
		/// representable integer in the given rounding mode
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Round (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_rint(result.number, operand.number, round);
		}

		/// <summary>
		/// Set result to the fractional part of operand, having the same sign as operand, 
		/// rounded in the direction round.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Frac (FloatMpfr result, FloatMpfr operand, RoundMode round) 
		{
			return mpfr_frac(result.number, operand.number, round);
		}

		public static bool IsInteger (FloatMpfr operand) 
		{
			if (mpfr_integer_p(operand.number) != 0) return true;
			else return false;
		}

		/// <summary>
		/// If x or y is NaN, set x to NaN. Otherwise, if x is different from y, 
		/// replace x by the next floating-point number (with the precision of x 
		/// and the current exponent range) in the direction of y, if there is one 
		/// (the infinite values are seen as the smallest and largest 
		/// floating-point numbers). If the result is zero, it keeps the same sign. 
		/// No underflow or overflow is generated.
		/// </summary>
		/// <param name="x"></param>
		/// <param name="y"></param>
		public static void NextToward (FloatMpfr x, FloatMpfr y) 
		{
			mpfr_nexttoward(x.number, y.number);
		}

		/// <summary>
		/// Equivalent to NextToward where y is plus infinity.
		/// </summary>
		/// <param name="x"></param>
		public static void NextAbove (FloatMpfr x)
		{
			mpfr_nextabove(x.number);
		}

		/// <summary>
		/// Equivalent to NextToward where y is minus infinity.
		/// </summary>
		/// <param name="x"></param>
		public static void NextBelow (FloatMpfr x) 
		{
			mpfr_nextbelow(x.number);
		}

		/// <summary>
		/// Set result to the minimum of operand1 and operand2. If operand1 and operand2 are both NaN, 
		/// then result is set to NaN. If operand1 or operand2 is NaN, then result is set to 
		/// the numeric value. If operand1 and operand2 are zeros of different signs, then result is set to -0.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Min (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round) 
		{
			return mpfr_min(result.number, operand1.number, operand2.number, round);
		}

		/// <summary>
		/// Set result to the maximum of operand1 and operand2. If operand1 and operand2 are both NaN, 
		/// then result is set to NaN. If operand1 or operand2 is NaN, then result is set to 
		/// the numeric value. If operand1 and operand2 are zeros of different signs, 
		/// then result is set to +0. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="round"></param>
		/// <returns></returns>
		public static int Max (FloatMpfr result, FloatMpfr operand1, FloatMpfr operand2, RoundMode round) 
		{
			return mpfr_max(result.number, operand1.number, operand2.number, round);
		}

		#endregion Miscallenous

		#endregion Static Methods

		#region Instance Methods

		public void Set (FloatMpfr value) 
		{
			int sign = mpfr_cmp_ui_2exp(this.number, 0, 0);
			mpfr_set4(this.number, value.number, FloatMpfr.DefaultRoundMode, sign);
		}

		public void Set (FloatMpfr value, RoundMode round) 
		{
			int sign = mpfr_cmp_ui_2exp(this.number, 0, 0);
			mpfr_set4(this.number, value.number, round, sign);
		}

		public void Set (uint value) 
		{
			mpfr_set_ui(this.number, value, FloatMpfr.DefaultRoundMode);
		}

		public void Set (uint value, RoundMode round) 
		{
			mpfr_set_ui(this.number, value, round);
		}

		public void Set (int value) 
		{
			mpfr_set_si(this.number, value, FloatMpfr.DefaultRoundMode);
		}

		public void Set (int value, RoundMode round) 
		{
			mpfr_set_si(this.number, value, round);
		}

		public void Set (double value) 
		{
			mpfr_set_d(this.number, value, FloatMpfr.DefaultRoundMode);
		}

		public void Set (double value, RoundMode round) 
		{
			mpfr_set_d(this.number, value, round);
		}

		public void Set (IntMP value) 
		{
			mpfr_set_z(this.number, value.number, FloatMpfr.DefaultRoundMode);
		}

		public void Set (IntMP value, RoundMode round) 
		{
			mpfr_set_z(this.number, value.number, round);
		}

		public void Set (RatnlMP value) 
		{
			mpfr_set_q(this.number, value.number, FloatMpfr.DefaultRoundMode);
		}

		public void Set (RatnlMP value, RoundMode round) 
		{
			mpfr_set_q(this.number, value.number, round);
		}

		public void Set (FloatMP value) 
		{
			mpfr_set_f(this.number, value.number, FloatMpfr.DefaultRoundMode);
		}

		public void Set (FloatMP value, RoundMode round) 
		{
			mpfr_set_f(this.number, value.number, round);
		}

		public void Set (String value) 
		{
			if (0 != mpfr_set_str(this.number, value, 10, FloatMpfr.DefaultRoundMode))
				throw new ArgumentException(String.Format(
					"Could not parse string \"{0}\".", value));
		}

		public void Set (String value, RoundMode round) 
		{
			if (0 != mpfr_set_str(this.number, value, 10, round))
				throw new ArgumentException(String.Format(
					"Could not parse string \"{0}\" with RoundMode {1}.", value, round));
			
		}

		public void Set (String value, int radix) 
		{
			if (0 != mpfr_set_str(this.number, value, radix, FloatMpfr.DefaultRoundMode))
				throw new ArgumentException(String.Format(
					"Could not parse string \"{0}\" with radix {1}.", value, radix));

		}

		public void Set (String value, int radix, RoundMode round)
		{
			if (0 != mpfr_set_str(this.number, value, 10, round))
				throw new ArgumentException(String.Format(
					"Could not parse string \"{0}\" with radix {1} and RoundMode {2}.", 
					value, radix, round));

		}

		/// <summary>
		/// Set this FloatMpfr instance to infinity with sign indicated by sign.
		/// </summary>
		/// <param name="sign">Plus infinity is indicated by sign >= 0.</param>
		public void SetInfinity (int sign) 
		{
			mpfr_set_inf(this.number, sign);
		}

		#endregion Instance Methods

		#region Operators Overloads

		public static FloatMpfr operator +(FloatMpfr operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator +(FloatMpfr operand1, uint operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add_ui(result.number, operand1.number, operand2, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator +(uint operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add_ui(result.number, operand2.number, operand1, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator +(FloatMpfr operand1, IntMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add_z(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator +(IntMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add_z(result.number, operand2.number, operand1.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator +(FloatMpfr operand1, RatnlMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add_q(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator +(RatnlMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_add_q(result.number, operand2.number, operand1.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(FloatMpfr operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_sub(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(FloatMpfr operand1, uint operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_sub_ui(result.number, operand1.number, operand2, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(uint operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_ui_sub(result.number, operand1, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(FloatMpfr operand1, IntMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_sub_z(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(IntMP operand1, FloatMpfr operand2) 
		{
			FloatMpfr result = new FloatMpfr();
			IntMP.__gmpz_neg(operand1.number, operand1.number);
			mpfr_add_z(result.number, operand2.number, operand1.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(FloatMpfr operand1, RatnlMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_sub_q(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(RatnlMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			RatnlMP.__gmpq_neg(operand1.number, operand1.number);
			mpfr_sub_q(result.number, operand2.number, operand1.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator -(FloatMpfr operand) 
		{
			mpfr_neg(operand.number, operand.number, FloatMpfr.DefaultRoundMode);
			return operand;
		}
		public static FloatMpfr operator *(FloatMpfr operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator *(FloatMpfr operand1, uint operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul_ui(result.number, operand1.number, operand2, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator *(uint operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul_ui(result.number, operand2.number, operand1, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator *(FloatMpfr operand1, IntMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul_z(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator *(IntMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul_z(result.number, operand2.number, operand1.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator *(FloatMpfr operand1, RatnlMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul_q(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator *(RatnlMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_mul_q(result.number, operand2.number, operand1.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(FloatMpfr operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_div(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(uint operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_ui_div(result.number, operand1, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(FloatMpfr operand1, uint operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_div_ui(result.number, operand1.number, operand2, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(FloatMpfr operand1, IntMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_div_z(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(IntMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr(operand1);
			mpfr_div(result.number, result.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(FloatMpfr operand1, RatnlMP operand2)
		{
			FloatMpfr result = new FloatMpfr();
			mpfr_div_q(result.number, operand1.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		public static FloatMpfr operator /(RatnlMP operand1, FloatMpfr operand2)
		{
			FloatMpfr result = new FloatMpfr(operand1);
			mpfr_div(result.number, result.number, operand2.number, FloatMpfr.DefaultRoundMode);
			return result;
		}

		//
		// TODO: modulus operators are defined for double/float in C# - try getting them implemented here as well
		//

		public static bool operator <(FloatMpfr operand1, FloatMpfr operand2)
		{
			if ( mpfr_less_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator <(FloatMpfr operand1, int operand2)
		{
			if ( mpfr_cmp_si_2exp(operand1.number, operand2, 0) < 0) return true;
			else return false;
		}

		public static bool operator <(int operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_si_2exp(operand2.number, operand1, 0) > 0) return true;
			else return false;
		}

		public static bool operator <(FloatMpfr operand1, uint operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand1.number, operand2, 0) < 0) return true;
			else return false;
		}

		public static bool operator <(uint operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand2.number, operand1, 0) > 0) return true;
			else return false;
		}

		public static bool operator <(FloatMpfr operand1, double operand2)
		{
			if ( mpfr_cmp_d(operand1.number, operand2) < 0) return true;
			else return false;
		}

		public static bool operator <(double operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_d(operand2.number, operand1) > 0) return true;
			else return false;
		}

		public static bool operator >(FloatMpfr operand1, FloatMpfr operand2)
		{
			if ( mpfr_greater_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator >(FloatMpfr operand1, int operand2)
		{
			if ( mpfr_cmp_si_2exp(operand1.number, operand2, 0) > 0) return true;
			else return false;
		}

		public static bool operator >(int operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_si_2exp(operand2.number, operand1, 0) < 0) return true;
			else return false;
		}

		public static bool operator >(FloatMpfr operand1, uint operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand1.number, operand2, 0) > 0) return true;
			else return false;
		}

		public static bool operator >(uint operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand2.number, operand1, 0) < 0) return true;
			else return false;
		}

		public static bool operator >(FloatMpfr operand1, double operand2)
		{
			if ( mpfr_cmp_d(operand1.number, operand2) > 0) return true;
			else return false;
		}

		public static bool operator >(double operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_d(operand2.number, operand1) < 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMpfr operand1, FloatMpfr operand2)
		{
			if ( mpfr_lessequal_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMpfr operand1, int operand2)
		{
			if ( mpfr_cmp_si_2exp(operand1.number, operand2, 0) <= 0) return true;
			else return false;
		}

		public static bool operator <=(int operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_si_2exp(operand2.number, operand1, 0) >= 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMpfr operand1, uint operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand1.number, operand2, 0) <= 0) return true;
			else return false;
		}

		public static bool operator <=(uint operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand2.number, operand1, 0) >= 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMpfr operand1, double operand2)
		{
			if ( mpfr_cmp_d(operand1.number, operand2) <= 0) return true;
			else return false;
		}

		public static bool operator <=(double operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_d(operand2.number, operand1) >= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMpfr operand1, FloatMpfr operand2)
		{
			if ( mpfr_greaterequal_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMpfr operand1, int operand2)
		{
			if ( mpfr_cmp_si_2exp(operand1.number, operand2, 0) >= 0) return true;
			else return false;
		}

		public static bool operator >=(int operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_si_2exp(operand2.number, operand1, 0) <= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMpfr operand1, uint operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand1.number, operand2, 0) >= 0) return true;
			else return false;
		}

		public static bool operator >=(uint operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand2.number, operand1, 0) <= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMpfr operand1, double operand2)
		{
			if ( mpfr_cmp_d(operand1.number, operand2) >= 0) return true;
			else return false;
		}

		public static bool operator >=(double operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_d(operand2.number, operand1) <= 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMpfr operand1, FloatMpfr operand2)
		{
			if ( mpfr_equal_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMpfr operand1, int operand2)
		{
			if ( mpfr_cmp_si_2exp(operand1.number, operand2, 0) == 0) return true;
			else return false;
		}

		public static bool operator ==(int operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_si_2exp(operand2.number, operand1, 0) == 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMpfr operand1, uint operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand1.number, operand2, 0) == 0) return true;
			else return false;
		}

		public static bool operator ==(uint operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand2.number, operand1, 0) == 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMpfr operand1, double operand2)
		{
			if ( mpfr_cmp_d(operand1.number, operand2) == 0) return true;
			else return false;
		}

		public static bool operator ==(double operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_d(operand2.number, operand1) == 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMpfr operand1, FloatMpfr operand2)
		{
			if ( mpfr_lessgreater_p(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMpfr operand1, int operand2)
		{
			if ( mpfr_cmp_si_2exp(operand1.number, operand2, 0) != 0) return true;
			else return false;
		}

		public static bool operator !=(int operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_si_2exp(operand2.number, operand1, 0) != 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMpfr operand1, uint operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand1.number, operand2, 0) != 0) return true;
			else return false;
		}

		public static bool operator !=(uint operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_ui_2exp(operand2.number, operand1, 0) != 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMpfr operand1, double operand2)
		{
			if ( mpfr_cmp_d(operand1.number, operand2) != 0) return true;
			else return false;
		}

		public static bool operator !=(double operand1, FloatMpfr operand2)
		{
			if ( mpfr_cmp_d(operand2.number, operand1) != 0) return true;
			else return false;
		}


		public static explicit operator FloatMpfr(byte operand) 
		{
			return new FloatMpfr((uint)operand);
		}

		public static explicit operator FloatMpfr(sbyte operand) 
		{
			return new FloatMpfr((int)operand);
		}

		public static explicit operator FloatMpfr(char operand) 
		{
			return new FloatMpfr((uint)operand);
		}

		public static explicit operator FloatMpfr(short operand) 
		{
			return new FloatMpfr((int)operand);
		}

		public static explicit operator FloatMpfr(ushort operand) 
		{
			return new FloatMpfr((uint)operand);
		}

		public static explicit operator FloatMpfr(uint operand) 
		{
			return new FloatMpfr(operand);
		}

		public static explicit operator FloatMpfr(int operand) 
		{
			return new FloatMpfr(operand);
		}

		public static explicit operator FloatMpfr(float operand) 
		{
			return new FloatMpfr((double)operand);
		}

		public static explicit operator FloatMpfr(double operand) 
		{
			return new FloatMpfr(operand);
		}

		public static explicit operator FloatMpfr(long operand) 
		{
			// TODO : provide final implementation
			return new FloatMpfr(new IntMP(operand));
		}

		public static explicit operator FloatMpfr(ulong operand) 
		{
			// TODO : provide final implementation
			return new FloatMpfr(new IntMP(operand));
		}

		public static explicit operator FloatMpfr(decimal operand) 
		{
			// TODO : provide final implementation
			return new FloatMpfr((IntMP)operand);
		}

		public static explicit operator FloatMpfr(IntMP operand) 
		{
			return new FloatMpfr(operand);
		}

		public static explicit operator FloatMpfr(RatnlMP operand) 
		{
			return new FloatMpfr(operand);
		}

		public static explicit operator FloatMpfr(FloatMP operand) 
		{
			return new FloatMpfr(operand);
		}


		public static explicit operator byte(FloatMpfr operand)
		{
			return ConvertMP.ToByte(operand);
		}

		public static explicit operator sbyte(FloatMpfr operand)
		{
			return ConvertMP.ToSByte(operand);
		}

		public static explicit operator char(FloatMpfr operand)
		{
			return ConvertMP.ToChar(operand);
		}

		public static explicit operator short(FloatMpfr operand)
		{
			return ConvertMP.ToInt16(operand);
		}

		public static explicit operator ushort(FloatMpfr operand)
		{
			return ConvertMP.ToUInt16(operand);
		}

		public static explicit operator uint(FloatMpfr operand)
		{
			return ConvertMP.ToUInt32(operand);
		}

		public static explicit operator int(FloatMpfr operand)
		{
			return ConvertMP.ToInt32(operand);
		}

		public static explicit operator float(FloatMpfr operand)
		{
			return ConvertMP.ToSingle(operand);
		}

		public static explicit operator double(FloatMpfr operand)
		{
			return ConvertMP.ToDouble(operand);
		}


		#endregion Operators Overloads

	}

	#endregion Class FloatMpfr

	#region Class mpfr_t

	// gmp.h

	//	#ifdef __GMP_SHORT_LIMB
	//		typedef unsigned int		mp_limb_t;
	//		typedef int			mp_limb_signed_t;
	//	#else
	//	#ifdef _LONG_LONG_LIMB
	//		typedef unsigned long long int	mp_limb_t;
	//		typedef long long int		mp_limb_signed_t;
	//	#else
	//		typedef unsigned long int	mp_limb_t;
	//		typedef long int		mp_limb_signed_t;
	//	#endif
	//	#endif

	//	#if defined (_CRAY) && ! defined (_CRAYMPP)
	//	/* plain `int' is much faster (48 bits) */
	//	#define __GMP_MP_SIZE_T_INT     1
	//	typedef int			mp_size_t;
	//	typedef int			mp_exp_t;
	//	#else
	//	#define __GMP_MP_SIZE_T_INT     0
	//	typedef long int		mp_size_t;
	//	typedef long int		mp_exp_t;
	//	#endif

	// mpfr.h

	//	typedef unsigned long int mp_prec_t; /* easy to change if necessary */

	//	typedef struct 
	//			{
	//				mp_prec_t _mpfr_prec;	/* WARNING : for the mpfr type, the precision */
	//										/* should be understood as the number of BITS,*/
	//										/* not the number of mp_limb_t's. This means  */
	//										/* that the corresponding number of allocated
	//										limbs is >= ceil(_mp_prec/BITS_PER_MP_LIMB) */
	//				mp_size_t _mpfr_size;         /* MPFR_ABSSIZE(.) is the number of allocated
	//												limbs the field _mp_d points to.
	//												The sign is that of _mpfr_size.
	//												The number 0 is such that _mp_d[k-1]=0
	//												where k = ceil(_mp_prec/BITS_PER_MP_LIMB) */
	//				mp_exp_t _mpfr_exp;
	//				mp_limb_t *_mpfr_d;
	//			}
	//	__mpfr_struct;
	//
	//	/*
	//	   The number represented is
	//
	//		sign(_mpfr_size)*(_mpfr_d[k-1]/B+_mpfr_d[k-2]/B^2+...+_mpfr_d[0]/B^k)*2^_mpfr_exp
	//
	//	   where k=ceil(_mp_prec/BITS_PER_MP_LIMB) and B=2^BITS_PER_MP_LIMB.
	//
	//	   For the msb (most significant bit) normalized representation, we must have
	//	   _mpfr_d[k-1]>=B/2, unless the number is zero (in that case its sign is still
	//	   given by sign(_mpfr_size)).
	//
	//	   We must also have the last k*BITS_PER_MP_LIMB-_mp_prec bits set to zero.
	//	*/

	/// <summary>
	/// Class mpfr_t is a managed equivalent of unmanaged struct with this same name.
	/// </summary>
	[StructLayout(LayoutKind.Explicit)]
	public class mpfr_t
	{
		// unsigned long int on everything
		[FieldOffset(0)] 
		public uint _mpfr_prec;

		// long int on everything except Cray
		[FieldOffset(4)] 
		public int _mpfr_size;	
		
		// long int on everything except Cray
		[FieldOffset(8)] 
		public int _mpfr_exp;

		[FieldOffset(12)] 
		public IntPtr _mpfr_d;
	}

	#endregion Class mpfr_t

	#region Enumeration RoundMode

	public enum RoundMode : short
	{
		Nearest = 0,
		Zero,
		PlusInfinity,
		MinusInfinity
	}

	#endregion Enumeration RoundMode

}
