/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;

// Check if preprocessor directives work as expected
#if x86_64
#if ! x86_64
#error
#endif
#endif

namespace NGmp.Math 
{

	#region Class IntMP

	/// <summary>
	/// Class IntMP provides arbitrary precision integer arithmetic over 
	/// IntMP numbers. Class provides CLI framework bindings
	/// for GNU MP library (GMP) integers: http://www.gmplib.org/.
	/// Supported GNU MP version = 4.1.3 - 4.1.4
	/// </summary>
	[Serializable]
	[SuppressUnmanagedCodeSecurity]
	public sealed class IntMP : 
		ICloneable, 
		IComparable, 
		IDisposable, 
		ISerializable,
		IConvertible,				// Duplicate due to IConvertibleMP
		IConvertibleMP
#if NET_2_0 
		,
		IComparable<IntMP>,
		IEquatable<IntMP>
#endif
	{
		#region Fields

		private static int mMpzCoreNativeSize = Marshal.SizeOf(typeof(mpz_t));
		internal IntPtr number = IntPtr.Zero;
		private bool disposed = false;

		#endregion Fields

		#region Constructors & Dispose Methods

		#region Constructors

		/// <summary>
		/// Internal initialization of IntMP.
		/// </summary>
		/// <param name="o">Dummy parameter to support overload resolution.</param>
		/// <exception cref="System.OutOfMemoryException"></exception>
		private IntMP(Object o)
		{
			this.number = Marshal.AllocHGlobal(mMpzCoreNativeSize);
			if (this.number == IntPtr.Zero)
			{
				// Unless we use runtime supplied allocation function we will reach
				// this code (runtime should have thrown OutOfMemory Exception on it's own already).
				throw new OutOfMemoryException();
			}
		}

		/// <summary>
		/// IntMP is created with value initialized to 0.
		/// </summary>
		public IntMP() : this((Object)null)
		{
			__gmpz_init_set_ui(this.number, 0);
		}
		
		public IntMP(int integer) : this((Object)null)
		{
			__gmpz_init_set_si(this.number, integer);
		}

		public IntMP(IntMP integer) : this((Object)null)
		{
			__gmpz_init_set(this.number, integer.number);
		}

		public IntMP(RatnlMP integer) : this((Object)null) 
		{
			__gmpz_set_q(this.number, integer.number);
		}

		public IntMP(FloatMP integer) : this((Object)null)
		{
			__gmpz_set_f(this.number, integer.number);
		}

		public IntMP(uint integer) : this((Object)null)
		{
			 __gmpz_init_set_ui(this.number, integer);
		}

		public IntMP(double integer) : this((Object)null)
		{
			__gmpz_init_set_d(this.number, integer);
		}

		/// <summary>
		/// Creates IntMP and sets it's value from String integer. Number is assumed to be in decimal base unless
		/// leading characters are used: 0x and 0X for hexadecimal, 0b and 0B for binary, 0 for octal.
		/// </summary>
		/// <param name="integer"></param>
		/// <exception cref="System.ArgumentException"></exception>
		public IntMP(String integer) : this(integer, 0)
		{
		}

		/// <summary>
		/// Set the value of created IntMP from String integer in base radix. White space is allowed
		/// in the string, and is simply ignored. The base may vary from 2 to 62, or if base is 0, 
		/// then the leading characters are used: 0x and 0X for hexadecimal, 0b and 0B for binary, 
		/// 0 for octal, or decimal otherwise. For bases up to 36, case is ignored; upper-case 
		/// and lower-case letters have the same value. For bases 37 to 62, upper-case letter 
		/// represent the usual 10..35 while lower-case letter represent 36..61.
		/// </summary>
		/// <param name="integer"></param>
		/// <param name="radix"></param>
		/// <exception cref="System.ArgumentException"></exception>
		/// <exception cref="System.ArgumentOutOfRangeException"></exception>
		public IntMP(String integer, int radix)
		{
			if ((radix >= 2 && radix <= 62) || (radix == 0))
			{
				this.number = Marshal.AllocHGlobal(mMpzCoreNativeSize);
				if (__gmpz_init_set_str(this.number, integer, radix) == 0) return;
				else
				{
					throw new ArgumentException(
						String.Format("In IntMP(String integer, int radix) argument String integer has a bad format in base {0}.", radix),
						"integer");
				}
			}
			else
			{
				throw new ArgumentOutOfRangeException("radix", (object)radix,
												 "IntMP(String integer, int radix) argument radix is outside of accepted value range."
												 + "\nAccepted values are in the range from 2 to 62 or 0.");
			}
		}

		public IntMP(ulong integer) : this((Object)null)
		{
#if x86_64
			__gmpz_init_set_ui(this.number, integer);
#else
			uint opL = unchecked ((uint)(integer & 0xffffffff));
			uint opH = (uint) ((integer & 0xffffffff00000000) >> 32);
			__gmpz_init_set_ui(this.number, opH);
			IntMP.__gmpz_mul_2exp(this.number, this.number, 32);
			IntMP.__gmpz_add_ui(this.number, this.number, opL);
#endif
			
		}

		public IntMP(long integer) : this((Object)null)
		{
#if x86_64
			__gmpz_init_set_si(this.number, integer);
#else
			ulong integer2 = 0;
			if (integer < 0) 
			{
				integer2 = (ulong) -integer;
			}
			else 
			{
				integer2 = (ulong) integer;
			}
			uint opL = unchecked ((uint)(integer2 & 0xffffffff));
			uint opH = (uint) ((integer2 & 0xffffffff00000000) >> 32);
			__gmpz_init_set_ui(this.number, opH);
			IntMP.__gmpz_mul_2exp(this.number, this.number, 32);
			IntMP.__gmpz_add_ui(this.number, this.number, opL);
			if (integer < 0) IntMP.__gmpz_neg(this.number, this.number);
#endif			
		}

		public IntMP(SerializationInfo info, StreamingContext context)
			: this((Object)null)
		{
			__gmpz_init(this.number);
			byte[] importedMe = (byte[]) info.GetValue("number", typeof(byte[]));
			this.Import(importedMe);
		}

		#endregion Constructors

		#region IDispose Methods
		
		// Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!this.disposed)
			{
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if(disposing)
				{
					// Dispose managed resources.
				}
             
				// Call the appropriate methods to clean up 
				// unmanaged resources here.
				// If disposing is false, 
				// only the following code is executed.
				if (this.number != IntPtr.Zero) 
				{
					__gmpz_clear(this.number);
					Marshal.FreeHGlobal(this.number);
					this.number = IntPtr.Zero;            
				}
			}
			disposed = true;         
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~IntMP()      
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}

		#endregion IDispose Methods

		#endregion Constructors & Dispose Methods

		#region Properties

		/// <summary>
		/// Property Disposed indicates wheather unmanaged memory block holding IntMP value was realeased.
		/// If Disposed is true call to IntMP method or property may return undefined value.
		/// </summary>
		public bool IsDisposed
		{
			get{return this.disposed;}
		}

		/// <summary>
		/// Property MPNumber provides direct access via pointer to underlying GMP mpz_t C style structure
		/// storing value of IntMP. Although memory allocated to it is released with Dispose()
		/// call no checks are made inside IntMP for mpz_t memory status due to efficiency improvements. 
		/// It is up to the caller to assure that IntMP object was not disposed by checking value 
		/// returned by IsDiposed property before calling methods.
		/// </summary>
		public IntPtr MPNumber 
		{
			get {return this.number;}
		}

		public mpz_t NativeIntMP
		{
			get 
			{ 
				mpz_t value = null;
				Marshal.PtrToStructure(this.number, value);
				return value;
			}
		}

		#endregion Properties

		#region GMP P/Inovke Prototypes

		#region gmpglue Prototypes

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_mpz();

		// TODO: implement glue for basic gmp data (limb size etc. etc.)

		#endregion gmpglue Prototypes

		#region Initialization & Memory Mgmt

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr integer);
		
#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init2(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr integer, 
			ulong n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_array_init (
			[In, Out,MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)] 
			mpz_t[] integerArray, 
			long array_size, 
			long fixed_num_bits);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_realloc (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr integer, 
			ulong size);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_realloc2 (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr integer, 
			ulong size);		
		
#else

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init2(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr integer, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_array_init (
			[In, Out,MarshalAs(UnmanagedType.LPArray, SizeParamIndex=1)] 
			mpz_t[] integerArray, 
			int array_size, 
			int fixed_num_bits);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_realloc (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr integer, 
			uint size);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_realloc2 (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr integer, 
			uint size);		
		
#endif

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_clear(
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr integer);


		
		// TODO: Implement direct access to gmp allocation functions via pointers/gmpglue
		// 		and use them for memory management instead of those provided by runtime.
		//		In most cases these would be exactly the same ones as those used by runtime 
		//		but ...


		#endregion Initialization & Memory Mngmnt

		#region Assignment Functions

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);
		
#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_ui(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_si(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			long operand);		
#else
	
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_ui(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_si(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int operand);
#endif	

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_d(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand);

		/// <summary>
		/// Method __gmpz_set_q assigns value of mpq_t to [In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr.
		/// </summary>
		/// <param name="result">[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr holding result of assigment.</param>
		/// <param name="operand">mpq_t holding value to be assigned.</param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_q(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Method mpz_set_f assigns value of mpf_t to [In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr.
		/// </summary>
		/// <param name="result">[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr holding result of assigment.</param>
		/// <param name="operand">mpf_t holding value to be assigned.</param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_set_f(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		// Still needs some analysis.
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern int __gmpz_set_str(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.LPStr)]string str, 
			int radix);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_swap(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr rop1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr rop2);

		#endregion Assignment Functions

		#region Initialization & Assignment

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init_set(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_set_mpz(
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init_set_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_set_ui_mpz (
			ulong operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init_set_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			long operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_set_si_mpz (
			long operand);		
		
#else
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init_set_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_set_ui_mpz (
			uint operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init_set_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_set_si_mpz (
			int operand);
#endif		

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_init_set_d (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_init_set_d_mpz (
			[In, MarshalAs(UnmanagedType.R8)] double operand);

//		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
//		internal static extern int __gmpz_init_set_str (
//			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
//			[In, MarshalAs(UnmanagedType.LPStr)] string str, 
//			int radix);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern IntPtr __gmpz_init_set_str_mpz (
			[In, MarshalAs(UnmanagedType.LPStr)] String str, 
			int radix);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern int __gmpz_init_set_str (
			//[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr 
			IntPtr result, 
			[In, MarshalAs(UnmanagedType.LPStr)] String str, 
			int radix);


		#endregion Initialization & Assignment

		#region Conversion

#if x86_64

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_get_ui (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern long __gmpz_get_si (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);
			
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double __gmpz_get_d_2exp (
			long exp, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);	

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, PreserveSig=false)]
		internal static extern ulong __gmpz_getlimbn (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			ulong n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, EntryPoint="__gmpz_getlimbn")]
		internal static extern ulong __gmpz_getlimbn_ui (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			ulong n);
		
#else

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_get_ui (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_get_si (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);
			
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double __gmpz_get_d_2exp (
			int exp, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);	
			
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, PreserveSig=false)]
		internal static extern uint __gmpz_getlimbn (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention = CallingConvention.Cdecl, EntryPoint = "__gmpz_getlimbn")]
		internal static extern uint __gmpz_getlimbn_ui(
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand,
			uint n);
					
#endif
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double __gmpz_get_d (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

//		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
//		internal static extern void __gmpz_get_str (
//			[In, Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder str,
//			int radix, 
//			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern void __gmpz_get_str (
			[In, Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder str,
			int radix, 
			IntPtr operand);



		#endregion Conversion

		#region Arithmetic
		
		
#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_add_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_sub_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_ui_sub (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mul_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			long operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_addmul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);
	
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_submul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mul_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);
		
		// return value uint baypassed - it is remainder of division
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_cdiv_q_ui(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong div);

		// return value uint baypassed - it is remainder of division
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_cdiv_r_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n,
			ulong div);

		// return value uint baypassed - it is remainder of division
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_cdiv_qr_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr num,
			ulong div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_cdiv_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_q_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong be);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_r_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong be);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_fdiv_q_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_fdiv_r_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_fdiv_qr_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr num, 
			ulong dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_fdiv_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_q_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong power);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_r_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong power);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_tdiv_q_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_tdiv_r_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_tdiv_qr_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr num, 
			ulong div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_tdiv_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_q_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong be);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_r_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong be);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, EntryPoint="__gmpz_fdiv_r_ui")]
		internal static extern void __gmpz_mod_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong div);		
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_divexact_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong d);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_divisible_ui_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_divisible_2exp_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong b);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_congruent_ui_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong c, 
			ulong d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_congruent_2exp_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr c, 
			ulong b);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_root (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			ulong n);

		[DllImport(Utilities.mp_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern void __gmpz_powm_ui(
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result,
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr radix,
			ulong exp,
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr mod);

		[DllImport(Utilities.mp_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern void __gmpz_pow_ui(
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result,
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr radix,
			ulong exp);

		[DllImport(Utilities.mp_library, CallingConvention = CallingConvention.Cdecl)]
		internal static extern void __gmpz_ui_pow_ui(
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result,
			ulong radix,
			ulong exp);		
				
#else
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_add_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_sub_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_ui_sub (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);		
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mul_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_addmul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_submul_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mul_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		// return value uint baypassed - it is remainder of division
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_cdiv_q_ui(
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint div);

		// return value uint baypassed - it is remainder of division
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_cdiv_r_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n,
			uint div);

		// return value uint baypassed - it is remainder of division
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_cdiv_qr_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr num,
			uint div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_cdiv_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_q_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint be);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_r_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint be);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_fdiv_q_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_fdiv_r_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_fdiv_qr_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr num, 
			uint dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_fdiv_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_q_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint power);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_r_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint power);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_tdiv_q_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_tdiv_r_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_tdiv_qr_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr num, 
			uint div);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_tdiv_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint dig);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_q_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint be);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_r_2exp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint be);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, EntryPoint="__gmpz_fdiv_r_ui")]
		internal static extern void __gmpz_mod_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint div);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_divexact_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_divisible_ui_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_divisible_2exp_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint b);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_congruent_ui_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint c, 
			uint d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_congruent_2exp_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr c, 
			uint b);	
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_root (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			uint n);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_powm_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr radix, 
			uint exp, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr mod);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_pow_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr radix, 
			uint exp);		
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_ui_pow_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint radix, 
			uint exp);
						
#endif


		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_add (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_sub (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);


//		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
//		internal static extern void __gmpz_mul (
//			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
//			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
//			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		public static extern void __gmpz_mul (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_addmul (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_submul (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_neg (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_abs (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_r (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_cdiv_qr (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_r (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fdiv_qr (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_q (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_r (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_tdiv_qr (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_mod (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr r, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_divexact (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr q, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_divisible_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_congruent_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr c, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr d);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_powm (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr radix, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr exp, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr mod);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_sqrt (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_sqrtrem (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr rop1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr rop2, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_perfect_power_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_perfect_square_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		#endregion Arithmetic

		#region Number Theoretic
		
#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_gcd_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lcm_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_kronecker_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			long b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_kronecker_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			ulong b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_si_kronecker (
			long a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_ui_kronecker (
			ulong a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_remove (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr f);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fac_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_bin_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			ulong k);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_bin_uiui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong n, 
			ulong ki);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fib_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr fn, 
			ulong n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fib2_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr fn, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr fnsub1, 
			ulong n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lucnum_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr ln, 
			ulong n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lucnum2_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr ln, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr lnsub1, 
			ulong n);

		
#else
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_gcd_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);
	
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lcm_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_kronecker_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			int b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_kronecker_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			uint b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_si_kronecker (
			int a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_ui_kronecker (
			uint a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_remove (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr f);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fac_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_bin_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint k);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_bin_uiui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint n, 
			uint ki);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fib_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr fn, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_fib2_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr fn, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr fnsub1, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lucnum_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr ln, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lucnum2_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr ln, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr lnsub1, 
			uint n);

		
#endif		
		

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_probab_prime_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			int reps);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_nextprime (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_gcd (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_gcdext (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr g, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr s, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr t, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_lcm (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_invert (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_jacobi (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_legendre (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr p);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_kronecker (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr b);

		#endregion Number Theoretic

		#region Comparison
		
#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmp_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			long operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmp_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmpabs_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			ulong operand2);

		
#else
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmp_si (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmp_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmpabs_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);
		
#endif		

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmp (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmp_d (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			double operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmpabs (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_cmpabs_d (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			double operand2);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_sign(
			[In, MarshalAs(UnmanagedType.SysInt)] IntPtr number);

		// Declared as macro in gmp - equivalent provided in NGmp.Math
		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_sgn (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		#endregion Comparison
		
		#region Logical & Bit


#if x86_64

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_popcount (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_hamdist (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_scan0 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			ulong starting_bit);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_scan1 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			ulong starting_bit);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_setbit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong bit_index);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_clrbit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong bit_index);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_combit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong bit_index);				
				
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_tstbit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			ulong bit_index);

						
#else

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_popcount (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_hamdist (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_scan0 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			uint starting_bit);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_scan1 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			uint starting_bit);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_setbit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint bit_index);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_clrbit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint bit_index);
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_combit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint bit_index);				
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_tstbit (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, 
			uint bit_index);

		
#endif

		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_and (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_ior (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_xor (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_com (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);


		#endregion Logical & Bit

		#region IO
		
#if x86_64

		/// <summary>
		/// Output operand on stdio stream stream, as a string of digits in base base. 
		/// The base may vary from 2 to 36.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="radix"></param>
		/// <param name="operand"></param>
		/// <returns>Return the number of bytes written, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern ulong __gmpz_out_str (
			[In, MarshalAs(UnmanagedType.SysInt)] IntPtr stream, 
			int radix, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Input a possibly white-space preceded string in base base from 
		/// stdio stream, and put the read integer in result. The base may vary from 2 to 36. 
		/// If base is 0, the actual base is determined from the leading characters: 
		/// if the first two characters are `0x' or `0X', hexadecimal is assumed, 
		/// otherwise if the first character is `0', octal is assumed, 
		/// otherwise decimal is assumed.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="stream"></param>
		/// <param name="radix"></param>
		/// <returns>Return the number of bytes read, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern ulong __gmpz_inp_str (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			System.IntPtr stream, 
			int radix);

		/// <summary>
		/// Output operand on stdio stream stream, in raw binary format. 
		/// The integer is written in a portable format, with 4 bytes of size information, 
		/// and that many bytes of limbs. Both the size and the limbs are written in decreasing 
		/// significance order (i.e., in big-endian). 
		/// The output can be read with mpz_inp_raw.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="operand"></param>
		/// <returns>Return the number of bytes written, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		public static extern ulong __gmpz_out_raw (
			IntPtr stream, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Input from stdio stream stream in the format written by 
		/// __gmpz_out_raw, and put the result in result. 
		/// This routine can read the output from mpz_out_raw also from GMP 1, 
		/// in spite of changes necessary for compatibility between 32-bit and 64-bit machines. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="stream"></param>
		/// <returns>Return the number of bytes read, or if an error occurred, return 0. </returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		public static extern ulong __gmpz_inp_raw (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			IntPtr stream);		
		
#else

		/// <summary>
		/// Output operand on stdio stream stream, as a string of digits in base base. 
		/// The base may vary from 2 to 36.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="radix"></param>
		/// <param name="operand"></param>
		/// <returns>Return the number of bytes written, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern uint __gmpz_out_str (
			[In, MarshalAs(UnmanagedType.SysInt)] IntPtr stream, 
			int radix, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Input a possibly white-space preceded string in base base from 
		/// stdio stream, and put the read integer in result. The base may vary from 2 to 36. 
		/// If base is 0, the actual base is determined from the leading characters: 
		/// if the first two characters are `0x' or `0X', hexadecimal is assumed, 
		/// otherwise if the first character is `0', octal is assumed, 
		/// otherwise decimal is assumed.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="stream"></param>
		/// <param name="radix"></param>
		/// <returns>Return the number of bytes read, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern uint __gmpz_inp_str (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			System.IntPtr stream, 
			int radix);

		/// <summary>
		/// Output operand on stdio stream stream, in raw binary format. 
		/// The integer is written in a portable format, with 4 bytes of size information, 
		/// and that many bytes of limbs. Both the size and the limbs are written in decreasing 
		/// significance order (i.e., in big-endian). 
		/// The output can be read with mpz_inp_raw.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="operand"></param>
		/// <returns>Return the number of bytes written, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		public static extern uint __gmpz_out_raw (
			IntPtr stream, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Input from stdio stream stream in the format written by 
		/// __gmpz_out_raw, and put the result in result. 
		/// This routine can read the output from mpz_out_raw also from GMP 1, 
		/// in spite of changes necessary for compatibility between 32-bit and 64-bit machines. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="stream"></param>
		/// <returns>Return the number of bytes read, or if an error occurred, return 0. </returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		public static extern uint __gmpz_inp_raw (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			IntPtr stream);
		
#endif		

		#endregion IO

		#region Integer Import/Export
		
#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_import (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			ulong count, 
			int order, 
			ulong size, 
			int endian, 
			ulong nails, 
			System.IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_export (
//			[Out, MarshalAs(UnmanagedType.LPArray)] 
			System.IntPtr result,
//			uint result,
			out ulong countp, 
			int order, 
			ulong size, 
			int endian, 
			ulong nails, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);		
		
#else

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_import (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint count, 
			int order, 
			uint size, 
			int endian, 
			uint nails, 
			System.IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern IntPtr __gmpz_export (
//			[Out, MarshalAs(UnmanagedType.LPArray)] 
			System.IntPtr result,
//			uint result,
			out uint countp, 
			int order, 
			uint size, 
			int endian, 
			uint nails, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);
		
#endif		

		#endregion Integer Import/Export

		#region Miscallenous

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_fits_ulong_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_fits_slong_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_fits_uint_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_fits_sint_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_fits_ushort_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_fits_sshort_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_odd_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpz_even_p (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

#if x86_64
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_size (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern ulong __gmpz_sizeinbase (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, int radix);
		
#else
		
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_size (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpz_sizeinbase (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr operand, int radix);
				
		
#endif


		#endregion Miscallenous

		#endregion GMP P/Inovke Prototypes

		#region Object Overrides

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			if (obj is IntMP) 
			{
				return __gmpz_cmp(this.number,((IntMP)obj).number) == 0;
			}
			else throw new ArgumentException(@"Equals argument obj has wrong Type. IntMP expected", "obj");
		}

		public override int GetHashCode()
		{
			mpz_t value = (mpz_t) Marshal.PtrToStructure(this.number, typeof(mpz_t));
			return IntMP.GetHashCode(value.mp_size, value.mp_d);
		}

		internal static int GetHashCode(int mp_size, IntPtr mp_d)
		{
			int limbCount = System.Math.Abs(mp_size);
			int hashVal = 0;
			unsafe
			{
				int* ptr = (int*) mp_d;
				for (int i = 0; i < limbCount; i++, ptr++) hashVal ^= *ptr;
			}
			// Take sign into account
			hashVal ^= mp_size;
			return hashVal;
		}

		/// <summary>
		/// Object ToString() override returning value of IntMP in decimal base.
		/// </summary>
		/// <returns>String containing decimal representation of IntMP value. 
		/// Max String length is equal to Int32.MaxValue 2147483647</returns>
		public override string ToString()
		{
 			return ToString(10);
		}

		/// <summary>
		/// Method ToString(int radix) overload returns string representation of 
		/// IntMP in predefined base.
		/// </summary>
		/// <param name="radix">Parameter Int32 radix sets base of conversion.
		/// Accepted range is 2 - 32 inclusive.</param>
		/// <returns>String containing representation of IntMP value in given base. 
		/// Max String length is equal to Int32.MaxValue 2147483647.</returns>
		public string ToString(int radix) 
		{
			if ( (radix >= -32 && radix <= -2) || (radix >= 2 && radix <= 62) ) {
				
				int capacity = (int) __gmpz_sizeinbase(this.number, radix);
				
				if (capacity < 0) 
					throw new ApplicationException("Something wrong with capacity at IntMP.ToString() - its value is: " + capacity);
				
				StringBuilder result = new StringBuilder(capacity + 2);
				__gmpz_get_str(result, radix, this.number);					
				return result.ToString();
				

			}
			else 
			{
				throw new ArgumentOutOfRangeException("radix", (object) radix,
												 "ToString(int radix) argument radix is outside of accepted value range."
												 + "\nAccepted values are in ranges from -32 to -2 and from 2 to 62.");
			}
		}


		#endregion Object Overrides

		#region IComparable Members

		public int CompareTo(object obj)
		{
			// TODO: implement x86_64 ABI
			if (obj is IntMP) return __gmpz_cmp(this.number, ((IntMP)obj).number);
#if x86_64
			else if (obj is ulong || obj is uint) return __gmpz_cmp_ui(this.number, (ulong)obj);
			else if (obj is long || obj is int) return __gmpz_cmp_si(this.number, (long)obj);
#else
			else if (obj is uint) return __gmpz_cmp_ui(this.number, (uint) obj);
			else if (obj is int) return __gmpz_cmp_si(this.number, (int) obj);
			else if (obj is long) return __gmpz_cmp(this.number, ((IntMP)new IntMP((long) obj)).number);
			else if (obj is ulong) return __gmpz_cmp(this.number, ((IntMP)new IntMP((ulong) obj)).number);
#endif
			else if (obj is double) return __gmpz_cmp_d(this.number, (double)obj);
			else throw new ArgumentException(
			     							String.Format("CompareTo argument obj has wrong Type: {0}. IntMP, int, long, uint, ulong, double expected", obj.GetType() ),
			                                 "obj");
		}

		#endregion

#if NET_2_0
		#region IComparable<> Members

		public int CompareTo(IntMP value)
		{
			return __gmpz_cmp(this.number, value.number);
		}

		#endregion 
#endif

		#region IConvertible Members

		ulong System.IConvertible.ToUInt64(IFormatProvider provider)
		{
			return ConvertMP.ToUInt64(this);
		}

		sbyte System.IConvertible.ToSByte(IFormatProvider provider)
		{
			return ConvertMP.ToSByte(this);
		}

		double System.IConvertible.ToDouble(IFormatProvider provider)
		{
			return ConvertMP.ToDouble(this);
		}

		DateTime System.IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		float System.IConvertible.ToSingle(IFormatProvider provider)
		{
			return ConvertMP.ToSingle(this);
		}

		bool System.IConvertible.ToBoolean(IFormatProvider provider)
		{
			return ConvertMP.ToBoolean(this);
		}

		int System.IConvertible.ToInt32(IFormatProvider provider)
		{
			return ConvertMP.ToInt32(this);
		}

		ushort System.IConvertible.ToUInt16(IFormatProvider provider)
		{
			return ConvertMP.ToUInt16(this);
		}

		short System.IConvertible.ToInt16(IFormatProvider provider)
		{
			return ConvertMP.ToInt16(this);
		}

		public string ToString(IFormatProvider provider)
		{
			return ConvertMP.ToString(this, provider);
		}

		byte System.IConvertible.ToByte(IFormatProvider provider)
		{
			return ConvertMP.ToByte(this);
		}

		char System.IConvertible.ToChar(IFormatProvider provider)
		{
			return ConvertMP.ToChar(this);
		}

		long System.IConvertible.ToInt64(IFormatProvider provider)
		{
			return ConvertMP.ToInt64(this);
		}

		public System.TypeCode GetTypeCode()
		{
			return (System.TypeCode) 19;
		}

		decimal System.IConvertible.ToDecimal(IFormatProvider provider)
		{
			return ConvertMP.ToDecimal(this);
		}

		public object ToType(Type conversionType, IFormatProvider provider)
		{
			return ConvertMP.ToType(this, conversionType, provider) ;
		}

		uint System.IConvertible.ToUInt32(IFormatProvider provider)
		{
			return ConvertMP.ToUInt32(this);
		}

		#endregion IConvertible Members

		#region IConvertibleMP Members

		public IntMP ToIntMP(IFormatProvider provider)
		{
			return this;
		}

		public FloatMP ToFloatMP(IFormatProvider provider)
		{
			return (FloatMP) this;
		}

		public FloatMpfr ToFloatMpfr(IFormatProvider provider)
		{
			return (FloatMpfr) this;
		}

		public RatnlMP ToRatnlMP(IFormatProvider provider)
		{
			return (RatnlMP) this;
		}

		#endregion

		#region ICloneable Members

		public object Clone() 
		{
			return new IntMP(this);
		}

		#endregion ICloneable Members

#if NET_2_0
		#region IEquatable<> Members

		public bool Equals(IntMP value)
		{
			return __gmpz_cmp(this.number, value.number) == 0;
		}

		#endregion IEquatable<> Members
#endif

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("number", this.Export(), typeof(byte[]));
		}

		#endregion

		#region Static Methods

		#region Arithmetic

		public static void Add (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_add(result.number, operand1.number, operand2.number);
		}

		public static void Sub (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_sub(result.number, operand1.number, operand2.number);
		}

		public static void Mul (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_mul(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Set result to result + operand1 * operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void AddMul (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_addmul(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Set result to result - operand1 * operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void SubMul (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_submul(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Set result to -operand.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		public static void Neg (IntMP result, IntMP operand) 
		{
			__gmpz_neg(result.number, operand.number);
		}

		public static void Abs (IntMP result, IntMP operand) 
		{
			__gmpz_abs(result.number, operand.number);
		}
		
#if x86_64


		public static void Add (IntMP result, IntMP operand1, ulong operand2)
		{
			__gmpz_add_ui(result.number, operand1.number, operand2);
		}

		public static void Sub (IntMP result, IntMP operand1, ulong operand2)
		{
			__gmpz_sub_ui(result.number, operand1.number, operand2);
		}

		public static void Sub (IntMP result, ulong operand1, IntMP operand2) 
		{
			__gmpz_ui_sub(result.number, operand1, operand2.number);
		}

		public static void Mul (IntMP result, IntMP operand1, long operand2)
		{
			__gmpz_mul_si(result.number, operand1.number, operand2);
		}

		public static void Mul (IntMP result, IntMP operand1, ulong operand2) 
		{
			__gmpz_mul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to result + operand1 * operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void AddMul (IntMP result, IntMP operand1, ulong operand2)
		{
			__gmpz_addmul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to result - operand1 * operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void SubMul (IntMP result, IntMP operand1, ulong operand2)
		{
			__gmpz_submul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to operand1 2 to power operand2. This operation is equivalent to left shift by operand2 bits.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Mul2Exp (IntMP result, IntMP operand1, ulong operand2)
		{
			__gmpz_mul_2exp(result.number, operand1.number, operand2);
		}
		
		
#else

		public static void Add (IntMP result, IntMP operand1, uint operand2)
		{
			__gmpz_add_ui(result.number, operand1.number, operand2);
		}

		public static void Sub (IntMP result, IntMP operand1, uint operand2)
		{
			__gmpz_sub_ui(result.number, operand1.number, operand2);
		}

		public static void Sub (IntMP result, uint operand1, IntMP operand2) 
		{
			__gmpz_ui_sub(result.number, operand1, operand2.number);
		}

		public static void Mul (IntMP result, IntMP operand1, int operand2)
		{
			__gmpz_mul_si(result.number, operand1.number, operand2);
		}

		public static void Mul (IntMP result, IntMP operand1, uint operand2) 
		{
			__gmpz_mul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to result + operand1 * operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void AddMul (IntMP result, IntMP operand1, uint operand2)
		{
			__gmpz_addmul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to result - operand1 * operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void SubMul (IntMP result, IntMP operand1, uint operand2)
		{
			__gmpz_submul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to operand1 2 to power operand2. This operation is equivalent to left shift by operand2 bits.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Mul2Exp (IntMP result, IntMP operand1, uint operand2)
		{
			__gmpz_mul_2exp(result.number, operand1.number, operand2);
		}

		
#endif		

		#region Division

		#region Division with Ceiling Rounding

		public static void DivC (IntMP quotient, IntMP dividend, IntMP divisor)
		{
			__gmpz_cdiv_q (quotient.number, dividend.number, divisor.number);
		}

		public static void DivCRem (IntMP remainder, IntMP dividend, IntMP divisor)
		{
			__gmpz_cdiv_r (remainder.number, dividend.number, divisor.number);
		}

		public static void DivRemC (IntMP quotient, IntMP remainder, IntMP dividend, IntMP divisor)
		{
			__gmpz_cdiv_qr (quotient.number, remainder.number, dividend.number, divisor.number);
		}

#if x86_64


		public static void DivC(IntMP quotient, IntMP dividend, ulong divisor)
		{
			__gmpz_cdiv_q_ui(quotient.number, dividend.number, divisor);
		}

		public static void DivCRem(IntMP remainder, IntMP dividend, ulong divisor)
		{
			__gmpz_cdiv_r_ui(remainder.number, dividend.number, divisor);
		}

		public static void DivRemC(IntMP quotient, IntMP remainder, IntMP dividend, ulong divisor)
		{
			__gmpz_cdiv_qr_ui(quotient.number, remainder.number, dividend.number, divisor);

		}

		/// <summary>
		/// Method DivC returns remainder of the division.
		/// </summary>
		/// <param name="dividend"></param>
		/// <param name="divisor"></param>
		/// <returns></returns>
		public static ulong DivCRem(IntMP dividend, ulong divisor)
		{
			return __gmpz_cdiv_ui(dividend.number, divisor);
		}

		/// <summary>
		/// Method returns remainder and sets result of dividend/divisor division to quotient.
		/// </summary>
		/// <param name="quotient">Quotient of division.</param>
		/// <param name="dividend"></param>
		/// <param name="divisor"></param>
		/// <returns>Remainder of division.</returns>
		public static ulong DivRemC(IntMP quotient, IntMP dividend, ulong divisor)
		{
			return __gmpz_cdiv_q_ui(quotient.number, dividend.number, divisor);
		}

		public static void Div2ExpC(IntMP quotient, IntMP dividend, ulong power)
		{
			__gmpz_cdiv_q_2exp(quotient.number, dividend.number, power);
		}

		public static void DivRem2ExpC(IntMP remainder, IntMP dividend, ulong power)
		{
			__gmpz_cdiv_r_2exp(remainder.number, dividend.number, power);
		}


#else

		public static void DivC (IntMP quotient, IntMP dividend, uint divisor) 
		{
			__gmpz_cdiv_q_ui (quotient.number, dividend.number, divisor);
		}

		public static void DivCRem (IntMP remainder, IntMP dividend, uint divisor) 
		{
			__gmpz_cdiv_r_ui (remainder.number, dividend.number, divisor);
		}

		public static void DivRemC (IntMP quotient, IntMP remainder, IntMP dividend, uint divisor)
		{
			__gmpz_cdiv_qr_ui (quotient.number, remainder.number, dividend.number, divisor);

		}

		/// <summary>
		/// Method DivC returns remainder of the division.
		/// </summary>
		/// <param name="dividend"></param>
		/// <param name="divisor"></param>
		/// <returns></returns>
		public static uint DivCRem (IntMP dividend, uint divisor) 
		{
			return __gmpz_cdiv_ui (dividend.number, divisor);
		}
		
		/// <summary>
		/// Method returns remainder and sets result of dividend/divisor division to quotient.
		/// </summary>
		/// <param name="quotient">Quotient of division.</param>
		/// <param name="dividend"></param>
		/// <param name="divisor"></param>
		/// <returns>Remainder of division.</returns>
		public static uint DivRemC (IntMP quotient, IntMP dividend, uint divisor)
		{
			return __gmpz_cdiv_q_ui (quotient.number, dividend.number, divisor);
		}

		public static void Div2ExpC (IntMP quotient, IntMP dividend, uint power)
		{
			__gmpz_cdiv_q_2exp(quotient.number, dividend.number, power);
		}

		public static void DivRem2ExpC (IntMP remainder, IntMP dividend, uint power)
		{
			__gmpz_cdiv_r_2exp(remainder.number, dividend.number, power);
		}

#endif

		#endregion Division with Ceiling Rounding

		#region Division with Floor Rounding
 
		public static void DivF (IntMP quotient, IntMP dividend, IntMP divisor) 
		{
			__gmpz_fdiv_q(quotient.number, dividend.number, divisor.number);
		}

		public static void DivFRem (IntMP remainder, IntMP dividend, IntMP divisor) 
		{
			__gmpz_fdiv_r(remainder.number, dividend.number, divisor.number);
		}

		public static void DivRemF (IntMP quotient, IntMP remainder, IntMP dividend, IntMP divisor)
		{
			__gmpz_fdiv_qr(quotient.number, remainder.number, dividend.number, divisor.number);
		}

#if x86_64

		public static void DivF(IntMP quotient, IntMP dividend, ulong divisor)
		{
			__gmpz_fdiv_q_ui(quotient.number, dividend.number, divisor);
		}

		public static void DivFRem(IntMP remainder, IntMP dividend, ulong divisor)
		{
			__gmpz_fdiv_r_ui(remainder.number, dividend.number, divisor);
		}

		public static void DivRemF(IntMP quotient, IntMP remainder, IntMP dividend, ulong divisor)
		{
			__gmpz_fdiv_qr_ui(quotient.number, remainder.number, dividend.number, divisor);
		}

		public static ulong DivFRem(IntMP dividend, ulong divisor)
		{
			return __gmpz_fdiv_ui(dividend.number, divisor);
		}

		public static void Div2ExpF(IntMP quotient, IntMP dividend, ulong power)
		{
			__gmpz_fdiv_q_2exp(quotient.number, dividend.number, power);
		}

		public static void DivRem2ExpF(IntMP remainder, IntMP dividend, ulong power)
		{
			__gmpz_fdiv_r_2exp(remainder.number, dividend.number, power);
		}

#else

		public static void DivF (IntMP quotient, IntMP dividend, uint divisor) 
		{
			__gmpz_fdiv_q_ui(quotient.number, dividend.number, divisor);
		}

		public static void DivFRem (IntMP remainder, IntMP dividend, uint divisor)
		{
			__gmpz_fdiv_r_ui(remainder.number, dividend.number, divisor);
		}

		public static void DivRemF (IntMP quotient, IntMP remainder, IntMP dividend, uint divisor)
		{
			__gmpz_fdiv_qr_ui(quotient.number, remainder.number, dividend.number, divisor);
		}

		public static uint DivFRem (IntMP dividend, uint divisor) 
		{
			return __gmpz_fdiv_ui(dividend.number, divisor);
		}

		public static void Div2ExpF (IntMP quotient, IntMP dividend, uint power) 
		{
			__gmpz_fdiv_q_2exp(quotient.number, dividend.number, power);
		}

		public static void DivRem2ExpF (IntMP remainder, IntMP dividend, uint power) 
		{
			__gmpz_fdiv_r_2exp(remainder.number, dividend.number, power);
		}

#endif

		#endregion Division with Floor Rounding

		#region Division with Truncation

		public static void DivT (IntMP quotient, IntMP dividend, IntMP divisor) 
		{
			__gmpz_tdiv_q(quotient.number, dividend.number, divisor.number);
		}

		public static void DivTRem (IntMP remainder, IntMP dividend, IntMP divisor)
		{
			__gmpz_tdiv_r(remainder.number, dividend.number, divisor.number);
		}

		public static void DivRemT (IntMP quotient, IntMP remainder, IntMP dividend, IntMP divisor)
		{
			__gmpz_tdiv_qr(quotient.number, remainder.number, dividend.number, divisor.number);
		}

#if x86_64

		public static void DivT(IntMP quotient, IntMP dividend, ulong divisor)
		{
			__gmpz_tdiv_q_ui(quotient.number, dividend.number, divisor);
		}

		public static void DivTRem(IntMP remainder, IntMP dividend, ulong divisor)
		{
			__gmpz_tdiv_r_ui(remainder.number, dividend.number, divisor);
		}

		public static void DivRemT(IntMP quotient, IntMP remainder, IntMP dividend, ulong divisor)
		{
			__gmpz_tdiv_qr_ui(quotient.number, remainder.number, dividend.number, divisor);
		}

		public static ulong DivTRem(IntMP dividend, ulong divisor)
		{
			return __gmpz_tdiv_ui(dividend.number, divisor);
		}

		public static void Div2ExpT(IntMP quotient, IntMP dividend, ulong power)
		{
			__gmpz_tdiv_q_2exp(quotient.number, dividend.number, power);
		}

		public static void DivRem2ExpT(IntMP remainder, IntMP dividend, ulong power)
		{
			__gmpz_tdiv_r_2exp(remainder.number, dividend.number, power);
		}


#else

		public static void DivT (IntMP quotient, IntMP dividend, uint divisor) 
		{
			__gmpz_tdiv_q_ui(quotient.number, dividend.number, divisor);
		}

		public static void DivTRem (IntMP remainder, IntMP dividend, uint divisor)
		{
			__gmpz_tdiv_r_ui(remainder.number, dividend.number, divisor);
		}

		public static void DivRemT (IntMP quotient, IntMP remainder, IntMP dividend, uint divisor)
		{
			__gmpz_tdiv_qr_ui(quotient.number, remainder.number, dividend.number, divisor);
		}

		public static uint DivTRem (IntMP dividend, uint divisor) 
		{
			return __gmpz_tdiv_ui(dividend.number, divisor);
		}

		public static void Div2ExpT (IntMP quotient, IntMP dividend, uint power)
		{
			__gmpz_tdiv_q_2exp(quotient.number, dividend.number, power);
		}

		public static void DivRem2ExpT (IntMP remainder, IntMP dividend, uint power) 
		{
			__gmpz_tdiv_r_2exp(remainder.number, dividend.number, power);
		}

#endif

		#endregion Division with Truncation

#if x86_64

		public static void Mod(IntMP result, IntMP dividend, ulong divisor)
		{
			__gmpz_mod_ui(result.number, dividend.number, divisor);
		}

		public static void DivExact(IntMP quotient, IntMP dividend, ulong divisor)
		{
			__gmpz_divexact_ui(quotient.number, dividend.number, divisor);
		}

		public static bool Divisible(IntMP dividend, ulong divisor)
		{
			return __gmpz_divisible_ui_p(dividend.number, divisor) != 0;
		}

		public static bool Divisible2Exp(IntMP dividend, ulong power)
		{
			return __gmpz_divisible_2exp_p(dividend.number, power) != 0;
		}

		public static bool Congruent(IntMP n, ulong c, ulong d)
		{
			return __gmpz_congruent_ui_p(n.number, c, d) != 0;
		}

		public static bool Congruent2Exp(IntMP n, IntMP c, ulong b)
		{
			return __gmpz_congruent_2exp_p(n.number, c.number, b) != 0;
		}

#else

		public static void Mod(IntMP result, IntMP dividend, uint divisor)
		{
			__gmpz_mod_ui(result.number, dividend.number, divisor);
		}

		public static void DivExact (IntMP quotient, IntMP dividend, uint divisor)
		{
			__gmpz_divexact_ui (quotient.number, dividend.number, divisor);
		}

		public static bool Divisible (IntMP dividend, uint divisor)
		{
			return __gmpz_divisible_ui_p(dividend.number, divisor) != 0;
		}

		public static bool Divisible2Exp (IntMP dividend, uint power)
		{
			return __gmpz_divisible_2exp_p(dividend.number, power) != 0;
		}

		public static bool Congruent (IntMP n, uint c, uint d) 
		{
			return __gmpz_congruent_ui_p (n.number, c, d) != 0 ;
		}

		public static bool Congruent2Exp (IntMP n, IntMP c, uint b) 
		{
			return __gmpz_congruent_2exp_p (n.number, c.number, b) != 0 ;
		}

#endif

		public static void Mod (IntMP result, IntMP dividend, IntMP divisor)
		{
			__gmpz_mod (result.number, dividend.number, divisor.number);
		}

		public static void DivExact (IntMP quotient, IntMP dividend, IntMP divisor)
		{
			__gmpz_divexact (quotient.number, dividend.number, divisor.number);
		}

		public static bool Divisible (IntMP dividend, IntMP divisor) 
		{
			return __gmpz_divisible_p(dividend.number, divisor.number) != 0;
		}

		public static bool Congruent (IntMP n, IntMP c, IntMP d) 
		{
			return __gmpz_congruent_p (n.number, c.number, d.number) != 0;
		}

		#endregion Division

		#region Exponentiation

		public static void PowMod (IntMP result, IntMP radix, IntMP exp, IntMP mod) 
		{
			__gmpz_powm(result.number, radix.number, exp.number, mod.number);
		}

#if x86_64
		
		public static void PowMod (IntMP result, IntMP radix, ulong exp, IntMP mod)
		{
			__gmpz_powm_ui(result.number, radix.number, exp, mod.number);
		}
		
		public static void Pow (IntMP result, IntMP radix, ulong exp) 
		{
			__gmpz_pow_ui(result.number, radix.number, exp);
		}		

								
		public static void Pow (IntMP result, ulong radix, ulong exp) 
		{
			__gmpz_ui_pow_ui (result.number, radix, exp);
		}


#else
		
		public static void PowMod (IntMP result, IntMP radix, uint exp, IntMP mod)
		{
			__gmpz_powm_ui(result.number, radix.number, exp, mod.number);
		}
		
		public static void Pow (IntMP result, IntMP radix, uint exp) 
		{
			__gmpz_pow_ui(result.number, radix.number, exp);
		}
		
		public static void Pow (IntMP result, uint radix, uint exp) 
		{
			__gmpz_ui_pow_ui (result.number, radix, exp);
		}

		
#endif

		#endregion Exponentiation

		#region Root Extraction
		
#if x86_64

		public static bool Root (IntMP result, IntMP operand, ulong n) 
		{
			// TODO: check if changing return ype of __gmpz_root to bool
			// would not have side effects and than change PInvoke declarations
			return __gmpz_root (result.number, operand.number, n) != 0 ;
		}		
		
#else

		public static bool Root (IntMP result, IntMP operand, uint n) 
		{
			// TODO: check if changing return ype of __gmpz_root to bool
			// would not have side effects and than change PInvoke declarations
			return __gmpz_root (result.number, operand.number, n) != 0 ;
		}

#endif
		
		public static void Sqrt (IntMP result, IntMP operand) 
		{
			__gmpz_sqrt (result.number, operand.number);
		}

		public static void Sqrt (IntMP result, IntMP remainder, IntMP operand) 
		{
			__gmpz_sqrtrem (result.number, remainder.number, operand.number);
		}

		/// <summary>
		/// Method PerfectPower returns true operand is perfect power i.e. there exists integers
		/// a, b with b > 1 such that operand = a to b.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static bool PerfectPower (IntMP operand) 
		{
			return __gmpz_perfect_power_p(operand.number) != 0 ;
		}

		/// <summary>
		/// Method PerfectSquare returns true when operand is a perfect square i.e
		/// if a square root of operand is integer.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static bool PerfectSquare (IntMP operand) 
		{
			return __gmpz_perfect_square_p (operand.number) != 0 ;
		}


		#endregion Root Extraction

		#endregion Arithmetic

		#region Assignment

		public static void Set(IntMP result, IntMP operand) 
		{
			__gmpz_set(result.number, operand.number);
		}
		
#if x86_64
		
		public static void Set(IntMP result, long operand) 
		{
			__gmpz_set_si(result.number, operand);
		}

		public static void Set(IntMP result, ulong operand)
		{
			__gmpz_set_ui(result.number,  operand);
		}
				
#else
		
		public static void Set(IntMP result, int operand) 
		{
			__gmpz_set_si(result.number, operand);
		}

		public static void Set(IntMP result, uint operand)
		{
			__gmpz_set_ui(result.number,  operand);
		}
		
#endif

		public static void Set(IntMP result, double operand) 
		{
			__gmpz_set_d(result.number, operand);
		}

		public static void Set(IntMP result, FloatMP operand) 
		{
			__gmpz_set_f(result.number, operand.number);
		}

		public static void Set(IntMP result, RatnlMP operand) 
		{
			__gmpz_set_q(result.number, operand.number);
		}

		public static void Swap(IntMP rop1, IntMP rop2) 
		{
			__gmpz_swap(rop1.number, rop2.number);
		}

		#endregion Assignment

		#region Comparison

		public static int Compare (IntMP operand1, IntMP operand2) 
		{
			return __gmpz_cmp(operand1.number, operand2.number);
		}

		public static int Compare (IntMP operand1, double operand2) 
		{
			return __gmpz_cmp_d(operand1.number, operand2);
		}
		
#if x86_64

		public static int Compare (IntMP operand1, long operand2) 
		{
			return __gmpz_cmp_si(operand1.number, operand2);
		}

		public static int Compare (IntMP operand1, ulong operand2) 
		{
			return __gmpz_cmp_ui(operand1.number, operand2);
		}

		public static int CompareAbs (IntMP operand1, ulong operand2) 
		{
			return __gmpz_cmpabs_ui(operand1.number, operand2);
		}


#else
		
		public static int Compare (IntMP operand1, int operand2) 
		{
			return __gmpz_cmp_si(operand1.number, operand2);
		}

		public static int Compare (IntMP operand1, uint operand2) 
		{
			return __gmpz_cmp_ui(operand1.number, operand2);
		}

		public static int CompareAbs (IntMP operand1, uint operand2) 
		{
			return __gmpz_cmpabs_ui(operand1.number, operand2);
		}


#endif

		public static int CompareAbs (IntMP operand1, IntMP operand2) 
		{
			return __gmpz_cmpabs(operand1.number, operand2.number);
		}

		public static int CompareAbs (IntMP operand1, double operand2)
		{
			return __gmpz_cmpabs_d(operand1.number, operand2);
		}

		public static int Sign (IntMP operand) 
		{
			return __gmpz_sign(operand.number);
		}

		#endregion Comparison

		#region Conversion

		/// <summary>
		/// Converts the string representation of a number in 
		/// a specified style and culture-specific format to its IntMP unlimited precision equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <param name="style">The combination of one or more 
		/// System.Globalization.NumberStyles constants that indicates the permitted format of s.</param>
		/// <param name="provider"> An System.IFormatProvider that supplies culture-specific formatting information about s.</param>
		/// <returns>A IntMP unlimited precision signed integer equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static IntMP Parse(String s, System.Globalization.NumberStyles style, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//if (s == null) throw new System.ArgumentNullException("s", "Value of IntMP underlying number cannot be null.");
			//IntMP result = new IntMP();
			//return result;
		}

		/// <summary>
		/// Converts the string representation of a number in 
		/// a specified culture-specific format to its IntMP unlimited precision equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <param name="provider"> An System.IFormatProvider that supplies culture-specific formatting information about s.</param>
		/// <returns>A IntMP unlimited precision signed integer equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static IntMP Parse(String s, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return IntMP.Parse(s, NumberStyles.Integer, provider);
		}

		/// <summary>
		/// Converts the string representation of a number in 
		/// a specified style to its IntMP unlimited precision equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <param name="style">The combination of one or more 
		/// System.Globalization.NumberStyles constants that indicates the permitted format of s.</param>
		/// <returns>A IntMP unlimited precision signed integer equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static IntMP Parse(String s, NumberStyles style) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return IntMP.Parse(s, style, null);
		}

		/// <summary>
		/// Converts the string representation of a number  
		/// to its IntMP unlimited precision equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <returns>A IntMP unlimited precision signed integer equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static IntMP Parse(String s) 
		{
			if (s == null) throw new System.ArgumentNullException("s", "Value of IntMP underlying number cannot be null.");
			//
			// TODO: s format style and format checks
			//
			IntMP result = new IntMP(s);
			return result;
		}
		
		// TODO: implement try parse

		#endregion Conversion

		#region IO

		/// <summary>
		/// Method Save persists IntMP operand value in file using binary or text mode in base 10.
		/// If file already exists its value will be overwritten.
		/// </summary>
		/// <param name="operand">IntMP to be persisited.</param>
		/// <param name="file">String containing full path to file.</param>
		/// <param name="binary">True to use binary mode</param>
		public static void Save (IntMP operand, String file, bool binary) 
		{
			FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None);
			try 
			{
				IntMP.Save(operand, fs, binary);
			}
			finally 
			{
				if (fs != null) fs.Close();
			}
		}

		/// <summary>
		/// Method Save persists IntMP value in base 10 in file using text mode in specified encoding.
		/// If file already exists its value will be overwritten.
		/// </summary>
		/// <param name="operand">IntMP value to be stored.</param>
		/// <param name="file">String containing full path to file.</param>
		/// <param name="encoding">Encoding to be used.</param>
		public static void Save (IntMP operand, String file, Encoding encoding) 
		{
			FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None);
			try 
			{
				IntMP.Save(operand, fs, encoding);
			}
			finally 
			{
				if (fs != null) fs.Close();
			}
		}

		/// <summary>
		/// Method Save persists IntMP value in base 10 in file using text mode in specified encoding.
		/// If file already exists its value will be overwritten.
		/// </summary>
		/// <param name="operand">IntMP value to be stored.</param>
		/// <param name="file">String containing full path to file.</param>
		/// <param name="encoding">Encoding to be used.</param>
		/// <param name="radix">Base in which IntMP value will be presented. Allowed values
		/// are from 2 to 32 inclusive.</param>
		public static void Save (IntMP operand, String file, Encoding encoding, int radix) 
		{
			FileStream fs = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.None);
			try 
			{
				IntMP.Save(operand, fs, encoding, radix);
			}
			finally 
			{
				if (fs != null) fs.Close();
			}
		}

		/// <summary>
		/// Method Save persist IntMP operand value to FileStream in binary or ASCII text format.
		/// It is responsibility of caller to open and close FileStream.
		/// </summary>
		/// <param name="operand">IntMP value to be stored.</param>
		/// <param name="file">FileStream supplied by caller.</param>
		/// <param name="binary">True to use binary mode</param>
		public static void Save (IntMP operand, FileStream file, bool binary) 
		{
			if (!binary) IntMP.Save(operand, file, Encoding.ASCII);
			else 
			{
				byte[] export = operand.Export();
				file.Write(export, 0, export.Length);
			}
		}

		/// <summary>
		/// Method Save persist IntMP operand value in text format using indicated by caller Encoding.
		/// It is responsibility of caller to open and close FileStream.
		/// </summary>
		/// <param name="operand">IntMP value to be stored.</param>
		/// <param name="file">FileStream supplied by caller.</param>
		/// <param name="encoding">Encoding to be used.</param>
		public static void Save (IntMP operand, FileStream file, Encoding encoding) 
		{
				byte[] export = encoding.GetBytes(operand.ToString());
				file.Write(export, 0, export.Length);
		}

		/// <summary>
		/// Method Save persist IntMP value in choosen base in text format using indicated by caller Encoding.
		/// It is responsibility of caller to open and close FileStream.
		/// </summary>
		/// <param name="operand">IntMP value to be stored.</param>
		/// <param name="file">FileStream supplied by caller.</param>
		/// <param name="encoding">Encoding to be used.</param>
		/// <param name="radix">Base in which IntMP value will be presented. Allowed values
		/// are from 2 to 32 inclusive.</param>
		public static void Save (IntMP operand, FileStream file, Encoding encoding, int radix)
		{
			byte[] export = encoding.GetBytes(operand.ToString(radix));
			file.Write(export, 0, export.Length);
		}

		/// <summary>
		/// Method Open reads data stored in file and converts them into IntMP.
		/// Both binary mode and ASCII text modes are supported.
		/// </summary>
		/// <param name="result">IntMP containing decoded value.</param>
		/// <param name="file">String containing full path to file.</param>
		/// <param name="binary">True to use binary mode</param>
		public static void Open (out IntMP result, String file, bool binary) 
		{
			FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
			try 
			{
				IntMP.Open(out result, fs, binary);
			}
			finally 
			{
				if (fs != null) fs.Close();
			}
		}

		/// <summary>
		/// Method Open reads data stored in text file with given encoding and converts them into IntMP.
		/// </summary>
		/// <param name="result">IntMP containing decoded value.</param>
		/// <param name="file">String containing full path to file.</param>
		/// <param name="encoding">Encoding to be used.</param>
		public static void Open (out IntMP result, String file, Encoding encoding) 
		{
			FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
			try 
			{
				IntMP.Open(out result, fs, encoding);
			}
			finally 
			{
				if (fs != null) fs.Close();
			}
		}

		/// <summary>
		/// Method Open reads data stored in text file with given encoding 
		/// and converts them into IntMP. Additionaly base in which numbers are stored
		/// should be provided.
		/// </summary>
		/// <param name="result">IntMP containing decoded value.</param>
		/// <param name="file">String containing full path to file.</param>
		/// <param name="encoding">Encoding to be used.</param>
		/// <param name="radix">Base in which IntMP value will be presented. Allowed values
		/// are from 2 to 32 inclusive.</param>
		public static void Open (out IntMP result, String file, Encoding encoding, int radix)
		{
			FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
			try 
			{
				IntMP.Open(out result, fs, encoding, radix);
			}
			finally 
			{
				if (fs != null) fs.Close();
			}
		}

		/// <summary>
		/// Method Open reads data stored in ASCII text or binary file.
		/// It is responsibility of caller to open and close FileStream with Read access rights.
		/// </summary>
		/// <param name="result">IntMP containing decoded value.</param>
		/// <param name="file">FileStream supplied by caller.</param>
		/// <param name="binary">True to use binary mode</param>
		public static void Open (out IntMP result, FileStream file, bool binary) 
		{
			if (!binary) IntMP.Open(out result, file, Encoding.ASCII);
			else 
			{
				byte[] import = new byte[file.Length];
				int read = file.Read(import, 0, (int) file.Length);
				if (read != file.Length) throw new System.IO.IOException("Cannot read file.");
				result = new IntMP();
				result.Import(import);
			}
		}

		/// <summary>
		/// Method Open reads data stored in text file with given encoding and converts them into IntMP.
		/// It is responsibility of caller to open and close FileStream with Read access rights.
		/// </summary>
		/// <param name="result">IntMP containing decoded value.</param>
		/// <param name="file">FileStream supplied by caller.</param>
		/// <param name="encoding">Encoding to be used.</param>
		public static void Open (out IntMP result, FileStream file, Encoding encoding) 
		{
			byte[] import = new byte[file.Length];
			int read = file.Read(import, 0, (int) file.Length);
			if (read != file.Length) throw new System.IO.IOException("Cannot read file.");
			result = new IntMP(encoding.GetString(import));
		}

		/// <summary>
		/// Method Open reads data stored in text file with given encoding and converts them into IntMP.
		/// It is responsibility of caller to open and close FileStream with Read access rights.
		/// </summary>
		/// <param name="result">IntMP containing decoded value.</param>
		/// <param name="file">FileStream supplied by caller.</param>
		/// <param name="encoding">Encoding to be used.</param>
		/// <param name="radix">Base in which IntMP value will be presented. Allowed values
		/// are from 2 to 32 inclusive.</param>
		public static void Open (out IntMP result, FileStream file, Encoding encoding, int radix)
		{
			byte[] import = new byte[file.Length];
			int read = file.Read(import, 0, (int) file.Length);
			if (read != file.Length) throw new System.IO.IOException("Cannot read file.");
			result = new IntMP(encoding.GetString(import), radix);
		}


		#endregion IO

		#region Integer Import/Export
		
#if x86_64
		
		/// <summary>
		/// Method Export allows for converting IntMP into array of arbitrary 
		/// sized words: bytes, Int16, Int32, Int64 etc. If operand is equal IntPtr.Zero
		/// no output is exported. To obtain pointer to unmanaged array of words
		/// memory block of apriopriate size has to be allocated by Marshal.AllocHGlobal.
		/// Caller should free allocated memory after using it.
		/// The sign of operand is ignored by method - absolute value is exported.
		/// Method Sgn can be used to retrieve sign and handle it in user code. 
		/// Method parameters are used to format output.
		/// </summary>
		/// <param name="result">Array of words containing exported data. result has to have 
		/// enough space allocated to accomodate all output words.</param>
		/// <param name="countp">Number of words written to result.</param>
		/// <param name="order">If set to 1 most significant word goes first, 
		/// if set to -1 least significant word goes first.</param>
		/// <param name="size">Size in bytes of word in result array.</param>
		/// <param name="endian">If set to 1 most significant byte within word goes first, 
		/// if set to -1 least significant byte in word goes first, 
		/// if set to 0 native endianess of host CPU is used.</param>
		/// <param name="nails">Parameter nails can be used to account for unused high bits in word.</param>
		/// <param name="operand">IntMP value to be exported.</param>
		public static void Export (IntPtr result, out ulong countp, int order, ulong size, int endian, ulong nails, IntMP operand)
		{
			__gmpz_export( result, out countp, order, size, endian, nails, operand.number);
		}

		/// <summary>
		/// Method Import converts array of arbitrary sized words: bytes, Int16, Int32, Int64 to IntMP.
		/// </summary>
		/// <param name="result">IntMP set to the value of imported data.</param>
		/// <param name="countp">Number of words to read.</param>
		/// <param name="order">If set to 1 most significant word goes first, 
		/// if set to -1 least significant word goes first.</param>
		/// <param name="size">Size in bytes of word in operand unmanaged array.</param>
		/// <param name="endian">If set to 1 most significant byte within word goes first, 
		/// if set to -1 least significant byte in word goes first, 
		/// if set to 0 native endianess of host CPU is used.</param>
		/// <param name="nails">Parameter nails can be used to account for unused high bits in word.</param>
		/// <param name="operand">Pointer to unmanaged memory block containing array of words to be imported.
		/// Unmanaged memory can be allocated by using Marshal.AllocHGlobal and later copying managed array
		/// data to that block with Marshal.Copy method. Both are available in Mono 1.0 and .NET.</param>
		public static void Import (IntMP result, ulong countp, int order, ulong size, int endian, ulong nails, IntPtr operand) 
		{
			__gmpz_import( result.number, countp, order, size, endian, nails, operand); 
		}
		
#else		

		/// <summary>
		/// Method Export allows for converting IntMP into array of arbitrary 
		/// sized words: bytes, Int16, Int32, Int64 etc. If operand is equal IntPtr.Zero
		/// no output is exported. To obtain pointer to unmanaged array of words
		/// memory block of apriopriate size has to be allocated by Marshal.AllocHGlobal.
		/// Caller should free allocated memory after using it.
		/// The sign of operand is ignored by method - absolute value is exported.
		/// Method Sgn can be used to retrieve sign and handle it in user code. 
		/// Method parameters are used to format output.
		/// </summary>
		/// <param name="result">Array of words containing exported data. result has to have 
		/// enough space allocated to accomodate all output words.</param>
		/// <param name="countp">Number of words written to result.</param>
		/// <param name="order">If set to 1 most significant word goes first, 
		/// if set to -1 least significant word goes first.</param>
		/// <param name="size">Size in bytes of word in result array.</param>
		/// <param name="endian">If set to 1 most significant byte within word goes first, 
		/// if set to -1 least significant byte in word goes first, 
		/// if set to 0 native endianess of host CPU is used.</param>
		/// <param name="nails">Parameter nails can be used to account for unused high bits in word.</param>
		/// <param name="operand">IntMP value to be exported.</param>
		public static void Export (IntPtr result, out uint countp, int order, uint size, int endian, uint nails, IntMP operand)
		{
			__gmpz_export( result, out countp, order, size, endian, nails, operand.number);
		}

		/// <summary>
		/// Method Import converts array of arbitrary sized words: bytes, Int16, Int32, Int64 to IntMP.
		/// </summary>
		/// <param name="result">IntMP set to the value of imported data.</param>
		/// <param name="countp">Number of words to read.</param>
		/// <param name="order">If set to 1 most significant word goes first, 
		/// if set to -1 least significant word goes first.</param>
		/// <param name="size">Size in bytes of word in operand unmanaged array.</param>
		/// <param name="endian">If set to 1 most significant byte within word goes first, 
		/// if set to -1 least significant byte in word goes first, 
		/// if set to 0 native endianess of host CPU is used.</param>
		/// <param name="nails">Parameter nails can be used to account for unused high bits in word.</param>
		/// <param name="operand">Pointer to unmanaged memory block containing array of words to be imported.
		/// Unmanaged memory can be allocated by using Marshal.AllocHGlobal and later copying managed array
		/// data to that block with Marshal.Copy method. Both are available in Mono 1.0 and .NET.</param>
		public static void Import (IntMP result, uint countp, int order, uint size, int endian, uint nails, IntPtr operand) 
		{
			__gmpz_import( result.number, countp, order, size, endian, nails, operand); 
		}
		
#endif		

		#endregion Integer Import/Export

		#region Logical & Bit

		/// <summary>
		/// Method And sets result to operand1 logical and operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void And (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_and(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Method Ior sets result to operand1 inclusive-or operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Ior (IntMP result, IntMP operand1, IntMP operand2)
		{
			__gmpz_ior(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Method Xor sets result to operand1 exclusive-or operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Xor (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_xor(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Method Com sets result to one's complement of operand.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Com (IntMP result, IntMP operand)
		{
			__gmpz_com(result.number, operand.number);
		}
		
#if x86_64
		
		/// <summary>
		/// Method PopCount returns number of 1's in operand if operand>=0. If operand is negative
		/// the number of 1's is infinite and return value is Uint32.MaxValue.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns>Number of 1's in operand if operand >= 0.</returns>
		public static ulong PopCount (IntMP operand) 
		{
			return __gmpz_popcount(operand.number);
		}

		/// <summary>
		/// Method HammmingDistance returns hamming distance between two operands, which is 
		/// the number of bit positions where operand1 and operand2 have different value, 
		/// providing that both operand1 and operand2 are of this same sign. If operand1 and operand2 have 
		/// different sign the number of differences is infinite and return value is UInt32.MaxValue.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static ulong HammingDistance (IntMP operand1, IntMP operand2) 
		{
			return __gmpz_hamdist(operand1.number, operand2.number);
		}

		/// <summary>
		/// Method Scan0 scans operand starting from position starting_bit until
		/// the first bit 0 is found returning the index of found bit.
		/// </summary>
		/// <param name="operand"></param>
		/// <param name="starting_bit"></param>
		/// <returns></returns>
		public static ulong Scan0 (IntMP operand, ulong starting_bit) 
		{
			return __gmpz_scan0(operand.number, starting_bit);
		}

		/// <summary>
		/// Method Scan0 scans operand starting from position starting_bit until
		/// the first bit 0 is found returning the index of found bit.
		/// </summary>
		/// <param name="operand"></param>
		/// <param name="starting_bit"></param>
		/// <returns></returns>
		public static ulong Scan1 (IntMP operand, ulong starting_bit)
		{
			return __gmpz_scan1(operand.number, starting_bit);
		}

		public static void SetBit (IntMP result, ulong bit_index) 
		{
			__gmpz_setbit(result.number, bit_index);
		}

		public static void ClearBit (IntMP result, ulong bit_index) 
		{
			__gmpz_clrbit(result.number, bit_index);
		}

		public static int TestBit (IntMP operand, ulong bit_index) 
		{
			return __gmpz_tstbit(operand.number, bit_index);
		}
		
		public static void ComBit (IntMP operand, ulong bit_index)
		{
			__gmpz_combit(operand.number, bit_index);
		}
				
		
#else		

		/// <summary>
		/// Method PopCount returns number of 1's in operand if operand>=0. If operand is negative
		/// the number of 1's is infinite and return value is Uint32.MaxValue.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static uint PopCount (IntMP operand) 
		{
			return __gmpz_popcount(operand.number);
		}

		/// <summary>
		/// Method HammmingDistance returns hamming distance between two operands, which is 
		/// the number of bit positions where operand1 and operand2 have different value, 
		/// providing that both operand1 and operand2 are of this same sign. If operand1 and operand2 have 
		/// different sign the number of differences is infinite and return value is UInt32.MaxValue.
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static uint HammingDistance (IntMP operand1, IntMP operand2) 
		{
			return __gmpz_hamdist(operand1.number, operand2.number);
		}

		/// <summary>
		/// Method Scan0 scans operand starting from position starting_bit until
		/// the first bit 0 is found returning the index of found bit.
		/// </summary>
		/// <param name="operand"></param>
		/// <param name="starting_bit"></param>
		/// <returns></returns>
		public static uint Scan0 (IntMP operand, uint starting_bit) 
		{
			return __gmpz_scan0(operand.number, starting_bit);
		}

		/// <summary>
		/// Method Scan0 scans operand starting from position starting_bit until
		/// the first bit 0 is found returning the index of found bit.
		/// </summary>
		/// <param name="operand"></param>
		/// <param name="starting_bit"></param>
		/// <returns></returns>
		public static uint Scan1 (IntMP operand, uint starting_bit)
		{
			return __gmpz_scan1(operand.number, starting_bit);
		}

		public static void SetBit (IntMP result, uint bit_index) 
		{
			__gmpz_setbit(result.number, bit_index);
		}

		public static void ClearBit (IntMP result, uint bit_index) 
		{
			__gmpz_clrbit(result.number, bit_index);
		}

		public static int TestBit (IntMP operand, uint bit_index) 
		{
			return __gmpz_tstbit(operand.number, bit_index);
		}
		
		public static void ComBit (IntMP operand, uint bit_index)
		{
			__gmpz_combit(operand.number, bit_index);
		}
		
#endif		

		#endregion Logical & Bit

		#region Miscallenous

#if x86_64

		public static bool FitsUInt64(IntMP operand)
		{
			return __gmpz_fits_ulong_p(operand.number) != 0;
		}

		public static bool FitsInt64(IntMP operand)
		{
			return __gmpz_fits_slong_p(operand.number) != 0;
		}

#endif

		public static bool FitsUInt32 (IntMP operand) 
		{
			return __gmpz_fits_uint_p(operand.number) != 0;
		}

		public static bool FitsInt32 (IntMP operand) 
		{
			return __gmpz_fits_sint_p(operand.number) != 0;
		}
		
		public static bool FitsUInt16 (IntMP operand) 
		{
			return __gmpz_fits_ushort_p(operand.number) != 0;
		}

		public static bool FitsInt16 (IntMP operand) 
		{
			return __gmpz_fits_sshort_p(operand.number) != 0;
		}

		public static bool Odd (IntMP operand) 
		{
			return __gmpz_odd_p(operand.number) != 0;
		}

		public static bool Even (IntMP operand) 
		{
			return __gmpz_even_p(operand.number) != 0;
		}

#if x86_64

		public static ulong SizeInBase(IntMP operand, int radix)
		{
			return __gmpz_sizeinbase(operand.number, radix);
		}

#else

		public static uint SizeInBase(IntMP operand, int radix) 
		{
			return __gmpz_sizeinbase(operand.number, radix);
		}

#endif

		#endregion Miscallenous

		#region Number Theoretic

#if x86_64


		public static ulong GreatestCommonDivisor(IntMP result, IntMP operand1, ulong operand2)
		{
			return __gmpz_gcd_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Method LCD computes least common multiple of operand1 and operand2.
		/// </summary>
		/// <param name="result">Computed lest common multiple of operand1 and operand2.</param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void LeastCommonMultiple(IntMP result, IntMP operand1, ulong operand2)
		{
			__gmpz_lcm_ui(result.number, operand1.number, operand2);
		}

		public static int Kronecker(IntMP a, long b)
		{
			return __gmpz_kronecker_si(a.number, b);
		}

		public static int Kronecker(IntMP a, ulong b)
		{
			return __gmpz_kronecker_ui(a.number, b);
		}

		public static int Kronecker(long a, IntMP b)
		{
			return __gmpz_si_kronecker(a, b.number);
		}

		public static int Kronecker(ulong a, IntMP b)
		{
			return __gmpz_ui_kronecker(a, b.number);
		}

		/// <summary>
		/// Method RemoveFactor removes all occurences of factor from operand and stores 
		/// result in result. The return value indicates how many factor occurences
		/// were removed.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="factor"></param>
		/// <returns></returns>
		public static ulong RemoveFactor(IntMP result, IntMP operand, IntMP factor)
		{
			return __gmpz_remove(result.number, operand.number, factor.number);
		}

		/// <summary>
		/// Method Factorial calculates factorial of operand (operand!) and stores result in result.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		public static void Factorial(IntMP result, ulong operand)
		{
			__gmpz_fac_ui(result.number, operand);
		}

		/// <summary>
		/// Method Binominal computes binominal coefficient (nk) and sets result to result of operation.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="n"></param>
		/// <param name="k"></param>
		public static void BinominalCoefficient(IntMP result, IntMP n, ulong k)
		{
			__gmpz_bin_ui(result.number, n.number, k);
		}

		/// <summary>
		/// Method Bin computes binominal coefficient (nk) and sets result to result of operation.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="n"></param>
		/// <param name="k"></param>
		public static void BinominalCoefficient(IntMP result, ulong n, ulong k)
		{
			__gmpz_bin_uiui(result.number, n, k);
		}

		/// <summary>
		/// Method Fibonacci sets fn to value of n th Fibonacci number Fn.
		/// </summary>
		/// <param name="fn"></param>
		/// <param name="n"></param>
		public static void Fibonacci(IntMP fn, ulong n)
		{
			__gmpz_fib_ui(fn.number, n);
		}

		/// <summary>
		/// Method Fibonacci sets fn to n'th Fibonacci number (Fn) and fnsub1 to (n-1)'th
		/// Fibonacci number (Fn-1).
		/// </summary>
		/// <param name="fn"></param>
		/// <param name="n"></param>
		public static void Fibonacci(IntMP fn, IntMP fnsub1, ulong n)
		{
			__gmpz_fib2_ui(fn.number, fnsub1.number, n);
		}

		/// <summary>
		/// Method Lucas sets ln to n'th Lucas number.
		/// </summary>
		/// <param name="ln"></param>
		/// <param name="n"></param>
		public static void Lucas(IntMP ln, ulong n)
		{
			__gmpz_lucnum_ui(ln.number, n);
		}

		/// <summary>
		/// Method Lucas sets ln to n'th Lucas number and lnsub1 to (n-1)'th Lucas number.
		/// </summary>
		/// <param name="ln"></param>
		/// <param name="lnsub1"></param>
		/// <param name="n"></param>
		public static void Lucas(IntMP ln, IntMP lnsub1, ulong n)
		{
			__gmpz_lucnum2_ui(ln.number, lnsub1.number, n);
		}

#else

		public static uint GreatestCommonDivisor (IntMP result, IntMP operand1, uint operand2) 
		{
			return __gmpz_gcd_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Method LCD computes least common multiple of operand1 and operand2.
		/// </summary>
		/// <param name="result">Computed lest common multiple of operand1 and operand2.</param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void LeastCommonMultiple (IntMP result, IntMP operand1, uint operand2) 
		{
			__gmpz_lcm_ui(result.number, operand1.number, operand2);
		}

		public static int Kronecker (IntMP a, int b) 
		{
			return __gmpz_kronecker_si(a.number, b);
		}

		public static int Kronecker (IntMP a, uint b) 
		{
			return __gmpz_kronecker_ui(a.number, b);
		}

		public static int Kronecker (int a, IntMP b) 
		{
			return __gmpz_si_kronecker(a, b.number);
		}

		public static int Kronecker (uint a, IntMP b) 
		{
			return __gmpz_ui_kronecker(a, b.number);
		}

		/// <summary>
		/// Method RemoveFactor removes all occurences of factor from operand and stores 
		/// result in result. The return value indicates how many factor occurences
		/// were removed.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		/// <param name="factor"></param>
		/// <returns></returns>
		public static uint RemoveFactor (IntMP result, IntMP operand, IntMP factor) 
		{
			return __gmpz_remove(result.number, operand.number, factor.number);
		}

		/// <summary>
		/// Method Factorial calculates factorial of operand (operand!) and stores result in result.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		public static void Factorial (IntMP result, uint operand) 
		{
			__gmpz_fac_ui (result.number, operand);
		}

		/// <summary>
		/// Method Binominal computes binominal coefficient (nk) and sets result to result of operation.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="n"></param>
		/// <param name="k"></param>
		public static void BinominalCoefficient (IntMP result, IntMP n, uint k) 
		{
			__gmpz_bin_ui (result.number, n.number, k);
		}

		/// <summary>
		/// Method Bin computes binominal coefficient (nk) and sets result to result of operation.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="n"></param>
		/// <param name="k"></param>
		public static void BinominalCoefficient (IntMP result, uint n, uint k) 
		{
			__gmpz_bin_uiui (result.number, n, k);
		}

		/// <summary>
		/// Method Fibonacci sets fn to value of n th Fibonacci number Fn.
		/// </summary>
		/// <param name="fn"></param>
		/// <param name="n"></param>
		public static void Fibonacci (IntMP fn, uint n) 
		{
			__gmpz_fib_ui (fn.number, n);
		}

		/// <summary>
		/// Method Fibonacci sets fn to n'th Fibonacci number (Fn) and fnsub1 to (n-1)'th
		/// Fibonacci number (Fn-1).
		/// </summary>
		/// <param name="fn"></param>
		/// <param name="n"></param>
		public static void Fibonacci (IntMP fn, IntMP fnsub1, uint n) 
		{
			__gmpz_fib2_ui (fn.number, fnsub1.number, n);
		}

		/// <summary>
		/// Method Lucas sets ln to n'th Lucas number.
		/// </summary>
		/// <param name="ln"></param>
		/// <param name="n"></param>
		public static void Lucas (IntMP ln, uint n) 
		{
			__gmpz_lucnum_ui (ln.number, n);
		}

		/// <summary>
		/// Method Lucas sets ln to n'th Lucas number and lnsub1 to (n-1)'th Lucas number.
		/// </summary>
		/// <param name="ln"></param>
		/// <param name="lnsub1"></param>
		/// <param name="n"></param>
		public static void Lucas (IntMP ln, IntMP lnsub1, uint n) 
		{
			__gmpz_lucnum2_ui (ln.number, lnsub1.number, n);
		}


#endif

		/// <summary>
		/// Method ProbPrime tests n for being prime. Method first does some trial division than
		/// performs Miller-Rabin primality tests. Number of tests is determined by reps parameter.
		/// Reasonable values of reps are 5 - 10.
		/// </summary>
		/// <param name="n">Integer to be tested.</param>
		/// <param name="reps">Number of Miller-Rabin tests to perform.</param>
		/// <returns>Return int has value 2 if n is prime, 1 if n is probably prime, 
		/// 0 if n is defenitely composite.</returns>
		public static int ProbablyPrime (IntMP n, int reps) 
		{
			return __gmpz_probab_prime_p (n.number, reps);
		}

		/// <summary>
		/// Method NextPrime sets result to next prime being greater than operand.
		/// Method uses probabilistic tests to identify primes so result is a probably prime. 
		/// For practical purposes the method is adequate, however, when exact values are needed 
		/// NextPrime method should not be used or should be used as a first step 
		/// before primality proving algorithms are applied.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		public static void NextPrime (IntMP result, IntMP operand) 
		{
			__gmpz_nextprime (result.number, operand.number);
		}

		public static void GreatestCommonDivisor (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_gcd(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Method GreatestCommonDivisor computes greates common divisor of a and b. In addition method sets s and t 
		/// to satisfy equation result = a * s + b * t;
		/// </summary>
		/// <param name="result"></param>
		/// <param name="s"></param>
		/// <param name="t"></param>
		/// <param name="a"></param>
		/// <param name="b"></param>
		public static void GreatestCommonDivisorExtended (IntMP result, IntMP s, IntMP t, IntMP a, IntMP b) 
		{
			__gmpz_gcdext (result.number, s.number, t.number, a.number, b.number);
		}

		/// <summary>
		/// Method LeastCommonMultiple computes least common multiple of operand1 and operand2.
		/// </summary>
		/// <param name="result">Computed lest common multiple of operand1 and operand2.</param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void LeastCommonMultiple (IntMP result, IntMP operand1, IntMP operand2) 
		{
			__gmpz_lcm(result.number, operand1.number, operand2.number);
		}

		/// <summary>
		/// Method Invert computes the inverse of operand1 module operand2 and puts result in result.
		/// If the inverse exists the return value is true and result will satisfy 0 smaller or equal result smaller operand2.
		/// If the inverse does not exist the return value is false and result is undefined.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <returns></returns>
		public static bool Invert (IntMP result, IntMP operand1, IntMP operand2)
		{
			return __gmpz_invert (result.number, operand1.number, operand2.number) != 0;
		}

		public static int Jacobi (IntMP a, IntMP b) 
		{
			return __gmpz_jacobi(a.number, b.number);
		}

		public static int Legendre (IntMP a, IntMP b) 
		{
			return __gmpz_legendre (a.number, b.number);
		}

		public static int Kronecker (IntMP a, IntMP b)
		{
			return __gmpz_kronecker(a.number, b.number);
		}

		#endregion Number Theoretic

		#endregion Static Methods

		#region Instance Methods

		#region Assignment

#if x86_64

		public void Set(ulong operand)
		{
			__gmpz_set_ui(this.number, operand);
		}

		public void Set(long operand)
		{
			__gmpz_set_si(this.number, operand);
		}

#else

		public void Set (uint operand) 
		{
			__gmpz_set_ui(this.number, operand);
		}

		public void Set (int operand) 
		{
			__gmpz_set_si(this.number, operand);
		}

#endif

		public void Set (double operand) 
		{
			__gmpz_set_d(this.number, operand);
		}

		public void Set (IntMP operand)
		{
			__gmpz_set(this.number, operand.number);
		}

		public void Set (FloatMP value) 
		{
			__gmpz_set_f(this.number, value.number);
		}

		public void Set (RatnlMP value) 
		{
			__gmpz_set_q(this.number, value.number);
		}

		#endregion Assignment

		#region Import/Export

		/// <summary>
		/// Simplified Export method converting IntMP to byte[] Array 
		/// with the following settings:
		/// first byte holds sign of IntMP - 1 is minus, 0 is 0 or positive,
		/// order - most significant bytes first,
		/// size of word - byte,
		/// endian - most significant bit first,
		/// nails - no nails.
		/// </summary>
		/// <returns>Array bytes (byte[]) representing sign and bytes of this IntMP instance.</returns>
		public byte[] Export() 
		{
			byte[] result = null;
			System.IntPtr array = IntPtr.Zero; 

			try 
			{
#if x86_64
				ulong countp;
#else
				uint countp;
#endif
				__gmpz_export(array, out countp, 1, 1, 1, 0, this.number);
				result = new byte[countp + 1];
				array = Marshal.AllocHGlobal((int)(countp));
				__gmpz_export(array, out countp, 1, 1, 1, 0, this.number);
				Marshal.Copy(array, result, 1, (int) countp);
				if (IntMP.Sign(this)< 0) result[0] = 1;
			}
			finally 
			{
				if (array != IntPtr.Zero) Marshal.FreeHGlobal(array);
			}

			return result;
		}

		/// <summary>
		/// Method Import converts Array of bytes (byte[]) to value of this IntMP instance.
		/// Format of bytes should be following:
		/// first byte holds sign of integer - 1 is minus,
		/// order - most significant bytes first,
		/// size of word - byte,
		/// endian - most significant bit first,
		/// nails - no nails.
		/// </summary>
		/// <param name="data">byet[] Array holding data to be imported.</param>
		public void Import(byte[] data) 
		{
			IntPtr array = IntPtr.Zero;
			
			try 
			{
				array = Marshal.AllocHGlobal(data.Length - 1);
				Marshal.Copy(data, 1, array, data.Length - 1);
				__gmpz_import(this.number, (uint) (data.Length - 1), 1, 1, 1, 0, array);
				if (data[0] == 1) __gmpz_neg(this.number, this.number);
			}
			finally 
			{
				if (array != IntPtr.Zero) Marshal.FreeHGlobal(array);
			}
		}


		#endregion Import/Export

		#endregion Instance Methods

		#region Operators Overloads

		public static IntMP operator +(IntMP operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_add (result.number, operand1.number, operand2.number);
			return result;
		}

#if x86_64

		public static IntMP operator +(IntMP operand1, ulong operand2)
		{
			IntMP result = new IntMP();
			__gmpz_add_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator +(ulong operand1, IntMP operand2)
		{
			IntMP result = new IntMP();
			__gmpz_add_ui(result.number, operand2.number, operand1);
			return result;
		}

		public static IntMP operator -(IntMP operand1, ulong operand2)
		{
			IntMP result = new IntMP();
			__gmpz_sub_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator -(ulong operand1, IntMP operand2)
		{
			IntMP result = new IntMP();
			__gmpz_ui_sub(result.number, operand1, operand2.number);
			return result;
		}

		public static IntMP operator *(IntMP operand1, long operand2)
		{
			IntMP result = new IntMP();
			__gmpz_mul_si(result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator *(long operand1, IntMP operand2)
		{
			IntMP result = new IntMP();
			__gmpz_mul_si(result.number, operand2.number, operand1);
			return result;
		}

		public static IntMP operator *(IntMP operand1, ulong operand2)
		{
			IntMP result = new IntMP();
			__gmpz_mul_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator *(ulong operand1, IntMP operand2)
		{
			IntMP result = new IntMP();
			__gmpz_mul_ui(result.number, operand2.number, operand1);
			return result;
		}

		public static IntMP operator %(IntMP n, ulong divisor)
		{
			if (divisor == 0) throw new DivideByZeroException("Attempt to divide by zero in modulus expression.");
			IntMP result = new IntMP();
			__gmpz_mod_ui(result.number, n.number, divisor);
			return result;
		}

		public static IntMP operator /(IntMP n, ulong divisor)
		{
			if (divisor == 0) throw new DivideByZeroException("Attempt to divide by zero.");
			IntMP result = new IntMP();
			IntMP.__gmpz_tdiv_q_ui(result.number, n.number, divisor);
			return result;
		}

		public static bool operator ==(IntMP operand1, long operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) == 0;
		}

		public static bool operator ==(long operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) == 0;
		}

		public static bool operator ==(IntMP operand1, ulong operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) == 0;
		}

		public static bool operator ==(ulong operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) == 0;
		}

		public static bool operator !=(IntMP operand1, long operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) != 0;
		}

		public static bool operator !=(long operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) != 0;
		}

		public static bool operator !=(IntMP operand1, ulong operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) != 0;
		}

		public static bool operator !=(ulong operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) != 0;
		}

		public static bool operator >(IntMP operand1, long operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) > 0;
		}

		public static bool operator >(long operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) < 0;
		}

		public static bool operator >(IntMP operand1, ulong operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) > 0;
		}

		public static bool operator >(ulong operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) < 0;
		}

		public static bool operator <(IntMP operand1, long operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) < 0;
		}

		public static bool operator <(long operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) > 0;
		}

		public static bool operator <(IntMP operand1, ulong operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) < 0;
		}

		public static bool operator <(ulong operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) > 0;
		}

		public static bool operator >=(IntMP operand1, long operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) >= 0;
		}

		public static bool operator >=(long operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) <= 0;
		}

		public static bool operator >=(IntMP operand1, ulong operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) >= 0;
		}

		public static bool operator >=(ulong operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) <= 0;
		}

		public static bool operator <=(IntMP operand1, long operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) <= 0;
		}

		public static bool operator <=(long operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) >= 0;
		}

		public static bool operator <=(IntMP operand1, ulong operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) <= 0;
		}

		public static bool operator <=(ulong operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) >= 0;
		}
#else

		public static IntMP operator +(IntMP operand1, uint operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_add_ui (result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator +(uint operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_add_ui (result.number, operand2.number, operand1);
			return result;
		}

		public static IntMP operator -(IntMP operand1, uint operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_sub_ui (result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator -(uint operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_ui_sub (result.number, operand1, operand2.number);
			return result;
		}

		public static IntMP operator *(IntMP operand1, int operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_mul_si(result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator *(int operand1, IntMP operand2)
		{
			IntMP result = new IntMP();
			__gmpz_mul_si(result.number, operand2.number, operand1);
			return result;
		}

		public static IntMP operator *(IntMP operand1, uint operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_mul_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static IntMP operator *(uint operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_mul_ui(result.number, operand2.number, operand1);
			return result;
		}

		public static IntMP operator %(IntMP n, uint divisor) 
		{
			if (divisor == 0) throw new DivideByZeroException("Attempt to divide by zero in modulus expression.");
			IntMP result = new IntMP();
			__gmpz_mod_ui(result.number, n.number, divisor);
			return result;
		}

		public static IntMP operator /(IntMP n, uint divisor) 
		{
			if (divisor == 0) throw new DivideByZeroException("Attempt to divide by zero.");
			IntMP result = new IntMP();
			IntMP.__gmpz_tdiv_q_ui(result.number, n.number, divisor);
			return result;
		}

		public static bool operator ==(IntMP operand1, int operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) == 0;
		}

		public static bool operator ==(int operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) == 0;
		}

		public static bool operator ==(IntMP operand1, uint operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) == 0;
		}

		public static bool operator ==(uint operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) == 0;
		}

		public static bool operator ==(IntMP operand1, ulong operand2) 
		{
			return operand1.Equals(new IntMP(operand2));
		}

		public static bool operator ==(ulong operand1, IntMP operand2) 
		{
			return operand2.Equals(new IntMP(operand1));
		}
		public static bool operator ==(IntMP operand1, long operand2) 
		{
			return operand1.Equals(new IntMP(operand2));
		}

		public static bool operator ==(long operand1, IntMP operand2)
		{
			return operand2.Equals(new IntMP(operand1));
		}

		public static bool operator !=(IntMP operand1, int operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) != 0;
		}

		public static bool operator !=(int operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) != 0;
		}

		public static bool operator !=(IntMP operand1, uint operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) != 0;
		}

		public static bool operator !=(uint operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) != 0;
		}

		public static bool operator !=(IntMP operand1, ulong operand2) 
		{
			return !operand1.Equals(new IntMP(operand2));
		}

		public static bool operator !=(ulong operand1, IntMP operand2)
		{
			return !operand2.Equals(new IntMP(operand1));
		}

		public static bool operator !=(IntMP operand1, long operand2)
		{
			return !operand1.Equals(new IntMP(operand2));
		}

		public static bool operator !=(long operand1, IntMP operand2)
		{
			return !operand2.Equals(new IntMP(operand1));
		}

		public static bool operator >(IntMP operand1, int operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) > 0;
		}

		public static bool operator >(int operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) < 0;
		}

		public static bool operator >(IntMP operand1, uint operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) > 0;
		}

		public static bool operator >(uint operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) < 0;
		}

		public static bool operator <(IntMP operand1, int operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) < 0;
		}

		public static bool operator <(int operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) > 0;
		}

		public static bool operator <(IntMP operand1, uint operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) < 0;
		}

		public static bool operator <(uint operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) > 0;
		}

		public static bool operator >=(IntMP operand1, int operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) >= 0;
		}

		public static bool operator >=(int operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) <= 0;
		}

		public static bool operator >=(IntMP operand1, uint operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) >= 0;
		}

		public static bool operator >=(uint operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) <= 0;
		}

		public static bool operator <=(IntMP operand1, int operand2)
		{
			return __gmpz_cmp_si(operand1.number, operand2) <= 0;
		}

		public static bool operator <=(int operand1, IntMP operand2)
		{
			return __gmpz_cmp_si(operand2.number, operand1) >= 0;
		}

		public static bool operator <=(IntMP operand1, uint operand2)
		{
			return __gmpz_cmp_ui(operand1.number, operand2) <= 0;
		}

		public static bool operator <=(uint operand1, IntMP operand2)
		{
			return __gmpz_cmp_ui(operand2.number, operand1) >= 0;
		}

#endif

		public static IntMP operator ++(IntMP operand) 
		{
			__gmpz_add_ui(operand.number, operand.number, 1);
			return operand;
		}

		public static IntMP operator -(IntMP operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_sub (result.number, operand1.number, operand2.number);
			return result;
		}

		public static IntMP operator -(IntMP integer) 
		{
			__gmpz_neg(integer.number, integer.number);
			return integer;
		}

		public static IntMP operator --(IntMP operand) 
		{
			__gmpz_sub_ui(operand.number, operand.number, 1);
			return operand;
		}

		public static IntMP operator *(IntMP operand1, IntMP operand2)
		{
			IntMP result = new IntMP();
			__gmpz_mul(result.number, operand1.number, operand2.number);
			return result;
		}

		public static IntMP operator %(IntMP n, IntMP divisor) 
		{
			if (divisor == 0) throw new DivideByZeroException("Attempt to divide by zero in modulus expression.");
			IntMP result = new IntMP();
			__gmpz_mod(result.number, n.number, divisor.number);
			return result;
		}

		public static IntMP operator /(IntMP n, IntMP divisor) 
		{
			if (divisor == 0) throw new DivideByZeroException("Attempt to divide by zero.");
			IntMP result = new IntMP();
			IntMP.__gmpz_tdiv_q(result.number, n.number, divisor.number);
			return result;
		}

		public static IntMP operator <<(IntMP operand1, int operand2) 
		{
			// TODO: - use mpn_lshift function for operand2 < 32
			IntMP result = new IntMP();
			__gmpz_mul_2exp(result.number, operand1.number, (uint) System.Math.Abs(operand2));
			return result;
		}

		public static IntMP operator >>(IntMP operand1, int operand2)
		{
			// TODO: - use mpn_rshift function for operand2 < 32
			IntMP result = new IntMP();
			__gmpz_fdiv_q_2exp(result.number, operand1.number, (uint) System.Math.Abs(operand2));
			return result;
		}

		public static bool operator ==(IntMP operand1, IntMP operand2) 
		{
			// It is really slow (cast to object) but consistent with 
			// base classes implementation
			if ((operand1 as object) == (operand2 as object)) 
				return true;
					
			if (null == operand1 || null == operand2)
				return false;
					
			return __gmpz_cmp(operand1.number, operand2.number) == 0;
		}

		public static bool operator ==(IntMP operand1, double operand2)
		{
			return __gmpz_cmp_d(operand1.number, operand2) == 0;
		}

		public static bool operator ==(double operand1, IntMP operand2)
		{
			return __gmpz_cmp_d(operand2.number, operand1) == 0;
		}

		public static bool operator !=(IntMP operand1, IntMP operand2)
		{
			// It is really slow (cast to object) but consistent with 
			// base classes implementation
			if ((operand1 as object) == (operand2 as object)) 
				return false;		

			if (null == operand1 || null == operand2)
				return true;
					
			return __gmpz_cmp(operand1.number, operand2.number) != 0;
		}

		public static bool operator !=(IntMP operand1, double operand2)
		{
			return __gmpz_cmp_d(operand1.number, operand2) != 0;
		}

		public static bool operator !=(double operand1, IntMP operand2)
		{
			return __gmpz_cmp_d(operand2.number, operand1) != 0;
		}

		public static bool operator >(IntMP operand1, IntMP operand2)
		{
			return __gmpz_cmp(operand1.number, operand2.number) > 0;
		}

		public static bool operator >(IntMP operand1, double operand2)
		{
			return __gmpz_cmp_d(operand1.number, operand2) > 0;
		}

		public static bool operator >(double operand1, IntMP operand2)
		{
			return __gmpz_cmp_d(operand2.number, operand1) < 0;
		}

		public static bool operator <(IntMP operand1, IntMP operand2)
		{
			return __gmpz_cmp(operand1.number, operand2.number) < 0;
		}

		public static bool operator <(IntMP operand1, double operand2)
		{
			return __gmpz_cmp_d(operand1.number, operand2) < 0;
		}

		public static bool operator <(double operand1, IntMP operand2)
		{
			return __gmpz_cmp_d(operand2.number, operand1) > 0;
		}

		public static bool operator >=(IntMP operand1, IntMP operand2)
		{
			return __gmpz_cmp(operand1.number, operand2.number) >= 0;
		}

		public static bool operator >=(IntMP operand1, double operand2)
		{
			return __gmpz_cmp_d(operand1.number, operand2) >= 0;
		}

		public static bool operator >=(double operand1, IntMP operand2)
		{
			return __gmpz_cmp_d(operand2.number, operand1) <= 0;
		}

		public static bool operator <=(IntMP operand1, IntMP operand2)
		{
			return __gmpz_cmp(operand1.number, operand2.number) <= 0;
		}

		public static bool operator <=(IntMP operand1, double operand2)
		{
			return __gmpz_cmp_d(operand1.number, operand2) <= 0;
		}

		public static bool operator <=(double operand1, IntMP operand2)
		{
			return __gmpz_cmp_d(operand2.number, operand1) >= 0;
		}

		public static IntMP operator &(IntMP operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_and(result.number, operand1.number, operand2.number);
			return result;
		}

		public static IntMP operator |(IntMP operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_ior(result.number, operand1.number, operand2.number);
			return result;
		}

		public static IntMP operator ^(IntMP operand1, IntMP operand2) 
		{
			IntMP result = new IntMP();
			__gmpz_xor(result.number, operand1.number, operand2.number);
			return result;
		}

		public static IntMP operator ~(IntMP operand) 
		{
			IntMP result = new IntMP();
			__gmpz_com(result.number, operand.number);
			return result;
		}


		public static explicit operator IntMP(byte operand) 
		{
			return new IntMP((uint) operand);
		}

		public static explicit operator IntMP(sbyte operand) 
		{
			return new IntMP((int) operand);
		}

		public static explicit operator IntMP(char operand) 
		{
			return new IntMP((uint) operand);
		}

		public static explicit operator IntMP(short operand) 
		{
			return new IntMP((int) operand);
		}

		public static explicit operator IntMP(ushort operand) 
		{
			return new IntMP((uint) operand);
		}

		public static explicit operator IntMP(uint operand) 
		{
			return new IntMP(operand);
		}

		public static explicit operator IntMP(int operand) 
		{
			return new IntMP(operand);
		}

		public static explicit operator IntMP(float operand)
		{
			return new IntMP((double) operand);
		}

		public static explicit operator IntMP(double operand) 
		{
			return new IntMP(operand);
		}

		public static explicit operator IntMP(ulong operand) 
		{
			return new IntMP(operand);
		}

		public static explicit operator IntMP(long operand)
		{
			return new IntMP(operand);
		}

		public static explicit operator IntMP(decimal operand) 
		{

			// TODO : provide more efficient implementation based on byte[] import

			// Naive first shot implementation - heavy stuff ;)
			int[] bits = Decimal.GetBits(operand);
			IntMP result = new IntMP( (uint) bits[2]);
			IntMP.__gmpz_mul_2exp(result.number, result.number, 64);
			IntMP interm = new IntMP( ((ulong) bits[1] << 32) + (ulong) bits[0]);
			IntMP.__gmpz_add(result.number, result.number, interm.number);
			IntMP interm2 = new IntMP();
			IntMP.__gmpz_ui_pow_ui(interm2.number, 10, (uint) (bits[3] & 0xff0000));
			IntMP.__gmpz_tdiv_q(result.number, result.number, interm2.number);
			if ( (bits[3] & 0xf0000000) > 0) IntMP.__gmpz_neg(result.number, result.number); // TODO: Fix sign test / assignment
			return result;

		}

		public static explicit operator IntMP(RatnlMP operand)
		{
			return new IntMP(operand);
		}

		public static explicit operator IntMP(FloatMP operand)
		{
			return new IntMP(operand);
		}

		public static explicit operator byte(IntMP operand)
		{
			return ConvertMP.ToByte(operand);
		}

		public static explicit operator sbyte(IntMP operand)
		{
			return ConvertMP.ToSByte(operand);
		}

		public static explicit operator char(IntMP operand)
		{
			return ConvertMP.ToChar(operand);
		}

		public static explicit operator short(IntMP operand)
		{
			return ConvertMP.ToInt16(operand);
		}

		public static explicit operator ushort(IntMP operand)
		{
			return ConvertMP.ToUInt16(operand);
		}

		public static explicit operator uint(IntMP operand)
		{
			return ConvertMP.ToUInt32(operand);
		}

		public static explicit operator int(IntMP operand)
		{
			return ConvertMP.ToInt32(operand);
		}

		public static explicit operator float(IntMP operand)
		{
			return ConvertMP.ToSingle(operand);
		}

		public static explicit operator double(IntMP operand)
		{
			return ConvertMP.ToDouble(operand);
		}

		public static explicit operator long(IntMP operand)
		{
			return ConvertMP.ToInt64(operand);
		}

		public static explicit operator ulong(IntMP operand)
		{
			return ConvertMP.ToUInt64(operand);
		}

		public static explicit operator decimal(IntMP operand)
		{
			return ConvertMP.ToDecimal(operand);
		}


		#endregion Operators Overloads

	}

	#endregion Class IntMP

	#region Class mpz_t

	[StructLayout(LayoutKind.Sequential)]
	public class mpz_t 
	{
		// Explicit layout makes mpz_t processor/compiler word size dependant
		// in original mpz_t struct all fields will vary with word size
		// However there are no problems of this kind with GCC / MSVC 
		/*[FieldOffset(0)]*/ public int mp_alloc;
		/*[FieldOffset(4)]*/ public int mp_size;
		/*[FieldOffset(8)]*/ public System.IntPtr mp_d;
	}

	#endregion Class mpz_t

}


