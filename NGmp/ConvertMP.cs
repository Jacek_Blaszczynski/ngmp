/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Globalization;

namespace NGmp.Math
{

	#region Class ConvertMP

	/// <summary>
	/// ConvertMP provides a set of conversions extending Convert class 
	/// to MP types.
	/// </summary>
	public sealed class ConvertMP
	{

		private ConvertMP()
		{
		}


		#region ToBoolean

		public static bool ToBoolean (IntMP value) 
		{ 
			return (value != 0); 
		}

		public static bool ToBoolean (FloatMpfr value) 
		{ 
			return (value != 0); 
		}

		public static bool ToBoolean (RatnlMP value) 
		{ 
			return (value != 0); 
		}

		public static bool ToBoolean (FloatMP value) 
		{ 
			return (value != 0); 
		}


		#endregion ToBoolean

		#region ToByte

		public static byte ToByte (IntMP value) 
		{

			if (IntMP.__gmpz_cmp_ui(value.number, 0) >= 0 
				&& IntMP.__gmpz_cmp_ui(value.number, 255) <= 0) 

				return (byte) IntMP.__gmpz_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than Byte.MaxValue or less than Byte.MinValue");

		}

		public static byte ToByte (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_cmp_ui_2exp(value.number, 0, 0) >= 0 
				&& FloatMpfr.mpfr_cmp_ui_2exp(value.number, 255, 0) <= 0) 

				return (byte) FloatMpfr.mpfr_get_ui(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than Byte.MaxValue or less than Byte.MinValue");
		}

		public static byte ToByte (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_ui(value.number, 0, 1) >= 0 
				&& RatnlMP.__gmpq_cmp_ui(value.number, 255, 1) <= 0) 

				return (byte) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Byte.MaxValue or less than Byte.MinValue");
		}

		public static byte ToByte (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_ui(value.number, 0) >= 0 
				&& FloatMP.__gmpf_cmp_ui(value.number, 255) <= 0) 

				return (byte) FloatMP.__gmpf_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than Byte.MaxValue or less than Byte.MinValue");
		}


		#endregion ToByte

		#region ToSByte

		public static sbyte ToSByte (IntMP value) 
		{

			if (IntMP.__gmpz_cmp_si(value.number, -128) >= 0 
				&& IntMP.__gmpz_cmp_si(value.number, 127) <= 0) 

				return (sbyte) IntMP.__gmpz_get_si(value.number); 

			else throw new OverflowException(
					 "Value is greater than SByte.MaxValue or less than SByte.MinValue");

		}

		public static sbyte ToSByte (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_cmp_si_2exp(value.number, -128, 0) >= 0 
				&& FloatMpfr.mpfr_cmp_si_2exp(value.number, 127, 0) <= 0) 

				return (sbyte) FloatMpfr.mpfr_get_si(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than SByte.MaxValue or less than SByte.MinValue");
		}

		public static sbyte ToSByte (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_si(value.number, -128, 1) >= 0 
				&& RatnlMP.__gmpq_cmp_si(value.number, 127, 1) <= 0) 

				return (sbyte) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than SByte.MaxValue or less than SByte.MinValue");
		}

		public static sbyte ToSByte (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_si(value.number, -128) >= 0 
				&& FloatMP.__gmpf_cmp_si(value.number, 127) <= 0) 

				return (sbyte) FloatMP.__gmpf_get_si(value.number); 

			else throw new OverflowException(
					 "Value is greater than SByte.MaxValue or less than SByte.MinValue");
		}


		#endregion ToSByte

		#region ToChar

		public static char ToChar (IntMP value) 
		{

			if (IntMP.__gmpz_cmp_ui(value.number, 0x0000) >= 0 
				&& IntMP.__gmpz_cmp_ui(value.number, 0xffff) <= 0) 

				return (char) IntMP.__gmpz_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than Char.MaxValue or less than Char.MinValue");

		}

		public static char ToChar (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_cmp_ui_2exp(value.number, 0x0000, 0) >= 0 
				&& FloatMpfr.mpfr_cmp_ui_2exp(value.number, 0xffff, 0) <= 0) 

				return (char) FloatMpfr.mpfr_get_ui(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than Char.MaxValue or less than Char.MinValue");
		}

		public static char ToChar (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_ui(value.number, 0x0000, 1) >= 0 
				&& RatnlMP.__gmpq_cmp_ui(value.number, 0xffff, 1) <= 0) 

				return (char) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Char.MaxValue or less than Char.MinValue");
		}

		public static char ToChar (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_ui(value.number, 0x0000) >= 0 
				&& FloatMP.__gmpf_cmp_ui(value.number, 0xffff) <= 0) 

				return (char) FloatMP.__gmpf_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than Char.MaxValue or less than Char.MinValue");
		}


		#endregion ToChar

		#region ToInt16

		public static short ToInt16 (IntMP value) 
		{

			if (IntMP.__gmpz_fits_sshort_p(value.number) != 0 )

				return (short) IntMP.__gmpz_get_si(value.number); 

			else throw new OverflowException(
					 "Value is greater than Int16.MaxValue or less than Int16.MinValue");

		}

		public static short ToInt16 (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_fits_sshort_p(value.number, FloatMpfr.DefaultRoundMode) != 0 )

				return (short) FloatMpfr.mpfr_get_si(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than Int16.MaxValue or less than Int16.MinValue");
		}

		public static short ToInt16 (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_si(value.number, -32768, 1) >= 0 
				&& RatnlMP.__gmpq_cmp_si(value.number, 32767, 1) <= 0) 

				return (short) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Int16.MaxValue or less than Int16.MinValue");
		}

		public static short ToInt16 (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_si(value.number, -32768) >= 0 
				&& FloatMP.__gmpf_cmp_si(value.number, 32767) <= 0) 

				return (short) FloatMP.__gmpf_get_si(value.number); 

			else throw new OverflowException(
					 "Value is greater than Int16.MaxValue or less than Int16.MinValue");
		}


		#endregion ToInt16

		#region ToUInt16

		public static ushort ToUInt16 (IntMP value) 
		{

			if (IntMP.__gmpz_fits_ushort_p(value.number) != 0 )

				return (ushort) IntMP.__gmpz_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than UInt16.MaxValue or less than UInt16.MinValue");

		}

		public static ushort ToUInt16 (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_fits_ushort_p(value.number,FloatMpfr.DefaultRoundMode) != 0 )

				return (ushort) FloatMpfr.mpfr_get_ui(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than UInt16.MaxValue or less than UInt16.MinValue");	
		}

		public static ushort ToUInt16 (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_ui(value.number, 0, 1) >= 0 
				&& RatnlMP.__gmpq_cmp_ui(value.number, 65535, 1) <= 0) 

				return (ushort) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than UInt16.MaxValue or less than UInt16.MinValue");
		}

		public static ushort ToUInt16 (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_ui(value.number, 0) >= 0 
				&& FloatMP.__gmpf_cmp_ui(value.number, 65535) <= 0) 

				return (ushort) FloatMP.__gmpf_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than UInt16.MaxValue or less than UInt16.MinValue");
		}


		#endregion ToUInt16

		#region ToInt32

		public static int ToInt32 (IntMP value) 
		{

			if (IntMP.__gmpz_fits_sint_p(value.number) != 0 )

				return (int) IntMP.__gmpz_get_si(value.number); 

			else throw new OverflowException(
					 "Value is greater than Int32.MaxValue or less than Int32.MinValue");

		}

		public static int ToInt32 (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_fits_slong_p(value.number, FloatMpfr.DefaultRoundMode) != 0 )

				return (int) FloatMpfr.mpfr_get_si(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than Int32.MaxValue or less than Int32.MinValue");		
		}

		public static int ToInt32 (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_si(value.number, -2147483648 , 1) >= 0 
				&& RatnlMP.__gmpq_cmp_si(value.number, 2147483647, 1) <= 0) 

				return (int) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Int32.MaxValue or less than Int32.MinValue");
		}

		public static int ToInt32 (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_si(value.number, -2147483648) >= 0 
				&& FloatMP.__gmpf_cmp_si(value.number, 2147483647) <= 0) 

				return (int) FloatMP.__gmpf_get_si(value.number); 

			else throw new OverflowException(
					 "Value is greater than Int32.MaxValue or less than Int32.MinValue");
		}


		#endregion ToInt32

		#region ToUInt32

        public static uint ToUInt32 (IntMP value) 
		{

			if (IntMP.__gmpz_fits_uint_p(value.number) != 0 )

				return (uint) IntMP.__gmpz_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than Byte.MaxValue or less than Byte.MinValue");

		}

		public static uint ToUInt32 (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_fits_ulong_p(value.number, FloatMpfr.DefaultRoundMode) != 0 )

				return (uint) FloatMpfr.mpfr_get_ui(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than UInt32.MaxValue or less than UInt32.MinValue");
		}

		public static uint ToUInt32 (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp_ui(value.number, 0, 1) >= 0 
				&& RatnlMP.__gmpq_cmp_ui(value.number, 4294967295, 1) <= 0) 

				return (uint) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than UInt32.MaxValue or less than UInt32.MinValue");
		}

		public static uint ToUInt32 (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_ui(value.number, 0) >= 0 
				&& FloatMP.__gmpf_cmp_ui(value.number, 4294967295) <= 0) 

				return (uint) FloatMP.__gmpf_get_ui(value.number); 

			else throw new OverflowException(
					 "Value is greater than UInt32.MaxValue or less than UInt32.MinValue");
		}


		#endregion ToUInt32

		#region ToInt64

		public static long ToInt64 (IntMP value) 
		{


#if x86_64
			if (IntMP.__gmpz_fits_slong_p(value.number) != 0) {
				long result = IntMP.__gmpz_get_si(value.number);
#else
			if (IntMP.__gmpz_cmp(value.number, ( new IntMP (-9223372036854775808L)).number) >= 0 
				&& IntMP.__gmpz_cmp(value.number, ( new IntMP (9223372036854775807L)).number) <= 0) 
			{
				
				
				long result = IntMP.__gmpz_getlimbn(value.number, 0);
				result += ( ((long)IntMP.__gmpz_getlimbn(value.number, 1)) << 32);
#endif		
				return result; 
			}

			else throw new OverflowException(
					 "Value is greater than Int64.MaxValue or less than Int64.MinValue");

		}

		public static long ToInt64 (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static long ToInt64 (RatnlMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static long ToInt64 (FloatMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}


		#endregion ToInt64

		#region ToUInt64

		public static ulong ToUInt64 (IntMP value) 
		{

#if x86_64	
			if (IntMP.__gmpz_sizeinbase(value.number, 2) <= 64) {
				return IntMP.__gmpz_get_ui(value.number);
#else
			if (IntMP.__gmpz_cmp_ui(value.number, 0) >= 0 
				&& IntMP.__gmpz_cmp(value.number, ( new IntMP (18446744073709551615)).number) <= 0) 
			{
				ulong result = IntMP.__gmpz_getlimbn_ui(value.number, 0);
				result += ( ((ulong)IntMP.__gmpz_getlimbn_ui(value.number, 1)) << 32);
				return result; 
#endif
			}

			else throw new OverflowException(
					 "Value is greater than UInt64.MaxValue or less than UInt64.MinValue");

		}

		public static ulong ToUInt64 (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static ulong ToUInt64 (RatnlMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static ulong ToUInt64 (FloatMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}


		#endregion ToUInt64

		#region ToDateTime

		public static DateTime ToDateTime (IntMP value) 
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static DateTime ToDateTime (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static DateTime ToDateTime (RatnlMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static DateTime ToDateTime (FloatMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}


		#endregion ToDateTime

		#region ToDecimal

		public static decimal ToDecimal (IntMP value) 
		{
			// It is horrible hack :/
			if (IntMP.__gmpz_cmp(value.number, (new IntMP( "-79228162514264337593543950335").number)) >= 0 
				&& IntMP.__gmpz_cmp(value.number, (new IntMP( "79228162514264337593543950335")).number) <= 0)
			{
				int[] buff = new int[3];
				mpz_t number = value.NativeIntMP;
				//int bitsPerLimb = Utilities.BitsPerLimb;
				//if (bitsPerLimb == 64)
				//{
				//}
				//else if (bitsPerLimb == 32)
				//{
				//}
				//else
				//{

				//}
#if x86_64
				// TODO: make it faster
				ulong value1 = IntMP.__gmpz_getlimbn(value.number, 0);
				ulong value2 = number.mp_size > 1 ? IntMP.__gmpz_getlimbn(value.number, 1) : 0;
				buff[0] = (int) (value1 & 0x00000000ffffffff);
				buff[1] = (int)((value1 & 0xffffffff00000000) >> 32);
				buff[2] = (int) (value2 & 0x00000000ffffffff);
#else
				for (uint i = 0; i < number.mp_size && i < 3 ; i++) 
					buff[i] = (int) IntMP.__gmpz_getlimbn(value.number, i);
#endif
				return new decimal(buff[0], buff[1], buff[2], value < 0, 0);

			}

			else throw new OverflowException(
					 "Value is greater than Decimal.MaxValue or less than Decimal.MinValue");

		}

		public static decimal ToDecimal (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static decimal ToDecimal (RatnlMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static decimal ToDecimal (FloatMP value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}


		#endregion ToDecimal

		#region ToDouble

		public static double ToDouble (IntMP value) 
		{

			if (IntMP.__gmpz_cmp_d(value.number, Double.MinValue) >= 0 
				&& IntMP.__gmpz_cmp_d(value.number, Double.MaxValue) <= 0) 

				return IntMP.__gmpz_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Double.MaxValue or less than Double.MinValue");

		}

		public static double ToDouble (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_cmp_d(value.number, Double.MinValue) >= 0 
				&& FloatMpfr.mpfr_cmp_d(value.number, Double.MaxValue) <= 0) 

				return (double) FloatMpfr.mpfr_get_d(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than Double.MaxValue or less than Double.MinValue");		
		}

		public static double ToDouble (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp(value.number, (new RatnlMP(Double.MinValue)).number) >= 0 
				&& RatnlMP.__gmpq_cmp(value.number, (new RatnlMP(Double.MaxValue)).number) <= 0) 

				return RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Double.MaxValue or less than Double.MinValue");
		}

		public static double ToDouble (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_d(value.number, Double.MinValue) >= 0 
				&& FloatMP.__gmpf_cmp_d(value.number, Double.MaxValue) <= 0) 

				return FloatMP.__gmpf_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Double.MaxValue or less than Double.MinValue");
		}


		#endregion ToDouble

		#region ToSingle

		public static float ToSingle (IntMP value) 
		{

			if (IntMP.__gmpz_cmp_d(value.number, Single.MinValue) >= 0 
				&& IntMP.__gmpz_cmp_d(value.number, Single.MaxValue) <= 0) 

				return (float) IntMP.__gmpz_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Byte.MaxValue or less than Byte.MinValue");

		}

		public static float ToSingle (FloatMpfr value) 
		{ 
			if (FloatMpfr.mpfr_cmp_d(value.number, Single.MinValue) >= 0 
				&& FloatMpfr.mpfr_cmp_d(value.number, Single.MaxValue) <= 0) 

				return (float) FloatMpfr.mpfr_get_d(value.number, FloatMpfr.DefaultRoundMode); 

			else throw new OverflowException(
					 "Value is greater than Single.MaxValue or less than Single.MinValue");		
		}

		public static float ToSingle (RatnlMP value) 
		{ 
			if (RatnlMP.__gmpq_cmp(value.number, (new RatnlMP(Single.MinValue)).number) >= 0 
				&& RatnlMP.__gmpq_cmp(value.number, (new RatnlMP(Single.MaxValue)).number) <= 0) 

				return (float) RatnlMP.__gmpq_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Single.MaxValue or less than Single.MinValue");
		}

		public static float ToSingle (FloatMP value) 
		{ 
			if (FloatMP.__gmpf_cmp_d(value.number, Single.MinValue) >= 0 
				&& FloatMP.__gmpf_cmp_d(value.number, Single.MaxValue) <= 0) 

				return (float) FloatMP.__gmpf_get_d(value.number); 

			else throw new OverflowException(
					 "Value is greater than Double.MaxValue or less than Double.MinValue");
		}


		#endregion ToSingle

		#region ToString

		public static string ToString (IntMP value) 
		{
			return value.ToString();
		}

		public static string ToString (FloatMpfr value) 
		{ 
			return value.ToString();
		}

		public static string ToString (RatnlMP value) 
		{ 
			return value.ToString();
		}

		public static string ToString (FloatMP value) 
		{ 
			return value.ToString();
		}

		public static string ToString (IntMP value, IFormatProvider provider) 
		{
			return value.ToString(provider);
		}

		public static string ToString (FloatMpfr value, IFormatProvider provider) 
		{ 
			return value.ToString(provider);
		}

		public static string ToString (RatnlMP value, IFormatProvider provider) 
		{ 
			return value.ToString(provider);
		}

		public static string ToString (FloatMP value, IFormatProvider provider) 
		{ 
			return value.ToString(provider);
		}


		#endregion ToString

		#region ToIntMP

		public static IntMP ToIntMP (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static IntMP ToIntMP (RatnlMP value) 
		{ 
			return (IntMP) value;
		}

		public static IntMP ToIntMP (FloatMP value) 
		{ 
			return (IntMP) value;
		}

		public static IntMP ToIntMP (bool value) 
		{ 
			return value ? new IntMP(1) : new IntMP(0); 
		}

		public static IntMP ToIntMP (byte value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (sbyte value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (char value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (short value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (ushort value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (int value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (uint value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (long value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (ulong value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (float value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (double value) 
		{ 
			return (IntMP) value; 
		}

		public static IntMP ToIntMP (decimal value) 
		{ 
			return (IntMP) value;
		}

		public static IntMP ToIntMP (DateTime value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static IntMP ToIntMP (string value) 
		{ 
			return IntMP.Parse(value);
		}

		public static IntMP ToIntMP (string value, IFormatProvider provider) 
		{ 
			return IntMP.Parse(value, provider);
		}

		public static IntMP ToIntMP (object value) 
		{ 
			if (value == null) return new IntMP(0);
			else return ToIntMP(value, null);
		}

		public static IntMP ToIntMP (object value, IFormatProvider provider) 
		{ 
			if (value == null) return new IntMP(0);
			else return ((IConvertibleMP)value).ToIntMP(provider);
		}


		#endregion ToIntMP

		#region ToFloatMP

		public static FloatMP ToFloatMP (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static FloatMP ToFloatMP (RatnlMP value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (IntMP value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (bool value) 
		{ 
			return value ? new FloatMP(1) : new FloatMP(0); 
		}

		public static FloatMP ToFloatMP (byte value) 
		{ 
			return (FloatMP) value; 
		}

		public static FloatMP ToFloatMP (sbyte value) 
		{ 
			return (FloatMP) value; 
		}

		public static FloatMP ToFloatMP (char value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (short value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (ushort value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (int value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (uint value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (long value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (ulong value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (float value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (double value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (decimal value) 
		{ 
			return (FloatMP) value;
		}

		public static FloatMP ToFloatMP (DateTime value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static FloatMP ToFloatMP (string value) 
		{ 
			return FloatMP.Parse(value);
		}

		public static FloatMP ToFloatMP (string value, IFormatProvider provider) 
		{ 
			return FloatMP.Parse(value, provider);
		}

		public static FloatMP ToFloatMP (object value) 
		{ 
			if (value == null) return new FloatMP();
			else return ToFloatMP(value, null);
		}

		public static FloatMP ToFloatMP (object value, IFormatProvider provider) 
		{ 
			if (value == null) return new FloatMP();
			else return ((IConvertibleMP)value).ToFloatMP(provider);
		}

		#endregion ToFloatMP

		#region ToRatnlMP

		public static RatnlMP ToRatnlMP (FloatMpfr value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static RatnlMP ToRatnlMP (IntMP value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (FloatMP value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (bool value) 
		{ 
			return value ? new RatnlMP(1) : new RatnlMP(0);
		}

		public static RatnlMP ToRatnlMP (byte value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (sbyte value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (char value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (short value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (ushort value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (int value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (uint value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (long value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (ulong value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (float value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (double value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (decimal value) 
		{ 
			return (RatnlMP) value;
		}

		public static RatnlMP ToRatnlMP (DateTime value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static RatnlMP ToRatnlMP (string value) 
		{ 
			return RatnlMP.Parse(value);
		}

		public static RatnlMP ToRatnlMP (string value, IFormatProvider provider) 
		{ 
			return RatnlMP.Parse(value, provider);
		}

		public static RatnlMP ToRatnlMP (object value) 
		{ 
			if (value == null) return new RatnlMP();
			else return ToRatnlMP(value, null);
		}

		public static RatnlMP ToRatnlMP (object value, IFormatProvider provider) 
		{ 
			if (value == null) return new RatnlMP();
			else return ((IConvertibleMP)value).ToRatnlMP(provider);
		}

		#endregion ToRatnlMP

		#region ToFloatMpfr

		public static FloatMpfr ToFloatMpfr (IntMP value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (RatnlMP value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (FloatMP value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (bool value) 
		{ 
			return value ? new FloatMpfr(1) : new FloatMpfr(0);
		}

		public static FloatMpfr ToFloatMpfr (byte value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (sbyte value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (char value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (short value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (ushort value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (int value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (uint value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (long value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (ulong value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (float value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (double value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (decimal value) 
		{ 
			return (FloatMpfr) value;
		}

		public static FloatMpfr ToFloatMpfr (DateTime value) 
		{ 
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public static FloatMpfr ToFloatMpfr (string value) 
		{ 
			return FloatMpfr.Parse(value);
		}

		public static FloatMpfr ToFloatMpfr (string value, IFormatProvider provider) 
		{ 
			return FloatMpfr.Parse(value, provider);
		}

		public static FloatMpfr ToFloatMpfr (object value) 
		{ 
			if (value == null) return new FloatMpfr();
			else return ToFloatMpfr(value, null);
		}

		public static FloatMpfr ToFloatMpfr (object value, IFormatProvider provider) 
		{ 
			if (value == null) return new FloatMpfr();
			else return ((IConvertibleMP)value).ToFloatMpfr(provider);
		}

		#endregion ToFloatMpfr

// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
// Code adapted from Mono

		// This fragment is an adapted code from Convert.cs file of Mono-1.0

		//
		// System.Convert.cs
		//
		// Author:
		//   Derek Holden (dholden@draper.com)
		//   Duncan Mak (duncan@ximian.com)
		//
		// (C) Ximian, Inc.  http://www.ximian.com
		//
		//
		//
		// Copyright (C) 2004 Novell, Inc (http://www.novell.com)
		//
		// Permission is hereby granted, free of charge, to any person obtaining
		// a copy of this software and associated documentation files (the
		// "Software"), to deal in the Software without restriction, including
		// without limitation the rights to use, copy, modify, merge, publish,
		// distribute, sublicense, and/or sell copies of the Software, and to
		// permit persons to whom the Software is furnished to do so, subject to
		// the following conditions:
		// 
		// The above copyright notice and this permission notice shall be
		// included in all copies or substantial portions of the Software.
		// 
		// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
		// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
		// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
		// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
		// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
		// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
		// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
		//

		#region Conversion Table


		// Lookup table for the conversion ToType method. Order
		// is important! Used by ToType for comparing the target
		// type, and uses hardcoded array indexes.
		private static readonly Type[] conversionTable = {

			// Valid ICovnertible Types
			null,				//	0 empty
			typeof (object),	//  1 TypeCode.Object
			typeof (DBNull),	//  2 TypeCode.DBNull
			typeof (Boolean),	//  3 TypeCode.Boolean
			typeof (Char),		//  4 TypeCode.Char
			typeof (SByte),		//  5 TypeCode.SByte
			typeof (Byte),		//  6 TypeCode.Byte
			typeof (Int16),		//  7 TypeCode.Int16
			typeof (UInt16),	//  8 TypeCode.UInt16
			typeof (Int32),		//  9 TypeCode.Int32
			typeof (UInt32),	// 10 TypeCode.UInt32
			typeof (Int64),		// 11 TypeCode.Int64
			typeof (UInt64),	// 12 TypeCode.UInt64
			typeof (Single),	// 13 TypeCode.Single
			typeof (Double),	// 14 TypeCode.Double
			typeof (Decimal),	// 15 TypeCode.Decimal
			typeof (DateTime),	// 16 TypeCode.DateTime
			null,				// 17 null.
			typeof (String),	// 18 TypeCode.String
			
			// Valid IConvertibleMP Types
			typeof(IntMP),		// 19 TypeCode.IntMP
			typeof(RatnlMP),	// 20 TypeCode.RatnlMP
			typeof(FloatMP),	// 21 TypeCode.FloatMP
			typeof(FloatMpfr)	// 22 TypeCode.FloatMpfr


		};

		#endregion Conversion Table


		public static object ChangeType (object value, TypeCode typeCode)
		{

			CultureInfo ci = CultureInfo.CurrentCulture;
			Type conversionType = conversionTable [(int) typeCode];
			NumberFormatInfo number = ci.NumberFormat;
			return ToType (value, conversionType, number);

		}

		public static object ChangeType (object value, Type conversionType, IFormatProvider provider)
		{

			if ((value != null) && (conversionType == null))
				throw new ArgumentNullException ("conversionType");

			return ToType (value, conversionType, provider);

		}

		public static object ChangeType (object value, TypeCode typeCode, IFormatProvider provider)
		{

			Type conversionType = conversionTable [(int)typeCode];
			return ToType (value, conversionType, provider);

		}


		public static object ToType (object value, Type conversionType, IFormatProvider provider)
		{

			if (value == null) return null;

			if (conversionType == null) 
				throw new InvalidCastException ("Cannot cast to destination type.");

			if (value.GetType () == conversionType) return value;

			if (value is IConvertible) 
			{
				IConvertible convertValue = (IConvertible) value;

				if (conversionType == conversionTable[0]) // 0 Empty
					throw new ArgumentNullException ();

				else if (conversionType == conversionTable[1]) // 1 TypeCode.Object
					return (object) value;

				else if (conversionType == conversionTable[2]) // 2 TypeCode.DBNull
					throw new InvalidCastException (
						"Cannot cast to DBNull, it's not IConvertible");

				else if (conversionType == conversionTable[3]) // 3 TypeCode.Boolean
					return (object) convertValue.ToBoolean (provider);

				else if (conversionType == conversionTable[4]) // 4 TypeCode.Char
					return (object) convertValue.ToChar (provider);

				else if (conversionType == conversionTable[5]) // 5 TypeCode.SByte
					return (object) convertValue.ToSByte (provider);

				else if (conversionType == conversionTable[6]) // 6 TypeCode.Byte
					return (object) convertValue.ToByte (provider);
				
				else if (conversionType == conversionTable[7]) // 7 TypeCode.Int16
					return (object) convertValue.ToInt16 (provider);
					
				else if (conversionType == conversionTable[8]) // 8 TypeCode.UInt16
					return (object) convertValue.ToUInt16 (provider);
		  
				else if (conversionType == conversionTable[9]) // 9 TypeCode.Int32
					return (object) convertValue.ToInt32 (provider);
			
				else if (conversionType == conversionTable[10]) // 10 TypeCode.UInt32
					return (object) convertValue.ToUInt32 (provider);
		  
				else if (conversionType == conversionTable[11]) // 11 TypeCode.Int64
					return (object) convertValue.ToInt64 (provider);
		  
				else if (conversionType == conversionTable[12]) // 12 TypeCode.UInt64
					return (object) convertValue.ToUInt64 (provider);
		  
				else if (conversionType == conversionTable[13]) // 13 TypeCode.Single
					return (object) convertValue.ToSingle (provider);
		  
				else if (conversionType == conversionTable[14]) // 14 TypeCode.Double
					return (object) convertValue.ToDouble (provider);

				else if (conversionType == conversionTable[15]) // 15 TypeCode.Decimal
					return (object) convertValue.ToDecimal (provider);

				else if (conversionType == conversionTable[16]) // 16 TypeCode.DateTime
					return (object) convertValue.ToDateTime (provider);
				
				else if (conversionType == conversionTable[18]) // 18 TypeCode.String
					return (object) convertValue.ToString (provider);

				else if (value is IConvertibleMP)
				{
					IConvertibleMP convertValueMP = (IConvertibleMP) value;

					if (conversionType == conversionTable[18]) // 19 TypeCode.IntMP
						return (object) convertValueMP.ToIntMP (provider);

					else if (conversionType == conversionTable[20]) // 20 TypeCode.RatnlMP
						return (object) convertValueMP.ToRatnlMP (provider);

					else if (conversionType == conversionTable[21]) // 21 TypeCode.FloatMP
						return (object) convertValueMP.ToFloatMP (provider);

					else if (conversionType == conversionTable[22]) // 22 TypeCode.FloatMpfr
						return (object) convertValueMP.ToFloatMpfr (provider);

					else 
					{
						try 
						{ 
							return (object) convertValue; 
						}
						catch 
						{
							throw new ArgumentException 
								("Unknown target conversion type"); 
						}
					}
				}
			}
			else 
			{
				// Not in the IConvertible or IConvertibleMP scope
				throw new InvalidCastException (
					"Value is not a convertible object: " + value.GetType().ToString() + " to " + conversionType.FullName);
			}
			return null;
		}
					


// End code adapted from Mono
//xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

	}

	#endregion Class ConvertMP
}
