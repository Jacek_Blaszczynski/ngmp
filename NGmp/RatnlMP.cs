/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;


namespace NGmp.Math 
{

	#region Class RatnlMP

	/// <summary>
	/// Class RatnlMP provides arbitrary precision rational arithmetic over 
	/// RatnlMP numbers. Class provides CLI framework bindings
	/// for GNU MP library (GMP) rational numbers: http://www.swox.com/gmp/.
	/// Supported GNU MP version = 4.1.3 - 4.1.4
	/// </summary>
	[Serializable]
	[SuppressUnmanagedCodeSecurity]
	public sealed class RatnlMP : 
		IDisposable, 
		IComparable, 
		ICloneable, 
		ISerializable, 
		IConvertible,				// Duplicate due to IConvertibleMP
		IConvertibleMP
#if NET_2_0
		,
		IComparable<RatnlMP>,
		IEquatable<RatnlMP>
#endif
	{
		#region Fields

		private static int mMpqCoreNativeSize = Marshal.SizeOf(typeof(mpq_t));

		internal IntPtr number = IntPtr.Zero;
		private bool disposed = false;

		#endregion Fields

		#region Constructors & Dispose Methods

		#region Constructors

		public RatnlMP()
		{
			this.number = Marshal.AllocHGlobal(mMpqCoreNativeSize);
			__gmpq_init(this.number);
		}

		public RatnlMP(RatnlMP rational) : this()
		{
			__gmpq_set(this.number, rational.number);
		}

		public RatnlMP(IntMP rational) : this() 
		{
			__gmpq_set_z(this.number, rational.number);
		}

		public RatnlMP(uint numerator, uint denominator) : this(numerator, denominator, true) 
		{
		}

		public RatnlMP(uint numerator, uint denominator, bool canonicalize) : this() 
		{
			__gmpq_set_ui(this.number, numerator, denominator);
			if (canonicalize) __gmpq_canonicalize(this.number);
		}

		public RatnlMP(int numerator, int denominator) : this(numerator, denominator, true) 
		{
		}

		public RatnlMP(int numerator, int denominator, bool canonicalize) : this() 
		{
			__gmpq_set_si(this.number, numerator, denominator);
			if (canonicalize) __gmpq_canonicalize(this.number);
		}

		public RatnlMP(double rational) : this ()
		{
			__gmpq_set_d(this.number, rational);
		}

		public RatnlMP(FloatMP rational) : this()
		{
			__gmpq_set_f(this.number, rational.number);
		}

		public RatnlMP(String rational) : this(rational, 10, true) 
		{
		}

		public RatnlMP(String rational, bool canonicalize) : this(rational, 10, canonicalize) 
		{
		}

		public RatnlMP(String rational, int radix, bool canonicalize) : this()
		{
			__gmpq_set_str(this.number, rational, radix);
			if (canonicalize) __gmpq_canonicalize(this.number);
		}

		public RatnlMP(SerializationInfo info, StreamingContext context) : this()
		{
			__gmpq_set_str(this.number, info.GetString("value"), 36);
//			this.Import(true, (byte[]) info.GetValue("numerator", typeof(byte[])));
//			this.Import(false, (byte[]) info.GetValue("denominator", typeof(byte[])));
		}

		#endregion Constructors

		#region IDispose Implementation

		// Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!this.disposed)
			{
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if(disposing)
				{
					// Dispose managed resources.
				}
             
				// Call the appropriate methods to clean up 
				// unmanaged resources here.
				// If disposing is false, 
				// only the following code is executed.
				if (this.number == IntPtr.Zero) 
				{
					__gmpq_clear(this.number);
					this.number = IntPtr.Zero;       
				}
			}
			disposed = true;         
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~RatnlMP()
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}


		#endregion IDispose Implementation

		#endregion Constructors & Dispose Methods

		#region Properties

		public bool IsDisposed
		{
			get {return this.disposed;}
		}

		public IntPtr MPNumber
		{
			get {return this.number;}
			set {this.number = value;}
		}

		public IntMP Numerator
		{
			get 
			{
				IntMP result = new IntMP();
				__gmpq_get_num(result.number, this.number);
				return result;
			}
			set { __gmpq_set_num(this.number, value.number);}
		}

		public IntMP Denominator 
		{
			get 
			{
				IntMP result = new IntMP();
				__gmpq_get_den(result.number, this.number);
				return result;
			}
			set { __gmpq_set_den(this.number, value.number);}
		}


		#endregion Properties

		#region GMP P/Invoke Prototypes

		#region Initialization & Memory Mngmnt

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_init (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_canonicalize (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_clear (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational);

		#endregion Initialization & Memory Mngmnt

		#region Assignment

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_z (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_si (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int operand1, 
			int operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_d (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_f (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern int __gmpq_set_str (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.LPStr)] String str /*char *str*/, 
			int radix);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_swap (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rop1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rop2);

		#endregion Assignment

		#region Conversion

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double __gmpq_get_d (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern void __gmpq_get_str ( 
			[Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder str, 
			int radix, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		#endregion Conversion

		#region Arithmetic

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_add (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr sum, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr addend1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr addend2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_sub (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr difference, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr minuend, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr subtrahend);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_mul (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr product, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr multiplier, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr multiplicand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_mul_2exp (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_neg (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr negated_operand, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_abs (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_inv (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr inverted_number, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr number);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_div (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr quotient, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr dividend, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr divisor);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_div_2exp (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		#endregion Arithmetic

		#region Comparison

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpq_cmp (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpq_cmp_si (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int num2, 
			uint den2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpq_cmp_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint num2, 
			uint den2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpq_sgn (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpq_equal (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		#endregion Comparison

		#region IO

		/// <summary>
		/// Output operand on stdio stream stream, as a string of digits in base base. 
		/// The base may vary from 2 to 36. Output is in the form num/den or 
		/// if the denominator is 1 then just num.
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="radix"></param>
		/// <param name="operand"></param>
		/// <returns>Return the number of bytes written, or if an error occurred, return 0.</returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern uint __gmpq_out_str (
			IntPtr stream, 
			int radix, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Read a string of digits from stream and convert them to a rational in result. 
		/// Any initial white-space characters are read and discarded. Return the number 
		/// of characters read (including white space), or 0 if a rational could not be read.
		/// The input can be a fraction like 17/63 or just an integer like 123. 
		/// Reading stops at the first character not in this form, and white space is not 
		/// permitted within the string. If the input might not be in canonical form, 
		/// then __gmpq_canonicalize must be called (see Rational Number Functions). 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="stream"></param>
		/// <param name="radix">The base can be between 2 and 36, or can be 0 in which case 
		/// the leading characters of the string determine the base, 0x or 0X for hexadecimal, 
		/// 0 for octal, or decimal otherwise. The leading characters are examined separately 
		/// for the numerator and denominator of a fraction, so for instance 0x10/11 is 16/11, 
		/// whereas 0x10/0x11 is 16/17.</param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern uint __gmpq_inp_str (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			IntPtr stream, 
			int radix);

		#endregion IO

		#region Miscallenous

		/// <summary>
		/// Return a reference to the numerator of operand. 
		/// The mpz functions can be used on the result of this macro. 
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern mpz_t __gmpq_numref (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Return a reference to the denominator of operand. 
		/// The mpz functions can be used on the result of this macro. 
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern mpz_t __gmpq_denref (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Get or set the numerator or denominator of a rational. 
		/// These functions are equivalent to calling mpz_set with 
		/// an appropriate mpq_numref or mpq_denref. Direct use of 
		/// mpq_numref or mpq_denref is recommended instead of these functions. 
		/// </summary>
		/// <param name="numerator"></param>
		/// <param name="rational"></param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_get_num (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr numerator, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational);

		/// <summary>
		/// Get or set the numerator or denominator of a rational. 
		/// These functions are equivalent to calling mpz_set with 
		/// an appropriate mpq_numref or mpq_denref. Direct use of 
		/// mpq_numref or mpq_denref is recommended instead of these functions. 
		/// </summary>
		/// <param name="denominator"></param>
		/// <param name="rational"></param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_get_den (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr denominator, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational);

		/// <summary>
		/// Get or set the numerator or denominator of a rational. 
		/// These functions are equivalent to calling mpz_set with 
		/// an appropriate mpq_numref or mpq_denref. Direct use of 
		/// mpq_numref or mpq_denref is recommended instead of these functions. 
		/// </summary>
		/// <param name="rational"></param>
		/// <param name="numerator"></param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_num (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr numerator);

		/// <summary>
		/// Get or set the numerator or denominator of a rational. 
		/// These functions are equivalent to calling mpz_set with 
		/// an appropriate mpq_numref or mpq_denref. Direct use of 
		/// mpq_numref or mpq_denref is recommended instead of these functions. 
		/// </summary>
		/// <param name="rational"></param>
		/// <param name="denominator"></param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpq_set_den (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rational, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr denominator);


		#endregion Miscallenous

		#endregion GMP P/Invoke Prototypes

		#region Object Overrides

		public override bool Equals(object obj)
		{
			throw new NotImplementedException("Method is not implemented yet.");
		}

		public override int GetHashCode()
		{
			mpq_t value = (mpq_t) Marshal.PtrToStructure(this.number, typeof(mpq_t));
			int hash1 = IntMP.GetHashCode(value._mp_num_mp_size, value._mp_den_mp_d);
			int hash2 = IntMP.GetHashCode(value._mp_den_mp_size, value._mp_den_mp_d);
			hash2 ^= hash1;
			return hash2;
		}

		public override string ToString()
		{
			return this.ToString(10);
		}

		/// <summary>
		/// Converts this RatnlMP to string given the number base in radix.
		/// </summary>
		/// <param name="radix">Accepted values are in range from 2 to 36 inclusive.</param>
		/// <returns></returns>
		public string ToString(int radix) 
		{
			if (radix >= 2 && radix <= 36) 
			{
				StringBuilder str;
				ulong denSize = 0;
				ulong numSize = IntMP.__gmpz_sizeinbase(this.Numerator.number, radix);
				if (this.Denominator != 1) 
				{
					denSize = IntMP.__gmpz_sizeinbase(this.Denominator.number, radix);
				}
				str = new StringBuilder((int)numSize + (int)denSize + 3);
				__gmpq_get_str(str, radix, this.number);
				return str.ToString();
			}
			else throw new ArgumentOutOfRangeException("radix", radix, 
					"Argument value otu of accpted range. Value from2 to 36 inclusive expected.");
		}

		

		#endregion Object Overrides

		#region IComparable Members

		public int CompareTo(object obj)
		{
			if (obj is RatnlMP) return __gmpq_cmp(this.number, ((RatnlMP) obj).number);
			else throw new ArgumentException(@"CompareTo argument obj has wrong Type. RatnlMP expected", "obj");
		}

		#endregion

#if NET_2_0
		#region IComparable<> Members

		public int CompareTo(RatnlMP value)
		{
			return __gmpq_cmp(this.number, (value.number));
		}

		#endregion IComparable<> Members
#endif

		#region IConvertible Members

		ulong System.IConvertible.ToUInt64(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		sbyte System.IConvertible.ToSByte(IFormatProvider provider)
		{
			return ConvertMP.ToSByte(this);
		}

		double System.IConvertible.ToDouble(IFormatProvider provider)
		{
			return ConvertMP.ToDouble(this);
		}

		DateTime System.IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		float System.IConvertible.ToSingle(IFormatProvider provider)
		{
			return ConvertMP.ToSingle(this);
		}

		bool System.IConvertible.ToBoolean(IFormatProvider provider)
		{
			return ConvertMP.ToBoolean(this);
		}

		int System.IConvertible.ToInt32(IFormatProvider provider)
		{
			return ConvertMP.ToInt32(this);
		}

		ushort System.IConvertible.ToUInt16(IFormatProvider provider)
		{
			return ConvertMP.ToUInt16(this);
		}

		short System.IConvertible.ToInt16(IFormatProvider provider)
		{
			return ConvertMP.ToInt16(this);
		}

		public string ToString(IFormatProvider provider)
		{
			return ConvertMP.ToString(this, provider);
		}

		byte System.IConvertible.ToByte(IFormatProvider provider)
		{
			return ConvertMP.ToByte(this);
		}

		char System.IConvertible.ToChar(IFormatProvider provider)
		{
			return ConvertMP.ToChar(this);
		}

		long System.IConvertible.ToInt64(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public System.TypeCode GetTypeCode()
		{
			return (System.TypeCode) 20;
		}

		decimal System.IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public object ToType(Type conversionType, IFormatProvider provider)
		{
			return this.GetType();
		}

		uint System.IConvertible.ToUInt32(IFormatProvider provider)
		{
			return ConvertMP.ToUInt32(this);
		}

		#endregion

		#region IConvertibleMP Members

		public IntMP ToIntMP(IFormatProvider provider)
		{
			// TODO:  Add RatnlMP.ToIntMP implementation
			return null;
		}

		public FloatMP ToFloatMP(IFormatProvider provider)
		{
			// TODO:  Add RatnlMP.ToFloatMP implementation
			return null;
		}

		public FloatMpfr ToFloatMpfr(IFormatProvider provider)
		{
			// TODO:  Add RatnlMP.ToFloatMpfr implementation
			return null;
		}

		public NGmp.Math.RatnlMP ToRatnlMP(IFormatProvider provider)
		{
			return this;
		}

		#endregion

		#region ICloneable Members

		public object Clone()
		{
			return new RatnlMP(this);
		}

		#endregion ICloneable Members

#if NET_2_0
		#region IEquatable<> Members

		public bool Equals(RatnlMP value)
		{
			return __gmpq_cmp(this.number, (value.number)) == 0;
		}

		#endregion IEquatable<> Members
#endif

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("value", this.ToString(36));
//			info.AddValue("numerator", this.Export(true), typeof(byte[]));
//			info.AddValue("denominator", this.Export(false), typeof(byte[]));
		}

		#endregion ISerializable Members

		#region Static Methods

		#region Assignment

		public static void Set (RatnlMP result, RatnlMP operand) 
		{
			__gmpq_set(result.number, operand.number);
		}

		public static void Set (RatnlMP result, IntMP operand) 
		{
			__gmpq_set_z(result.number, operand.number);
		}

		public static void Set (RatnlMP result, FloatMP operand) 
		{
			__gmpq_set_f(result.number, operand.number);
		}

		public static void Set (RatnlMP result, uint numerator, uint denomiantor)
		{
			__gmpq_set_ui(result.number, numerator, denomiantor);
		}

		public static void Set (RatnlMP result, int numerator, int denomiantor)
		{
			__gmpq_set_si(result.number, numerator, denomiantor);
		}

		public static void Set (RatnlMP result, double operand) 
		{
			__gmpq_set_d(result.number, operand);
		}

		public static void Set (RatnlMP result, String value, int radix) 
		{
			__gmpq_set_str(result.number, value, radix);
		}

		public static void Set (RatnlMP result, String value) 
		{
			__gmpq_set_str(result.number, value, 10);
		}

		public static void Canonicalize (RatnlMP result) 
		{
			__gmpq_canonicalize(result.number);
		}

		public static void Swap (RatnlMP value1, RatnlMP value2) 
		{
			__gmpq_swap(value1.number, value2.number);
		}

		#endregion Assignment

		#region Arithmetic

		public static void Add (RatnlMP sum, RatnlMP operand1, RatnlMP operand2) 
		{
			__gmpq_add(sum.number, operand1.number, operand2.number);
		}

		public static void Sub (RatnlMP difference, RatnlMP minuend, RatnlMP subtrahend) 
		{
			__gmpq_sub(difference.number, minuend.number, subtrahend.number);
		}

		public static void Mul (RatnlMP product, RatnlMP multiplier, RatnlMP multiplicand) 
		{
			__gmpq_mul(product.number, multiplier.number, multiplicand.number);
		}

		/// <summary>
		/// Multiply operand1 times 2 raised to power of operand2 and set result to operation value.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Mul2Exp (RatnlMP result, RatnlMP operand1, uint operand2) 
		{
			__gmpq_mul_2exp(result.number, operand1.number, operand2);
		}

		public static void Neg (RatnlMP result, RatnlMP operand) 
		{
			__gmpq_neg(result.number, operand.number);
		}

		public static void Abs (RatnlMP result, RatnlMP operand) 
		{
			__gmpq_abs(result.number, operand.number);
		}

		public static void Invert (RatnlMP result, RatnlMP operand) 
		{
			__gmpq_inv(result.number, operand.number);
		}

		public static void Div (RatnlMP quotient, RatnlMP dividend, RatnlMP divisor) 
		{
			__gmpq_div(quotient.number, dividend.number, divisor.number);
		}

		/// <summary>
		/// Set result to operand1 divided by 2 raised to operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="opernad2"></param>
		public static void Div2Exp (RatnlMP result, RatnlMP operand1, uint opernad2) 
		{
			__gmpq_div_2exp(result.number, operand1.number, opernad2);
		}


		#endregion Arithmetic

		#region Comparison

		public static int Compare (RatnlMP operand, uint numerator, uint denominator) 
		{
			return __gmpq_cmp_ui(operand.number, numerator, denominator);
		}

		public static int Compare (RatnlMP operand, int numerator, uint denominator) 
		{
			return __gmpq_cmp_si(operand.number, numerator, denominator);
		}

		/// <summary>
		/// Return 1 if opernad > 0, 0 if operand == 0 and -1 if operand is negative.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		public static int Sign (RatnlMP operand) 
		{
			return IntMP.__gmpz_sign(operand.Numerator.number);
		}

		public static bool Equal (RatnlMP operand1, RatnlMP operand2) 
		{
			if (__gmpq_equal(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		#endregion Comparison

		#region Conversion

		public static RatnlMP Parse(String s, System.Globalization.NumberStyles style, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//if (s == null) throw new System.ArgumentNullException("s", "Value of RatnlMP underlying number cannot be null.");
			//RatnlMP result = new RatnlMP();
			//return result;
		}

		public static RatnlMP Parse(String s, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return RatnlMP.Parse(s, NumberStyles.Integer, provider);
		}

		public static RatnlMP Parse(String s, NumberStyles style) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return RatnlMP.Parse(s, style, null);
		}

		public static RatnlMP Parse(string s) 
		{
			if (s == null) throw new System.ArgumentNullException("s", "Value of RatnlMP underlying number cannot be null.");

			// TODO: s format style and format checks
			
			return new RatnlMP(s);
		}

		#endregion Conversion

		#region IO

		public static void Save (String fileName, RatnlMP operand) 
		{
			Save (fileName, 10, operand);
		}

		/// <summary>
		/// Output operand to text file, as a string of digits in base radix. 
		/// The base may vary from 2 to 36. Output is in the form numerator/denominator 
		/// or if the denominator is 1 then just numerator. Save uses native function
		/// __gmpq_out_str which is faster than runtime supported serialization.
		/// </summary>
		/// <param name="fileName">Fully qulified path to file.</param>
		/// <param name="radix">Base in which operand value will be stored.</param>
		/// <param name="operand">Value to store in text file.</param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="IOException"></exception>
		public static void Save (String fileName, int radix, RatnlMP operand) 
		{

			if (fileName == null ) 
				throw new ArgumentNullException("fileName", 
					"Argument is null. Fully qualified path to file expected.");

			if (operand == null ) 
				throw new ArgumentNullException("operand", 
					"Argument is null. Expected valid RatnlMP for saving.");
            
#if NET_2_0
			if (fileName.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
#else
			if (fileName.IndexOfAny(Path.InvalidPathChars) >= 0) 
#endif
				throw new ArgumentException("Invalid characters in path: " + fileName, "fileName");

			if (!Directory.Exists(Path.GetDirectoryName(fileName))) 
				throw new DirectoryNotFoundException(
					"Specified directory does not exist: " + Path.GetDirectoryName(fileName));

			if (radix < 2 || radix > 36 ) 
				throw new ArgumentOutOfRangeException("radix", radix, 
					"Argument radix is out of range. Accepted values are from 2 to 36 inclusive.");

			IntPtr pf = IntPtr.Zero;
			try 
			{
				pf = Utilities.fopen(fileName, "wt");
				if (pf != IntPtr.Zero)
				{
					if (__gmpq_out_str(pf, radix, operand.number)==0)
						throw new IOException("Cannot write to file: " + fileName); 
				}
				else throw new System.IO.IOException("Could not create file: " + fileName);
			}
			finally 
			{
				if (pf != IntPtr.Zero) 
					if (Utilities.fclose(pf)!= 0) 
						throw new System.IO.IOException("Could not close file: " + fileName);
			}
		}

		public static void Open (RatnlMP result, String fileName) 
		{
			Open(result, fileName, 10);
		}

		/// <summary>
		/// Read a string of digits from text file and convert them to a rational number
		/// RatnlMP in result. Any initial white-space characters are read and discarded. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="fileName"></param>
		/// <param name="radix"></param>
		public static void Open (RatnlMP result, String fileName, int radix)
		{

			if (fileName == null ) 
				throw new ArgumentNullException("fileName", 
					"Argument is null. Fully qualified path to file expected.");

			if (result == null ) 
				throw new ArgumentNullException("result", 
					"Argument is null. Expected valid RatnlMP.");
            
#if NET_2_0
			if (fileName.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
#else
			if (fileName.IndexOfAny(Path.InvalidPathChars) >= 0) 
#endif
				throw new ArgumentException("Invalid characters in path: " + fileName, "fileName");

			if (!File.Exists(fileName)) 
				throw new FileNotFoundException("Specified file does not exist.", fileName);

			if ( radix > 36 || radix < 0 || radix == 1 )
				throw new ArgumentOutOfRangeException("radix",
					"Argument radix is out of range. Accepted values are from 2 to 36 inclusive or 0.");


			IntPtr pf = IntPtr.Zero;
			try 
			{
				pf = Utilities.fopen(fileName, "rt");
				if (pf != IntPtr.Zero)
				{
					if (__gmpq_inp_str(result.number, pf, radix)== 0)
						throw new IOException("Cannot read file: " + fileName); 
				}
				else throw new System.IO.IOException("Could not open file: " + fileName);
			}
			finally 
			{
				if (pf != IntPtr.Zero) 
					if (Utilities.fclose(pf)!= 0) 
						throw new System.IO.IOException("Could not close file: " + fileName);
			}		
		}

		#endregion IO

		#endregion Static Methods

		#region Instance Methods

		public void Canonicalize () 
		{
			__gmpq_canonicalize(this.number);
		}

		public void Set (RatnlMP value) 
		{
			__gmpq_set(this.number, value.number);
		}

		public void Set (IntMP value) 
		{
			__gmpq_set_z(this.number, value.number);
		}

		public void Set (uint numerator, uint denominator) 
		{
			__gmpq_set_ui(this.number, numerator, denominator);
		}

		public void Set (int numerator, int denominator) 
		{
			__gmpq_set_si(this.number, numerator, denominator);
		}

		public void Set (double value) 
		{
			__gmpq_set_d(this.number, value);
		}

		public void Set (FloatMP value) 
		{
			__gmpq_set_f(this.number, value.number);
		}

		public void Set (String value) 
		{
			__gmpq_set_str(this.number, value, 10);
		}

		public void Set (String value, int radix) 
		{
			__gmpq_set_str(this.number, value, radix);
		}


		private byte[] Export(bool numerator)
		{
			byte[] result = null;
			System.IntPtr array = IntPtr.Zero; 
			System.IntPtr expnumber = IntPtr.Zero;
			if (numerator) expnumber = this.Numerator.number;
			else expnumber = this.Denominator.number;

			try 
			{
#if x86_64
				ulong countp;
#else
				uint countp;
#endif

				IntMP.__gmpz_export(array, out countp, 1, 1, 1, 0, expnumber);
				result = new byte[countp + 1];
				array = Marshal.AllocHGlobal((int)(countp));
				IntMP.__gmpz_export(array, out countp, 1, 1, 1, 0, expnumber);
				Marshal.Copy(array, result, 1, (int) countp);
				if ( IntMP.__gmpz_sign(expnumber) < 0 )result[0] = 1;
			}
			finally 
			{
				if (array != IntPtr.Zero) Marshal.FreeHGlobal(array);
			}

			return result;
		}

		private void Import(bool numerator, byte[] data) 
		{
			IntPtr array = IntPtr.Zero;
			IntPtr expnumber = IntPtr.Zero;
			if (numerator) expnumber = this.Numerator.number;
			else expnumber = this.Denominator.number;
			
			try 
			{
				array = Marshal.AllocHGlobal(data.Length - 1);
				Marshal.Copy(data, 1, array, data.Length - 1);
				IntMP.__gmpz_import(expnumber, (uint) (data.Length - 1), 1, 1, 1, 0, array);
				if (data[0] == 1) IntMP.__gmpz_neg(expnumber, expnumber);
			}
			finally 
			{
				if (array != IntPtr.Zero) Marshal.FreeHGlobal(array);
			}		
		}

		
		#endregion Instance Methods

		#region Operators Overloads

		public static RatnlMP operator +(RatnlMP operand1, RatnlMP operand2)
		{
			RatnlMP result = new RatnlMP();
			__gmpq_add(result.number, operand1.number, operand2.number);
			return result;
		}

		public static RatnlMP operator -(RatnlMP operand1, RatnlMP operand2)
		{
			RatnlMP result = new RatnlMP();
			__gmpq_sub(result.number, operand1.number, operand2.number);
			return result;
		}

		public static RatnlMP operator -(RatnlMP operand) 
		{
			__gmpq_neg(operand.number, operand.number);
			return operand;
		}

		public static RatnlMP operator *(RatnlMP operand1, RatnlMP operand2)
		{
			RatnlMP result = new RatnlMP();
			__gmpq_mul(result.number, operand1.number, operand2.number);
			return result;
		}

		public static RatnlMP operator /(RatnlMP operand1, RatnlMP operand2)
		{
			RatnlMP result = new RatnlMP();
			__gmpq_div(result.number, operand1.number, operand2.number);
			return result;
		}

		public static bool operator <(RatnlMP operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp(operand1.number, operand2.number) < 0) return true;
			else return false;
		}

		public static bool operator <(RatnlMP operand1, int operand2)
		{
			if ( __gmpq_cmp_si(operand1.number, operand2, 1) < 0) return true;
			else return false;
		}

		public static bool operator <(int operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_si(operand2.number, operand1, 1) > 0) return true;
			else return false;
		}

		public static bool operator <(RatnlMP operand1, uint operand2)
		{
			if ( __gmpq_cmp_ui(operand1.number, operand2, 1) < 0) return true;
			else return false;
		}

		public static bool operator <(uint operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_ui(operand2.number, operand1, 1) > 0) return true;
			else return false;
		}

		public static bool operator >(RatnlMP operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp(operand1.number, operand2.number) > 0) return true;
			else return false;
		}

		public static bool operator >(RatnlMP operand1, int operand2)
		{
			if ( __gmpq_cmp_si(operand1.number, operand2, 1) > 0) return true;
			else return false;
		}

		public static bool operator >(int operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_si(operand2.number, operand1, 1) < 0) return true;
			else return false;
		}

		public static bool operator >(RatnlMP operand1, uint operand2)
		{
			if ( __gmpq_cmp_ui(operand1.number, operand2, 1) > 0) return true;
			else return false;
		}

		public static bool operator >(uint operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_ui(operand2.number, operand1, 1) < 0) return true;
			else return false;
		}

		public static bool operator <=(RatnlMP operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp(operand1.number, operand2.number) <= 0) return true;
			else return false;
		}

		public static bool operator <=(RatnlMP operand1, int operand2)
		{
			if ( __gmpq_cmp_si(operand1.number, operand2, 1) <= 0) return true;
			else return false;
		}

		public static bool operator <=(int operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_si(operand2.number, operand1, 1) >= 0) return true;
			else return false;
		}

		public static bool operator <=(RatnlMP operand1, uint operand2)
		{
			if ( __gmpq_cmp_ui(operand1.number, operand2, 1) <= 0) return true;
			else return false;
		}

		public static bool operator <=(uint operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_ui(operand2.number, operand1, 1) >= 0) return true;
			else return false;
		}

		public static bool operator >=(RatnlMP operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp(operand1.number, operand2.number) >= 0) return true;
			else return false;
		}

		public static bool operator >=(RatnlMP operand1, int operand2)
		{
			if ( __gmpq_cmp_si(operand1.number, operand2, 1) >= 0) return true;
			else return false;
		}

		public static bool operator >=(int operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_si(operand2.number, operand1, 1) <= 0) return true;
			else return false;
		}

		public static bool operator >=(RatnlMP operand1, uint operand2)
		{
			if ( __gmpq_cmp_ui(operand1.number, operand2, 1) >= 0) return true;
			else return false;
		}

		public static bool operator >=(uint operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_ui(operand2.number, operand1, 1) <= 0) return true;
			else return false;
		}

		public static bool operator ==(RatnlMP operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp(operand1.number, operand2.number) == 0) return true;
			else return false;
		}

		public static bool operator ==(RatnlMP operand1, int operand2)
		{
			if ( __gmpq_cmp_si(operand1.number, operand2, 1) == 0) return true;
			else return false;
		}

		public static bool operator ==(int operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_si(operand2.number, operand1, 1) == 0) return true;
			else return false;
		}

		public static bool operator ==(RatnlMP operand1, uint operand2)
		{
			if ( __gmpq_cmp_ui(operand1.number, operand2, 1) == 0) return true;
			else return false;
		}

		public static bool operator ==(uint operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_ui(operand2.number, operand1, 1) == 0) return true;
			else return false;
		}

		public static bool operator !=(RatnlMP operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator !=(RatnlMP operand1, int operand2)
		{
			if ( __gmpq_cmp_si(operand1.number, operand2, 1) != 0) return true;
			else return false;
		}

		public static bool operator !=(int operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_si(operand2.number, operand1, 1) != 0) return true;
			else return false;
		}

		public static bool operator !=(RatnlMP operand1, uint operand2)
		{
			if ( __gmpq_cmp_ui(operand1.number, operand2, 1) != 0) return true;
			else return false;
		}

		public static bool operator !=(uint operand1, RatnlMP operand2)
		{
			if ( __gmpq_cmp_ui(operand2.number, operand1, 1) != 0) return true;
			else return false;
		}


		public static explicit operator RatnlMP(byte operand) 
		{
			return new RatnlMP((uint)operand, 1u, false);
		}

		public static explicit operator RatnlMP(sbyte operand) 
		{
			return new RatnlMP((int)operand, 1, false);
		}

		public static explicit operator RatnlMP(char operand) 
		{
			return new RatnlMP((uint)operand, 1u, false);
		}

		public static explicit operator RatnlMP(short operand) 
		{
			return new RatnlMP((int)operand, 1, false);
		}

		public static explicit operator RatnlMP(ushort operand) 
		{
			return new RatnlMP((uint) operand, 1u, false);
		}

		public static explicit operator RatnlMP(uint operand) 
		{
			return new RatnlMP(operand, 1u, false);
		}

		public static explicit operator RatnlMP(int operand) 
		{
			return new RatnlMP(operand, 1, false);
		}

		public static explicit operator RatnlMP(float operand) 
		{
			return new RatnlMP((double)operand);
		}

		public static explicit operator RatnlMP(double operand) 
		{
			return new RatnlMP(operand);
		}

		public static explicit operator RatnlMP(long operand) 
		{
			// TODO : provide final implementation
			return new RatnlMP(new IntMP(operand));
		}

		public static explicit operator RatnlMP(ulong operand) 
		{
			// TODO : provide final implementation
			return new RatnlMP(new IntMP(operand));
		}

		public static explicit operator RatnlMP(decimal operand) 
		{
			// TODO : provide final implementation
			return new RatnlMP((IntMP) operand);
		}

		public static explicit operator RatnlMP(IntMP operand) 
		{
			return new RatnlMP(operand);
		}

		public static explicit operator RatnlMP(FloatMP operand) 
		{
			return new RatnlMP(operand);
		}


		public static explicit operator byte(RatnlMP operand)
		{
			return ConvertMP.ToByte(operand);
		}

		public static explicit operator sbyte(RatnlMP operand)
		{
			return ConvertMP.ToSByte(operand);
		}

		public static explicit operator char(RatnlMP operand)
		{
			return ConvertMP.ToChar(operand);
		}

		public static explicit operator short(RatnlMP operand)
		{
			return ConvertMP.ToInt16(operand);
		}

		public static explicit operator ushort(RatnlMP operand)
		{
			return ConvertMP.ToUInt16(operand);
		}

		public static explicit operator uint(RatnlMP operand)
		{
			return ConvertMP.ToUInt32(operand);
		}

		public static explicit operator int(RatnlMP operand)
		{
			return ConvertMP.ToInt32(operand);
		}

		public static explicit operator float(RatnlMP operand)
		{
			return ConvertMP.ToSingle(operand);
		}

		public static explicit operator double(RatnlMP operand)
		{
			return ConvertMP.ToDouble(operand);
		}


		#endregion Operators Overloads

	}

	#endregion Class RatnlMP

	#region Class mpq_t

//	[StructLayout(LayoutKind.Sequential)]
//	public class mpq_t
//	{
//		public mpz_t _mp_num;
//		public mpz_t _mp_den;
//
//		public mpq_t()
//		{
//			this._mp_num = new mpz_t();
//			this._mp_den = new mpz_t();
//		}
//	}

	// Flattenned class declaration avoids native heap memory errors which are caused by
	// above structre - unfortunately current access to Numerator and Denominator is slower
	// since it is implemented via __gmpq_get_xxx / __gmpq_set_xxx functions 
	// instead of direct member access - are there any better solutions?
	// Perhaps explicit layout and union of mpz_t with underlying fields?

	[StructLayout(LayoutKind.Sequential)]
	public class mpq_t 
	{
		// NUMERATOR

		// Explicit layout makes mpz_t processor dependant
		// in original mpz_t struct all fields will vary with word size
		/*[FieldOffset(0)]*/ public int _mp_num_mp_alloc;
		/*[FieldOffset(4)]*/ public int _mp_num_mp_size;
		/*[FieldOffset(8)]*/ public System.IntPtr _mp_num_mp_d;

		// DENOMINATOR

		// Explicit layout makes mpz_t processor dependant
		// in original mpz_t struct all fields will vary with word size
		/*[FieldOffset(0)]*/ public int _mp_den_mp_alloc;
		/*[FieldOffset(4)]*/ public int _mp_den_mp_size;
		/*[FieldOffset(8)]*/ public System.IntPtr _mp_den_mp_d;
	}

	#endregion Class mpq_t

}