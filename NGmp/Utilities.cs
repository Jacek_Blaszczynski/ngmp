/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;
using System.Threading;

namespace NGmp.Math
{

	#region Class Utilities

	/// <summary>
	/// Calss Utilities contains several useful P/Invoke methods.
	/// </summary>
	[SuppressUnmanagedCodeSecurity]
	public class Utilities
	{

		#region Fields

		private static int nBitsPerLimb;
		private static String nGmpVersion;
		private static Version nGmpGlueVersion = new Version();

		#region Multiprecision General Library DllImport target

#if MPIR
		public const string mp_library = "mpir";
#elif GMP
		public const string mp_library = "gmp";
#else
#error
#endif

		#endregion Multiprecision Library DllImport target

		#region Multiprecision Floating Point Library DllImport target

		public const string mpf_library = "mpfr";

		#endregion Multiprecision Floating Point Library DllImport target

		#region C Runtime Library DllImport target

#if MSVCR
#if DEBUG
		public const string c_library = "msvcr100d";
#else
		public const string c_library = "msvcr100";
#endif
#elif GCC
		private const string c_library = "glibc";
#else
		// just in case
#error
#endif

		#endregion C Runtime Library DllImport target

		#region Glue Library DllImport target

		public const string glue_library = "gmpglue";

		#endregion Glue Library DllImport target

		#endregion Fields

		#region Constructors

		public Utilities()
		{
		}

		static Utilities() 
		{
			IntPtr glueVersion = get_gmpglue_version();
			string glueVer = Marshal.PtrToStringAnsi(glueVersion);
			nGmpGlueVersion = new Version(glueVer);

			nBitsPerLimb = __gmp_bits_per_limb_g();
			// Additional check for ABI compatibility
#if x86_64
			if (nBitsPerLimb != 64)
				throw new ApplicationException(
					String.Format("Unsupported ABI with limb size {0}. Expected size for this build is 64 bits per limb.", nBitsPerLimb));
#else
			if (nBitsPerLimb != 32)
				throw new ApplicationException(
					String.Format("Unsupported ABI with limb size {0}. Expected size for this build is 32 bits per limb.", nBitsPerLimb));
#endif

			IntPtr ver = __gmp_version_g();
			nGmpVersion = Marshal.PtrToStringAnsi(ver);
		}


		#endregion Constructors

		#region Properties

		public static int BitsPerLimb
		{
			get {return nBitsPerLimb;}
		}

		public static String GmpVersion 
		{
			get {return nGmpVersion;}
		}

		public static Version GmpGlueVersion
		{
			get {return nGmpGlueVersion;}
		}

		#endregion Properties

		#region P/Invoke Prototypes

		#region C Run-Time Library

		// int fclose( FILE *stream );
		[DllImport(Utilities.c_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.I4)]
		public static extern int fclose( [In, MarshalAs(UnmanagedType.SysInt)] IntPtr stream );

		// int fflush( FILE *stream );
		[DllImport(Utilities.c_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.I4)]
		public static extern int fflush( IntPtr stream );


		// FILE *fopen( const char *filename, const char *mode );
		[DllImport(Utilities.c_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.SysInt)]
		public static extern IntPtr fopen( 
			[In, MarshalAs(UnmanagedType.LPStr)] String filename, 
			[In, MarshalAs(UnmanagedType.LPStr)] String mode );


		// int fputc( int c, FILE *stream );
		[DllImport(Utilities.c_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.I4)]
		public static extern int fputc( int c, System.IntPtr stream );


		// FILE *freopen( const char *path, const char *mode, FILE *stream );
		[DllImport(Utilities.c_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.SysInt)]
		public static extern IntPtr freopen( 			
			[In, MarshalAs(UnmanagedType.LPStr)] String filename, 
			[In, MarshalAs(UnmanagedType.LPStr)] String mode, 
			[In, MarshalAs(UnmanagedType.SysInt)] IntPtr stream );

		#endregion C Run-Time Library

		#region GMP P/Invoke Low Level Prototypes

		/// <summary>
		/// Convert {s1p, s1n} to a raw unsigned char array at str in base base, 
		/// and return the number of characters produced. There may be leading zeros 
		/// in the string. The string is not in ASCII; to convert it to printable format, 
		/// add the ASCII codes for 0 or A, depending on the base and range. 
		/// base can vary from 2 to 256. The most significant limb of the input {s1p, s1n} 
		/// must be non-zero. The input {s1p, s1n} is clobbered, 
		/// except when base is a power of 2, in which case it's unchanged. The area at str 
		/// has to have space for the largest possible number represented by 
		/// a s1n long limb array, plus one extra character.
		/// </summary>
		/// <param name="str"></param>
		/// <param name="radix"></param>
		/// <param name="s1p"></param>
		/// <param name="s1n"></param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpn_get_str (IntPtr str, int radix, IntPtr s1p, int s1n);
		/// <summary>
		/// Convert bytes {str,strsize} in the given base to limbs at rp. 
		/// str[0] is the most significant byte and str[strsize-1] is 
		/// the least significant. Each byte should be a value in the range 0 to base-1, 
		/// not an ASCII character. base can vary from 2 to 256. The return value 
		/// is the number of limbs written to rp. If the most significant input byte 
		/// is non-zero then the high limb at rp will be non-zero, and only that 
		/// exact number of limbs will be required there. 																																																	  If the most significant input byte is zero then there may be high zero limbs written to rp and included in the return value. strsize must be at least 1, and no overlap is permitted between {str,strsize} and the result at rp.
		/// </summary>
		/// <param name="rp"></param>
		/// <param name="str"></param>
		/// <param name="strsize"></param>
		/// <param name="radix"></param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpn_set_str (IntPtr rp, IntPtr str, uint strsize, int radix);

		#endregion GMP P/Invoke Low Level Prototypes

		#region gmpglue

		// Mono bug on *nix systems - wrong handling of system specific prefixes and suffixes
		// We love Mono - don't we? Because of that we do some more work here.

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		public static extern int __gmp_bits_per_limb_g();


		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		[return: MarshalAs(UnmanagedType.SysInt)]
		public static extern IntPtr __gmp_version_g();


		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		public static extern IntPtr get_gmpglue_version();


		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl, EntryPoint="get_processor_ticks")]
		[return: MarshalAs(UnmanagedType.U4)]
		public static extern uint GetProcessorTicksLow();

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl, EntryPoint="get_processor_ticks_p")]
		[return: MarshalAs(UnmanagedType.U8)]
		public static extern ulong GetProcessorTicks();


		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		[return: MarshalAs(UnmanagedType.U8)]
		private static extern ulong get_processor_freq(uint divider);

		/// <summary>
		/// Measures current processor frequency. It could be read from Win registry
		/// or cat proc/cpuinfo  file on *nix'es but for those who want to measure it here 
		/// it is - the way to do it.
		/// </summary>
		/// <param name="miliseconds">Number of milliseconds for measuring frequency.
		/// Usually 10 ms should be sufficient, however, highest precision could be
		/// achieved while running it for around 1000 ms. 
		/// When 0 is passed measurement is done for 10 ms </param>
		/// <returns>Current processor frequency in Hz.</returns>
		public static ulong GetProcessorFrequency(uint miliseconds) 
		{
			if (miliseconds == 0) miliseconds = 10;

			ulong result = 0;

			// Mono does not implement currently any of the Properties used to set 
			// priority. Using them does not throw exceptions but has no effect.
			// gmpglue implements priority changes for *nix systems.
			ProcessPriorityClass processPriority = Process.GetCurrentProcess().PriorityClass;
			ThreadPriority threadPriority = Thread.CurrentThread.Priority;

			try 
			{

				Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime;
				Thread.CurrentThread.Priority = ThreadPriority.Highest;

				result = get_processor_freq(miliseconds);
			}
			finally
			{
				Thread.CurrentThread.Priority = threadPriority;
				Process.GetCurrentProcess().PriorityClass = processPriority;
			}

			return result;

		}


		#endregion gmpglue

		#endregion P/Invoke Prototypes

	}

	#endregion Class Utilities

}
