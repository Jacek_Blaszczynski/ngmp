/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;

namespace NGmp.Math 
{
	#region Class FloatMP 

	/// <summary>
	/// Class FloatMP provides arbitrary precision rational arithmetic over 
	/// FloatMP numbers. Class provides CLI framework bindings
	/// for GNU MP library (GMP) float numbers: http://www.swox.com/gmp/.
	/// Supported GNU MP version = 4.1.3 - 4.1.4.
	/// Note that the GNU MP floating point functions are not intended as 
	/// a smooth extension to IEEE P754 arithmetic. In particular results 
	/// obtained on one computer often differ from the results on 
	/// a computer with a different word size.
	/// </summary>
	[Serializable]
	[SuppressUnmanagedCodeSecurity]
	public sealed class FloatMP : 
		IDisposable, 
		IComparable, 
		ICloneable, 
		ISerializable,
		IConvertible,				// Duplicate due to IConvertibleMP
		IConvertibleMP
#if NET_2_0
		,
		IComparable<FloatMP>,
		IEquatable<FloatMP>
#endif
	{
		
		#region Fields

		private static int mMpfCoreNativeSize = Marshal.SizeOf(typeof(mpf_t));

		internal IntPtr number;
		private bool rawPrecision = false;
		private uint origPrecision;
		private bool disposed = false;

		private uint nPrecision;
		private bool precisionSet = false;
		private static uint nDefaultPrecision;
		private static bool defaultPrecisionSet;

		#endregion Fields

		#region Constructors & Dispose Methods

		#region Constructors

		public FloatMP()
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			__gmpf_init(this.number);
		}

		/// <summary>
		/// Initializes new instance of FloatMP and sets either default or instance
		/// precision to prec bits depending on value of defPrec parameter. In every
		/// scenario this instance will be initialized with prec precision. If defPrec 
		/// is set to true this and all subsequent instances of FloatMP will be initialized
		/// with prec precision.
		/// </summary>
		/// <param name="prec"></param>
		/// <param name="defPrec"></param>
		public FloatMP(uint prec, bool defPrec)
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			if (defPrec) 
			{
				__gmpf_set_default_prec(prec);
				nDefaultPrecision = prec;
				defaultPrecisionSet= true;
				__gmpf_init(this.number);
			}
			else 
			{
				__gmpf_init2(this.number, prec);
				nPrecision = prec;
				precisionSet = true;
			}
		}

		/// <summary>
		/// Initializes new instance of FloaGMP class using value from str.
		/// Constructor assumes that str stores value in base 10 and exponent is decimal.
		/// </summary>
		/// <param name="str">A string containing a number to assign to this instance.</param>
		/// <param name="radix">The argument base may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal.</param>
		public FloatMP(String str, int radix) 
		{
			if ((radix >= -36 && radix <= -2) || (radix >= 2 && radix <= 36)) 
			{
				this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
				__gmpf_init_set_str(this.number, str, radix);
			}
			else throw new ArgumentOutOfRangeException("radix", radix, 
						"Argument was out of expected range. Values should be between" +
						"-36 to -2 or 2 to 36 inclusive.");
		}

		/// <summary>
		/// Initializes new instance of FloaGMP class using value from str.
		/// Constructor assumes that str stores value in base 10 and exponent is decimal.
		/// </summary>
		/// <param name="str">A string containing a number to assign to this instance.</param>
		public FloatMP(String str) 
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			__gmpf_init_set_str(this.number, str, -10);
		}

		public FloatMP(FloatMP operand) 
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			__gmpf_init_set(this.number, operand.number);
		}

		public FloatMP(RatnlMP operand) : this()
		{
			__gmpf_set_q(this.number, operand.number);
		}

		public FloatMP(IntMP operand) : this() 
		{
			__gmpf_set_z(this.number, operand.number);
		}

		public FloatMP(double operand) 
		{
			if (!Double.IsNaN(operand) && operand != double.NegativeInfinity && operand != double.PositiveInfinity) 
			{
				this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
				__gmpf_init_set_d(this.number, operand);
			}
			else throw new ArgumentOutOfRangeException("operand", operand, 
					 "Argument out of accepted range. FloatMP does not accept NaN or Infinity values.");
		}

		public FloatMP(uint operand) 
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			__gmpf_init_set_ui(this.number, operand);
		}

		public FloatMP(int operand) 
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			__gmpf_init_set_si(this.number, operand);
		}

		public FloatMP(SerializationInfo info, StreamingContext context)
		{
			this.number = Marshal.AllocHGlobal(mMpfCoreNativeSize);
			__gmpf_init_set_str(this.number, info.GetString("value"), -10);
		}

		#endregion Constructors

		#region IDisposable Methods
		// Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!this.disposed)
			{
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if(disposing)
				{
					// Dispose managed resources.
				}
             
				// Call the appropriate methods to clean up 
				// unmanaged resources here.
				// If disposing is false, 
				// only the following code is executed.
				if (this.rawPrecision) 
				{
					__gmpf_set_prec_raw(this.number, this.origPrecision);
					this.rawPrecision = false;
				}
				if (this.number != IntPtr.Zero) 
				{
					__gmpf_clear(this.number);
					this.number = IntPtr.Zero;       
				}
			}
			disposed = true;         
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~FloatMP()      
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}

		#endregion IDisposable Methods

		#endregion Constructors & Dispose Methods

		#region Properties

		public bool IsDisposed
		{
			get {return this.disposed;}
		}

		public IntPtr MPNumber
		{
			get {return this.number;}
			set {this.number = value;}
		}

		public bool IsRawPrecision
		{
			get {return this.rawPrecision;}
		}

		public uint Precision
		{
			get 
			{ 
				if (precisionSet) 
				{
					return this.nPrecision;

				}
				else 
				{
					this.nPrecision = __gmpf_get_prec(this.number);
					precisionSet = true;
					return this.nPrecision;
				}
			}
			set 
			{ 
				if (this.rawPrecision) 
				{
					__gmpf_set_prec_raw(this.number, this.origPrecision);
					this.rawPrecision = false;
				}
				this.nPrecision = value;
				__gmpf_set_prec(this.number, value);
			}
		}

		public static uint DefaultPrecision
		{
			get 
			{ 
				if (defaultPrecisionSet) 
				{
					return nDefaultPrecision;
				}
				else 
				{
					nDefaultPrecision = __gmpf_get_default_prec();
					defaultPrecisionSet = true;
					return nDefaultPrecision;
				}
			}
			set 
			{
				__gmpf_set_default_prec(value);
				nDefaultPrecision = value;
				defaultPrecisionSet = true;
			}
		}

		#endregion Properties

		#region GMP P/Invoke Prototypes

		#region Initialization & Memory Mngmnt

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr float_n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init (
			[In, Out, MarshalAs(UnmanagedType.LPStruct)] mpf_t float_n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init2 (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr float_n, 
			uint precision);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_clear (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr float_n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_default_prec (uint prec);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpf_get_default_prec ();

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpf_get_prec (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_prec (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr n, 
			uint prec);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_prec_raw (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint prec);

		#endregion Initializtaion & Memory Mngnmnt

		#region Assignment

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_si (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_d (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_z (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_set_q (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="str"></param>
		/// <param name="radix">The argument radix may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal. </param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern int __gmpf_set_str (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.LPStr)] String str, 
			int radix);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_swap (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rop1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr rop2);

		#endregion Assignment

		#region Initialization & Assignment

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init_set (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init_set_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init_set_si (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_init_set_d (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.R8)] double operand);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="str"></param>
		/// <param name="radix">The argument radix may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal. </param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern int __gmpf_init_set_str (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.LPStr)] string str, 
			int radix);

		#endregion Initialization & Assignment

		#region Conversion

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double __gmpf_get_d (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern double __gmpf_get_d_2exp (
			int exp, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		// return type long int
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_get_si (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		// return type unsigned long int
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern uint __gmpf_get_ui (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Convert operand to a string of digits in radix base. 
		/// radix can be 2 to 36. Up to n_digits digits will be generated. 
		/// Trailing zeros are not returned. No more digits than can be accurately 
		/// represented by operand are ever generated. If n_digits is 0 then that 
		/// accurate maximum number of digits are generated. 
		/// </summary>
		/// <param name="str"></param>
		/// <param name="expptr"></param>
		/// <param name="radix"></param>
		/// <param name="n_digits"></param>
		/// <param name="operand"></param>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern void __gmpf_get_str (
			[Out, MarshalAs(UnmanagedType.LPStr)] out StringBuilder str, 
			ref int expptr,
			int radix, 
			uint n_digits, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern void __gmpf_get_str (
			[Out, MarshalAs(UnmanagedType.LPStr)] StringBuilder str, 
			ref int expptr,
			int radix, 
			uint n_digits, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		#endregion Conversion

		#region Arithmetic

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_add (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_add_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_sub (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_sub_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_ui_sub (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_mul (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_mul_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_mul_2exp (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_neg (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_abs (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_div (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_ui_div (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_div_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_div_2exp (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_pow_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_sqrt (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_sqrt_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint operand);

		#endregion Arithmetic

		#region Comparison

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_cmp (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_cmp_d (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			double operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_cmp_ui (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			uint operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_cmp_si (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			int operand2);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_sgn (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_eq (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2, 
			uint operand3);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_reldiff (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand1, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand2);

		[DllImport(Utilities.glue_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_sign(
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		#endregion Comparison

		#region IO

		/// <summary>
		/// Print operand to stream, as a string of digits. Return the number of bytes written, 
		/// or if an error occurred, return 0. The mantissa is prefixed with an 0. 
		/// and is in the given base, which may vary from 2 to 36. An exponent then printed, 
		/// separated by an e, or if base is greater than 10 then by an @. 
		/// The exponent is always in decimal. The decimal point follows the current locale, 
		/// on systems providing localeconv. Up to n_digits will be printed from the mantissa, 
		/// except that no more digits than are accurately representable by operand will be printed. 
		/// n_digits can be 0 to select that accurate maximum. 
		/// </summary>
		/// <param name="stream"></param>
		/// <param name="radix"></param>
		/// <param name="n_digits"></param>
		/// <param name="operand"></param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern uint __gmpf_out_str (
			IntPtr stream, 
			int radix, 
			uint n_digits, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Read a string in base base from stream, and put the read float in result. 
		/// The string is of the form M@N or, if the base is 10 or less, alternatively MeN. 
		/// M is the mantissa and N is the exponent. The mantissa is always in the specified base. 
		/// The exponent is either in the specified base or, if base is negative, in decimal. 
		/// The decimal point expected is taken from the current locale, on systems 
		/// providing localeconv. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="stream"></param>
		/// <param name="radix">The argument radix may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal.</param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Ansi)]
		internal static extern uint __gmpf_inp_str (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			IntPtr stream, 
			int radix);

		#endregion IO

		#region Miscallenous

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_fits_ulong_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_fits_slong_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_fits_uint_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_fits_sint_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_fits_ushort_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_fits_sshort_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_ceil (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_floor (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_trunc (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		/// <summary>
		/// Return non-zero if operand is an integer.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmpf_integer_p (
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr operand);

		#endregion Miscallenous

		#endregion GMP P/Invoke Prototypes

		#region Object Overrides

		public override bool Equals(object obj)
		{
			if (obj == null) return false;
			if (obj is FloatMP) 
			{
				return __gmpf_cmp(this.number,((FloatMP)obj).number) == 0;
			}
			else throw new ArgumentException(@"Equals argument obj has wrong Type. FloatMP expected", "obj");
		}

		public override int GetHashCode()
		{
			mpf_t value = (mpf_t)Marshal.PtrToStructure(this.number, typeof(mpf_t));
			int hash = IntMP.GetHashCode(value._mp_size, value._mp_d);
			hash ^= value._mp_exp;
			return hash;
		}

		public override string ToString()
		{
			return this.ToString(10, 0);
		}

		public string ToString(uint numberDigits) 
		{
			return this.ToString(10, numberDigits);
		}

		public string ToString(int radix) 
		{
			return this.ToString(radix, 0);
		}

		public string ToString(int radix, uint numberDigits) 
		{
			if (radix >= 2 && radix <= 36) 
			{
				StringBuilder str = null;
				if (numberDigits != 0) str = new StringBuilder((int)numberDigits+2); // new StringBuilder((int) this.Precision + 16);
				else str = new StringBuilder(1024);
				int expptr = 0;
				__gmpf_get_str(str, ref expptr, radix, numberDigits, this.number);
				if (str != null && str.Length != 0)
				{
					int absexpptr = System.Math.Abs(expptr);
					int x = 0;
					if (str[0] == '-') x = 1;
					//				else x = 1;
					if ((absexpptr+x) < str.Length) str = str.Insert(absexpptr + x, '.');
					else if ((absexpptr+x) > str.Length) 
					{
						string exponent = "";
						if (radix > 10 ) exponent = "@";
						else exponent = "e";
						if (x == 1) str.Insert(1, "0.");
						else str.Insert(0, "0.");
						str.Append(exponent + expptr);
					}
					return str.ToString();
				}
				else return null;
			}
			else throw new ArgumentOutOfRangeException("radix", radix, 
					"Argument out of accepted ranges. Values from 2 to 36 inclusive expected.");
		}

		#endregion Object Overrides

		#region IComparable Members

		public int CompareTo(object obj)
		{
			if (obj is FloatMP) return __gmpf_cmp(this.number, ((FloatMP)obj).number);
			else if (obj is uint) return __gmpf_cmp_ui(this.number, (uint) obj);
			else if (obj is int) return __gmpf_cmp_si(this.number, (int) obj);
			else if (obj is double) return __gmpf_cmp_d(this.number, (double) obj);
			else throw new ArgumentException(@"CompareTo argument obj has wrong Type. FloatMP, int, uint, double expected", "obj");
		}

		#endregion IComparable Members

#if NET_2_0
		#region IComparable<> Members

		public int CompareTo(FloatMP value)
		{
			return __gmpf_cmp(this.number, value.number);
		}

		#endregion IComparable<> Members
#endif

		#region IConvertible Members

		ulong System.IConvertible.ToUInt64(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		sbyte System.IConvertible.ToSByte(IFormatProvider provider)
		{
			return ConvertMP.ToSByte(this);
		}

		double System.IConvertible.ToDouble(IFormatProvider provider)
		{
			return ConvertMP.ToDouble(this);
		}

		DateTime System.IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		float System.IConvertible.ToSingle(IFormatProvider provider)
		{
			return ConvertMP.ToSingle(this);
		}

		bool System.IConvertible.ToBoolean(IFormatProvider provider)
		{
			return ConvertMP.ToBoolean(this);
		}

		int System.IConvertible.ToInt32(IFormatProvider provider)
		{
			return ConvertMP.ToInt32(this);
		}

		ushort System.IConvertible.ToUInt16(IFormatProvider provider)
		{
			return ConvertMP.ToUInt16(this);
		}

		short System.IConvertible.ToInt16(IFormatProvider provider)
		{
			return ConvertMP.ToInt16(this);
		}

		public string ToString(IFormatProvider provider)
		{
			return ConvertMP.ToString(this, provider);
		}

		byte System.IConvertible.ToByte(IFormatProvider provider)
		{
			return ConvertMP.ToByte(this);
		}

		char System.IConvertible.ToChar(IFormatProvider provider)
		{
			return ConvertMP.ToChar(this);
		}

		long System.IConvertible.ToInt64(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public System.TypeCode GetTypeCode()
		{
			return (System.TypeCode) 21;
		}

		decimal System.IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException ("This conversion is not supported.");
		}

		public object ToType(Type conversionType, IFormatProvider provider)
		{
			return this.GetType();
		}

		uint System.IConvertible.ToUInt32(IFormatProvider provider)
		{
			return ConvertMP.ToUInt32(this);
		}

		#endregion

		#region IConvertibleMP Members

		public IntMP ToIntMP(IFormatProvider provider)
		{
			// TODO:  Add FloatMP.ToIntMP implementation
			return null;
		}

		public NGmp.Math.FloatMP ToFloatMP(IFormatProvider provider)
		{
			return this;
		}

		public FloatMpfr ToFloatMpfr(IFormatProvider provider)
		{
			// TODO:  Add FloatMP.ToFloatMpfr implementation
			return null;
		}

		public RatnlMP ToRatnlMP(IFormatProvider provider)
		{
			// TODO:  Add FloatMP.ToRatnlMP implementation
			return null;
		}

		#endregion

		#region ICloneable Members

		public object Clone()
		{
			return new FloatMP(this);
		}

		#endregion

#if NET_2_0
		#region IEquatable<> Members

		public bool Equals(FloatMP value)
		{
			return __gmpf_cmp(this.number, value.number) == 0;
		}

		#endregion IEquatable<> Members
#endif

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("value", this.ToString());
		}

		private static byte[] GetBinaryData(FloatMP operand) 
		{
			return null;
		}

		private static void SetBinaryData(FloatMP result, byte[] data) 
		{

		}

		#endregion

		#region Static Methods

		#region Initialization

		/// <summary>
		/// Set the default precision to be at least prec bits. 
		/// All subsequently created FloatMP objects will use this precision, 
		/// but previously initialized variables are unaffected.
		/// </summary>
		/// <param name="prec"></param>
		public static void SetDefaultPrecision(uint prec) 
		{
			__gmpf_set_default_prec(prec);
			nDefaultPrecision = prec;
			defaultPrecisionSet = true;
		}

		/// <summary>
		/// Return the default precision used currently by library.
		/// </summary>
		/// <returns>Precision in bits.</returns>
		public static uint GetDefaultPrecision() 
		{
			nDefaultPrecision = __gmpf_get_default_prec();
			defaultPrecisionSet = true;
			return nDefaultPrecision;
		}

		/// <summary>
		/// Set the precision of result to be at least prec bits, without changing the memory allocated.
		/// SetPrecisionRaw is an efficient way to use an FloatMP variable at different precisions 
		/// during a calculation, perhaps to gradually increase precision in an iteration, 
		/// or just to use various different precisions for different purposes during a calculation.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="prec"></param>
		public static void SetPrecisionRaw(FloatMP result, uint prec) 
		{
			result.origPrecision = result.Precision;
			result.rawPrecision = true;
			__gmpf_set_prec_raw(result.number, prec);
		}

		#endregion Initialization

		#region Assignment

		public static void Set(FloatMP result, FloatMP operand) 
		{
			__gmpf_set(result.number, operand.number);
		}

		public static void Set(FloatMP result, uint operand) 
		{
			__gmpf_set_ui(result.number, operand);
		}

		public static void Set(FloatMP result, int operand) 
		{
			__gmpf_set_si(result.number, operand);
		}

		public static void Set(FloatMP result, double operand) 
		{
			__gmpf_set_d(result.number, operand);
		}

		public static void Set(FloatMP result, IntMP operand) 
		{
			__gmpf_set_z(result.number, operand.number);
		}

		public static void Set(FloatMP result, RatnlMP operand) 
		{
			__gmpf_set_q(result.number, operand.number);
		}

		/// <summary>
		/// Assigns the string representation of a number to 
		/// its FloatMP result floating point number equivalent.
		/// </summary>
		/// <param name="result">A FloatMP floating point number equivalent to the numeric value specified in str.</param>
		/// <param name="str">A string containing a number to assign.</param>
		/// <param name="radix">The argument base may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal.</param>
		public static void Set(FloatMP result, String str, int radix) 
		{
			if (str == null) throw new ArgumentNullException("str", "Argument cannot be null.");
			if (radix < -36 || radix > 36 || (radix > -2 && radix < 2))
				throw new ArgumentOutOfRangeException("radix", radix, "Argument radix is out of accepted range.");
			__gmpf_set_str(result.number, str, radix);
		}

		public static void Swap (FloatMP rop1, FloatMP rop2) 
		{
			__gmpf_swap(rop1.number, rop2.number);
		}


		#endregion Assignment

		#region Conversion

		/// <summary>
		/// Converts the string representation of a number in 
		/// a specified style and culture-specific format to its FloatMP equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <param name="style">The combination of one or more 
		/// System.Globalization.NumberStyles constants that indicates the permitted format of s.</param>
		/// <param name="provider"> An System.IFormatProvider that supplies culture-specific formatting information about s.</param>
		/// <returns>A FloatMP unlimited precision floating point equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static FloatMP Parse(String s, System.Globalization.NumberStyles style, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is yet not implemented.");
			//if (s == null) throw new System.ArgumentNullException("s", "Value of FloatMP underlying number cannot be null.");
			//FloatMP result = new FloatMP(s);
			//return result;
		}

		/// <summary>
		/// Converts the string representation of a number in 
		/// a specified culture-specific format to its FloatMP unlimited precision equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <param name="provider"> An System.IFormatProvider that supplies culture-specific formatting information about s.</param>
		/// <returns>A FloatMP unlimited precision equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static FloatMP Parse(String s, System.IFormatProvider provider) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return FloatMP.Parse(s, NumberStyles.Integer, provider);
		}

		/// <summary>
		/// Converts the string representation of a number in 
		/// a specified style to its FloatMP unlimited precision equivalent.
		/// </summary>
		/// <param name="s">A string containing a number to convert.</param>
		/// <param name="style">The combination of one or more 
		/// System.Globalization.NumberStyles constants that indicates the permitted format of s.</param>
		/// <returns>A FloatMP unlimited precision equivalent to the number specified in s.</returns>
		/// <exception cref="System.ArgumentNullException">s is null.</exception>
		public static FloatMP Parse(String s, NumberStyles style) 
		{
			throw new NotImplementedException("Method is not implemented yet.");
			//return FloatMP.Parse(s, style, null);
		}

		/// <summary>
		/// Converts the string representation of a number to 
		/// its FloatMP floating point number equivalent.
		/// </summary>
		/// <param name="str">A string containing a number to convert.</param>
		/// <param name="radix">The argument base may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal.</param>
		/// <returns>A FloatMP floating point number equivalent to the numeric value specified in str.</returns>
		/// <exception cref="ArgumentNullException">str is null.</exception>
		/// <exception cref="ArgumentOutOfRange">radix is out of accepted range.</exception>
		public static FloatMP Parse(String str, int radix) 
		{
			if (str == null) throw new ArgumentNullException("str", "Argument cannot be null.");
			if (radix < -36 || radix > 36 || (radix > -2 && radix < 2))
				throw new ArgumentOutOfRangeException("radix", radix, "Argument radix is out of accepted range.");
			return new FloatMP(str, radix);
		}

		/// <summary>
		/// Converts the string representation of a number to 
		/// its FloatMP floating point number equivalent assuming base 10 
		/// and exponent is decimal.
		/// </summary>
		/// <param name="str">A string containing a number to convert.</param>
		/// <returns>A FloatMP floating point number equivalent to the numeric value specified in str.</returns>
		/// <exception cref="ArgumentNullException">str is null.</exception>
		public static FloatMP Parse(String str) 
		{
			return new FloatMP(str, -10);
		}
			  
		public static double GetDouble (FloatMP operand) 
		{
			return __gmpf_get_d(operand.number);
		}

		/// <summary>
		/// Find d and exp such that d times 2 raised to exp, with  1 > abs(d) >= 0.5, 
		/// is a good approximation to operand. This is similar to the standard C function frexp.
		/// </summary>
		/// <param name="operand"></param>
		/// <param name="exp"></param>
		/// <returns></returns>
		public static double GetDouble(FloatMP operand, int exp) 
		{
			if (__gmpf_fits_slong_p(operand.number) == 0) throw new OverflowException("Value of operand cannot fit to Int32.");
			return __gmpf_get_d_2exp(exp, operand.number);
		}

		/// <summary>
		/// Convert operand to a Int32 , truncating any fraction part. 
		/// If operand is too big for the return type, function throws OverflowException.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		/// <exception cref="OverflowException">operand cannot fit to System.Int32.</exception>
		public static int GetInt32(FloatMP operand) 
		{
			if (__gmpf_fits_slong_p(operand.number) == 0) throw new OverflowException("Value of operand cannot fit to Int32.");
			return __gmpf_get_si(operand.number);
		}

		/// <summary>
		/// Convert operand to a UInt32 , truncating any fraction part. 
		/// If operand is too big for the return type, function throws OverflowException.
		/// </summary>
		/// <param name="operand"></param>
		/// <returns></returns>
		/// <exception cref="OverflowException">operand cannot fit to System.UInt32.</exception>
		public static uint GetUInt32(FloatMP operand) 
		{
			if (__gmpf_fits_ulong_p(operand.number) == 0) throw new OverflowException("Value of operand cannot fit to Int32.");
			return __gmpf_get_ui(operand.number);
		}

		#endregion Conversion

		#region Arithmetic

		public static void Add (FloatMP result, FloatMP operand1, FloatMP operand2) 
		{
			__gmpf_add(result.number, operand1.number, operand2.number);
		}

		public static void Add (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_add_ui(result.number, operand1.number, operand2);
		}

		public static void Sub (FloatMP result, FloatMP operand1, FloatMP operand2) 
		{
			__gmpf_sub(result.number, operand1.number, operand2.number);
		}

		public static void Sub (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_sub_ui(result.number, operand1.number, operand2);
		}

		public static void Sub (FloatMP result, uint operand1, FloatMP operand2) 
		{
			__gmpf_ui_sub(result.number, operand1, operand2.number);
		}

		public static void Mul (FloatMP result, FloatMP operand1, FloatMP operand2) 
		{
			__gmpf_mul(result.number, operand1.number, operand2.number);
		}

		public static void Mul (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_mul_ui(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to operand1 times 2 raised to operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Mul2Exp (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_mul_2exp(result.number, operand1.number, operand2);
		}

		/// <summary>
		/// Set result to -operand.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		public static void Neg (FloatMP result, FloatMP operand)
		{
			__gmpf_neg(result.number, operand.number);
		}

		public static void Abs(FloatMP result, FloatMP operand) 
		{
			__gmpf_abs(result.number, operand.number);
		}

		public static void Div (FloatMP result, FloatMP operand1, FloatMP operand2) 
		{
			__gmpf_div(result.number, operand1.number, operand2.number);
		}

		public static void Div (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_div_ui(result.number, operand1.number, operand2);
		}

		public static void Div (FloatMP result, uint operand1, FloatMP operand2) 
		{
			__gmpf_ui_div(result.number, operand1, operand2.number);
		}

		/// <summary>
		/// Set result to operand1 divided by 2 raised to operand2.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		public static void Div2Exp (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_div_2exp(result.number, operand1.number, operand2);
		}

		public static void Pow (FloatMP result, FloatMP operand1, uint operand2) 
		{
			__gmpf_pow_ui(result.number, operand1.number, operand2);
		}

		public static void Sqrt (FloatMP result, uint operand1) 
		{
			__gmpf_sqrt_ui(result.number, operand1);
		}

		public static void Sqrt (FloatMP result, FloatMP operand1) 
		{
			__gmpf_sqrt(result.number, operand1.number);
		}

		public static int Sign(FloatMP operand) 
		{
			return __gmpf_sign(operand.number);
		}
		
		#endregion Arithmetic

		#region Comparison

		/// <summary>
		/// Return non-zero if the first operand3 bits of operand1 and operand2 are equal, zero otherwise. 
		/// I.e., test of operand1 and operand2 are approximately equal.
		/// Caution: Currently only whole limbs are compared, and only in an exact fashion. 
		/// In the future values like 1000 and 0111 may be considered the same to 3 bits 
		/// (on the basis that their difference is that small). 
		/// </summary>
		/// <param name="operand1"></param>
		/// <param name="operand2"></param>
		/// <param name="operand3">Number of most significant bits to compare.</param>
		/// <returns></returns>
		public static bool Equal (FloatMP operand1, FloatMP operand2, uint operand3) 
		{
			return __gmpf_eq(operand1.number, operand2.number, operand3) != 0;
		}

		public static void RelativeDifference (FloatMP result, FloatMP operand1, FloatMP operand2) 
		{
			__gmpf_reldiff(result.number, operand1.number, operand2.number);
		}

		#endregion Comparison

		#region IO

		public static void Save (String fileName, FloatMP operand) 
		{
			Save (fileName, 10, 0, operand);
		}

		public static void Save (String fileName, int radix, FloatMP operand) 
		{
			Save (fileName, radix, 0, operand);
		}

		/// <summary>
		/// Save value of operand to text file. The mantissa is prefixed with an 0. and is 
		/// in the given base equal to radix, which may vary from 2 to 36.
		/// An exponent then printed, separated by an e, or if base is greater than 10 then by an @. 
		/// The exponent is always in decimal. The decimal point follows the current locale, 
		/// on systems providing localeconv. Up to numberDigits will be printed from the mantissa, 
		/// except that no more digits than are accurately representable by op will be printed. 
		/// numberDigits can be set to 0 to select accurate maximum.
		/// </summary>
		/// <param name="fileName">Fully qualified path to file.</param>
		/// <param name="radix"></param>
		/// <param name="numberDigits"></param>
		/// <param name="operand"></param>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="DirectoryNotFoundException"></exception>
		/// <exception cref="IOException"></exception>
		public static void Save (String fileName, int radix, uint numberDigits, FloatMP operand) 
		{
			if (fileName == null ) 
				throw new ArgumentNullException("fileName", 
									"Argument is null. Fully qualified path to file expected.");

			if (operand == null ) 
				throw new ArgumentNullException("operand", 
									"Argument is null. Expected valid FloatMP for saving.");

#if NET_2_0
			if (fileName.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
#else
			if (fileName.IndexOfAny(Path.InvalidPathChars) >= 0) 
#endif 
				throw new ArgumentException("Invalid characters in path: " + fileName, "fileName");

			if (!Directory.Exists(Path.GetDirectoryName(fileName))) 
				throw new DirectoryNotFoundException(
					"Specified directory does not exist: " + Path.GetDirectoryName(fileName));

			if (radix < 2 || radix > 36 ) 
				throw new ArgumentOutOfRangeException("radix", radix, 
					"Argument radix is out of range. Accepted values are from 2 to 36 inclusive.");

			IntPtr pf = IntPtr.Zero;
			try 
			{
				pf = Utilities.fopen(fileName, "wt");
				if (pf != IntPtr.Zero)
				{
					if (__gmpf_out_str(pf, radix, numberDigits, operand.number)==0)
						throw new IOException("Cannot write to file: " + fileName); 
				}
				else throw new System.IO.IOException("Could not create file: " + fileName);
			}
			finally 
			{
				if (pf != IntPtr.Zero) 
					if (Utilities.fclose(pf)!= 0) 
						throw new System.IO.IOException("Could not close file: " + fileName);
			}		
		}

		public static void Open (FloatMP result, String fileName) 
		{
			Open(result, fileName, -10);
		}

		/// <summary>
		/// Read a string in base radix from file fileName, and put the read float in result. 
		/// The string is of the form M@N or, if the base is 10 or less, alternatively MeN. 
		/// M is the mantissa and N is the exponent. The mantissa is always in the specified base. 
		/// The exponent is either in the specified base or, if base is negative, in decimal. 
		/// The decimal point expected is taken from the current locale, 
		/// on systems providing localeconv. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="fileName">Fully qulified path to file.</param>
		/// <param name="radix">The argument base may be in the ranges 2 to 36, or -36 to -2. 
		/// Negative values are used to specify that the exponent is in decimal.</param>
		/// <exception cref="ArgumentNullException"></exception>
		/// <exception cref="ArgumentException"></exception>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		/// <exception cref="FileNotFoundException"></exception>
		/// <exception cref="IOException"></exception>
		public static void Open (FloatMP result, String fileName, int radix)
		{

			if (fileName == null ) 
				throw new ArgumentNullException("fileName", 
					"Argument is null. Fully qualified path to file expected.");

			if (result == null ) 
				throw new ArgumentNullException("result", 
					"Argument is null. Expected valid FloatMP.");

#if NET_2_0
			if (fileName.IndexOfAny(Path.GetInvalidPathChars()) >= 0)
#else
			if (fileName.IndexOfAny(Path.InvalidPathChars) >= 0) 
#endif
				throw new ArgumentException("Invalid characters in path: " + fileName, "fileName");

			if (!File.Exists(fileName)) 
				throw new FileNotFoundException("Specified file does not exist.", fileName);

			if (radix < -36 || radix > 36 || (radix > -2 && radix < 2) )
				throw new ArgumentOutOfRangeException("radix",
					"Argument radix is out of range. Accepted values are from 2 to 36 or -36 to -2 inclusive.");

			IntPtr pf = IntPtr.Zero;
			try 
			{
				pf = Utilities.fopen(fileName, "rt");
				if (pf != IntPtr.Zero)
				{
					if (__gmpf_inp_str(result.number, pf, radix)== 0)
						throw new IOException("Cannot read file: " + fileName); 
				}
				else throw new System.IO.IOException("Could not open file: " + fileName);
			}
			finally 
			{
				if (pf != IntPtr.Zero) 
					if (Utilities.fclose(pf)!= 0) 
						throw new System.IO.IOException("Could not close file: " + fileName);
			}		
		}


		#endregion IO

		#region Miscallenous

		public static bool FitsUInt32 (FloatMP operand) 
		{
			if (__gmpf_fits_ulong_p(operand.number)!= 0 ) return true;
			else return false;
		}

		public static bool FitsInt32 (FloatMP operand) 
		{
			if (__gmpf_fits_slong_p(operand.number)!= 0 ) return true;
			else return false;
		}

		public static bool FitsUInt16 (FloatMP operand) 
		{
			if (__gmpf_fits_ushort_p(operand.number)!= 0 ) return true;
			else return false;
		}

		public static bool FitsInt16 (FloatMP operand) 
		{
			if (__gmpf_fits_sshort_p(operand.number)!= 0 ) return true;
			else return false;
		}

		public static void Ceil (FloatMP result, FloatMP operand) 
		{
			__gmpf_ceil(result.number, operand.number);
		}

		public static void Floor (FloatMP result, FloatMP operand) 
		{
			__gmpf_floor(result.number, operand.number);
		}

		/// <summary>
		/// Set result to operand rounded to an integer towards zero. 
		/// </summary>
		/// <param name="result"></param>
		/// <param name="operand"></param>
		public static void Trunc (FloatMP result, FloatMP operand) 
		{
			__gmpf_trunc(result.number, operand.number);
		}

		public static bool IsInteger (FloatMP operand) 
		{
			if (__gmpf_integer_p(operand.number) != 0) return true;
			else return false;
		}

		#endregion Miscallenous

		#endregion Static Methods

		#region Instance Methods

		public void Set (FloatMP value) 
		{
			__gmpf_set(this.number, value.number);
		}

		public void Set (uint value)
		{
			__gmpf_set_ui(this.number, value);
		}

		public void Set (int value) 
		{
			__gmpf_set_si(this.number, value);
		}

		public void Set (double value) 
		{
			__gmpf_set_d(this.number, value);
		}

		public void Set (IntMP value) 
		{
			__gmpf_set_z(this.number, value.number);
		}

		public void Set (RatnlMP value) 
		{
			__gmpf_set_q(this.number, value.number);
		}

		public void Set (String value)
		{
			__gmpf_set_str(this.number, value, -10);
		}

		public void Set (String value, int radix) 
		{
			if (radix < -36 || radix > 36 || (radix > -2 && radix < 2))
				throw new ArgumentOutOfRangeException("radix", radix,
					"Parameter radix is out of accepted range. Value should be between -36 and -2 or 2 and 36 inclusive.");
			__gmpf_set_str(this.number, value, radix);
		}

		#endregion Instance Methods

		#region Operators Overloads


		public static FloatMP operator +(FloatMP operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_add(result.number, operand1.number, operand2.number);
			return result;
		}

		public static FloatMP operator +(FloatMP operand1, uint operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_add_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static FloatMP operator +(uint operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_add_ui(result.number, operand2.number, operand1);
			return result;
		}

		public static FloatMP operator -(FloatMP operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_sub(result.number, operand1.number, operand2.number);
			return result;
		}

		public static FloatMP operator -(FloatMP operand1, uint operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_sub_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static FloatMP operator -(uint operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_ui_sub(result.number, operand1, operand2.number);
			return result;
		}

		public static FloatMP operator -(FloatMP operand) 
		{
			__gmpf_neg(operand.number, operand.number);
			return operand;
		}
		public static FloatMP operator *(FloatMP operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_mul(result.number, operand1.number, operand2.number);
			return result;
		}

		public static FloatMP operator *(FloatMP operand1, uint operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_mul_ui(result.number, operand1.number, operand2);
			return result;
		}

		public static FloatMP operator *(uint operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_mul_ui(result.number, operand2.number, operand1);
			return result;
		}

		public static FloatMP operator /(FloatMP operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_div(result.number, operand1.number, operand2.number);
			return result;
		}

		public static FloatMP operator /(uint operand1, FloatMP operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_ui_div(result.number, operand1, operand2.number);
			return result;
		}

		public static FloatMP operator /(FloatMP operand1, uint operand2)
		{
			FloatMP result = new FloatMP();
			__gmpf_div_ui(result.number, operand1.number, operand2);
			return result;
		}

		//
		// TODO: modulus operators are defined for double/float in C# - try getting them implemented here as well
		//

		public static bool operator <(FloatMP operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp(operand1.number, operand2.number) < 0) return true;
			else return false;
		}

		public static bool operator <(FloatMP operand1, int operand2)
		{
			if ( __gmpf_cmp_si(operand1.number, operand2) < 0) return true;
			else return false;
		}

		public static bool operator <(int operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_si(operand2.number, operand1) > 0) return true;
			else return false;
		}

		public static bool operator <(FloatMP operand1, uint operand2)
		{
			if ( __gmpf_cmp_ui(operand1.number, operand2) < 0) return true;
			else return false;
		}

		public static bool operator <(uint operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_ui(operand2.number, operand1) > 0) return true;
			else return false;
		}

		public static bool operator <(FloatMP operand1, double operand2)
		{
			if ( __gmpf_cmp_d(operand1.number, operand2) < 0) return true;
			else return false;
		}

		public static bool operator <(double operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_d(operand2.number, operand1) > 0) return true;
			else return false;
		}

		public static bool operator >(FloatMP operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp(operand1.number, operand2.number) > 0) return true;
			else return false;
		}

		public static bool operator >(FloatMP operand1, int operand2)
		{
			if ( __gmpf_cmp_si(operand1.number, operand2) > 0) return true;
			else return false;
		}

		public static bool operator >(int operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_si(operand2.number, operand1) < 0) return true;
			else return false;
		}

		public static bool operator >(FloatMP operand1, uint operand2)
		{
			if ( __gmpf_cmp_ui(operand1.number, operand2) > 0) return true;
			else return false;
		}

		public static bool operator >(uint operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_ui(operand2.number, operand1) < 0) return true;
			else return false;
		}

		public static bool operator >(FloatMP operand1, double operand2)
		{
			if ( __gmpf_cmp_d(operand1.number, operand2) > 0) return true;
			else return false;
		}

		public static bool operator >(double operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_d(operand2.number, operand1) < 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMP operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp(operand1.number, operand2.number) <= 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMP operand1, int operand2)
		{
			if ( __gmpf_cmp_si(operand1.number, operand2) <= 0) return true;
			else return false;
		}

		public static bool operator <=(int operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_si(operand2.number, operand1) >= 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMP operand1, uint operand2)
		{
			if ( __gmpf_cmp_ui(operand1.number, operand2) <= 0) return true;
			else return false;
		}

		public static bool operator <=(uint operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_ui(operand2.number, operand1) >= 0) return true;
			else return false;
		}

		public static bool operator <=(FloatMP operand1, double operand2)
		{
			if ( __gmpf_cmp_d(operand1.number, operand2) <= 0) return true;
			else return false;
		}

		public static bool operator <=(double operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_d(operand2.number, operand1) >= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMP operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp(operand1.number, operand2.number) >= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMP operand1, int operand2)
		{
			if ( __gmpf_cmp_si(operand1.number, operand2) >= 0) return true;
			else return false;
		}

		public static bool operator >=(int operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_si(operand2.number, operand1) <= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMP operand1, uint operand2)
		{
			if ( __gmpf_cmp_ui(operand1.number, operand2) >= 0) return true;
			else return false;
		}

		public static bool operator >=(uint operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_ui(operand2.number, operand1) <= 0) return true;
			else return false;
		}

		public static bool operator >=(FloatMP operand1, double operand2)
		{
			if ( __gmpf_cmp_d(operand1.number, operand2) >= 0) return true;
			else return false;
		}

		public static bool operator >=(double operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_d(operand2.number, operand1) <= 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMP operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp(operand1.number, operand2.number) == 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMP operand1, int operand2)
		{
			if ( __gmpf_cmp_si(operand1.number, operand2) == 0) return true;
			else return false;
		}

		public static bool operator ==(int operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_si(operand2.number, operand1) == 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMP operand1, uint operand2)
		{
			if ( __gmpf_cmp_ui(operand1.number, operand2) == 0) return true;
			else return false;
		}

		public static bool operator ==(uint operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_ui(operand2.number, operand1) == 0) return true;
			else return false;
		}

		public static bool operator ==(FloatMP operand1, double operand2)
		{
			if ( __gmpf_cmp_d(operand1.number, operand2) == 0) return true;
			else return false;
		}

		public static bool operator ==(double operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_d(operand2.number, operand1) == 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMP operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp(operand1.number, operand2.number) != 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMP operand1, int operand2)
		{
			if ( __gmpf_cmp_si(operand1.number, operand2) != 0) return true;
			else return false;
		}

		public static bool operator !=(int operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_si(operand2.number, operand1) != 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMP operand1, uint operand2)
		{
			if ( __gmpf_cmp_ui(operand1.number, operand2) != 0) return true;
			else return false;
		}

		public static bool operator !=(uint operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_ui(operand2.number, operand1) != 0) return true;
			else return false;
		}

		public static bool operator !=(FloatMP operand1, double operand2)
		{
			if ( __gmpf_cmp_d(operand1.number, operand2) != 0) return true;
			else return false;
		}

		public static bool operator !=(double operand1, FloatMP operand2)
		{
			if ( __gmpf_cmp_d(operand2.number, operand1) != 0) return true;
			else return false;
		}


		public static explicit operator FloatMP(bool operand) 
		{
			return new FloatMP(Convert.ToUInt32(operand));
		}

		public static explicit operator FloatMP(byte operand) 
		{
			return new FloatMP((uint)operand);
		}

		public static explicit operator FloatMP(sbyte operand) 
		{
			return new FloatMP((int)operand);
		}

		public static explicit operator FloatMP(char operand) 
		{
			return new FloatMP((uint)operand);
		}

		public static explicit operator FloatMP(short operand) 
		{
			return new FloatMP((int)operand);
		}

		public static explicit operator FloatMP(ushort operand) 
		{
			return new FloatMP((uint)operand);
		}

		public static explicit operator FloatMP(uint operand) 
		{
			return new FloatMP(operand);
		}

		public static explicit operator FloatMP(int operand) 
		{
			return new FloatMP(operand);
		}

		public static explicit operator FloatMP(float operand) 
		{
			return new FloatMP((double)operand);
		}

		public static explicit operator FloatMP(double operand) 
		{
			return new FloatMP(operand);
		}

		public static explicit operator FloatMP(long operand) 
		{
			// TODO : provide final implementation
			return new FloatMP(new IntMP(operand));
		}

		public static explicit operator FloatMP(ulong operand) 
		{
			// TODO : provide final implementation
			return new FloatMP(new IntMP(operand));
		}

		public static explicit operator FloatMP(decimal operand) 
		{
			// TODO : provide final implementation
			return new FloatMP((IntMP)operand);
		}

		public static explicit operator FloatMP(IntMP operand) 
		{
			return new FloatMP(operand);
		}

		public static explicit operator FloatMP(RatnlMP operand) 
		{
			return new FloatMP(operand);
		}


		public static explicit operator byte(FloatMP operand)
		{
			return ConvertMP.ToByte(operand);
		}

		public static explicit operator sbyte(FloatMP operand)
		{
			return ConvertMP.ToSByte(operand);
		}

		public static explicit operator char(FloatMP operand)
		{
			return ConvertMP.ToChar(operand);
		}

		public static explicit operator short(FloatMP operand)
		{
			return ConvertMP.ToInt16(operand);
		}

		public static explicit operator ushort(FloatMP operand)
		{
			return ConvertMP.ToUInt16(operand);
		}

		public static explicit operator uint(FloatMP operand)
		{
			return ConvertMP.ToUInt32(operand);
		}

		public static explicit operator int(FloatMP operand)
		{
			return ConvertMP.ToInt32(operand);
		}

		public static explicit operator float(FloatMP operand)
		{
			return ConvertMP.ToSingle(operand);
		}

		public static explicit operator double(FloatMP operand)
		{
			return ConvertMP.ToDouble(operand);
		}


		#endregion Operators Overloads

	}

	#endregion Class FloatMP

	#region Class mpf_t


	[StructLayout(LayoutKind.Sequential)]
	[Serializable]
	public class mpf_t : ISerializable
	{
		// Original implementation uses C int which size is dependant on 
		// processor / compiler combination.
		public int _mp_prec;	// Max precision, in number of `mp_limb_t's.
								// Set by mpf_init and modified by
								// mpf_set_prec.  The area pointed to by the
								// _mp_d field contains `prec' + 1 limbs.

		public int _mp_size;	// abs(_mp_size) is the number of limbs the
								// last field points to.  If _mp_size is
								// negative this is a negative number.

		public int _mp_exp;     // Exponent, in the base of `mp_limb_t'.

		public IntPtr _mp_d;	// Pointer to the limbs.

		public mpf_t (SerializationInfo info, StreamingContext context)
		{

			FloatMP.__gmpf_init(this);

			this._mp_prec = info.GetInt32("_mp_prec");
			this._mp_size = info.GetInt32("_mp_size");
			this._mp_exp = info.GetInt32("_mp_exp");
			int bitsPerLimb = info.GetInt32("BitsPerLimb");


			if (bitsPerLimb == 32) 
			{
				byte[] limbs = (byte[]) info.GetValue("_mp_d", typeof(byte[]));
				
					IntPtr bytes = Marshal.AllocHGlobal(limbs.Length);
					Array.Reverse(limbs);
					Marshal.Copy(limbs, 0, bytes, limbs.Length);
					Utilities.__gmpn_set_str(this._mp_d, bytes, (uint) limbs.Length, 256);

			}
			else if (bitsPerLimb == 64) 
			{
				long[] limbs = (long[]) info.GetValue("_mp_d", typeof(int[]));

					this._mp_d = Marshal.AllocHGlobal(limbs.Length * 8);
					Marshal.Copy(limbs, 0, this._mp_d, limbs.Length);
			}
			
		}

		public mpf_t () {}

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// Not very efficient implementation - to many data duplications
			// mpd is duplicating mpf_t limbs in managed space

			info.AddValue("_mp_prec", this._mp_prec);
			info.AddValue("_mp_size", this._mp_size);
			info.AddValue("_mp_exp", this._mp_exp);
			// We need to store it since remoting architectures may differ
			info.AddValue("BitsPerLimb", Utilities.BitsPerLimb);
			if (Utilities.BitsPerLimb == 32)
			{
				int size = System.Math.Abs(this._mp_size);
				IntPtr str = Marshal.AllocHGlobal(size * 4 + 1);
				Utilities.__gmpn_get_str(str, 256, this._mp_d, size);
				byte[] mpd = new byte[size * 4 + 1];
				Marshal.Copy(str, mpd, 0, mpd.Length);
				info.AddValue("_mp_d", mpd, typeof(byte[]));
			}
			else if (Utilities.BitsPerLimb == 64) 
			{
				byte[] mpd = new byte[System.Math.Abs(this._mp_size)*8];
				Marshal.Copy((IntPtr) this._mp_d, mpd, 0, mpd.Length);
				info.AddValue("_mp_d", mpd, typeof(byte[]));
			}
		}

		#endregion
	}

	#endregion Class mpf_t
}