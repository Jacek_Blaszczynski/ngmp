/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Text;

namespace NGmp.Math
{
	
	#region Class RandomMP
	
	/// <summary>
	/// Class RandomMP provides methods for generation of IntMP and FloatMP
	/// sequences of pseudo-random numbers.
	/// </summary>
	[Serializable]
	[SuppressUnmanagedCodeSecurity]
	public class RandomMP : IDisposable, ISerializable
	{

		#region Fields

		private bool disposed = false;
		private IntPtr state = IntPtr.Zero;

		#endregion Fields

		#region Constructors & Dispose Methods

		#region Constructors

		public RandomMP()
		{
			this.state = Marshal.AllocHGlobal(64);
			__gmp_randinit_default(this.state);
		}

		public RandomMP(IntMP seed) : this() 
		{
			__gmp_randseed(this.state, seed.number);
		}

		/// <summary>
		/// Initialize state for a linear congruential algorithm X = (a*X + c) mod 2^m2exp.. 
		/// a, c and m2exp are selected from a table, chosen so that size bits (or more) 
		/// of each X will be used, ie. m2exp/2 >= size. 	
		/// The maximum size currently supported is 128.
		/// </summary>
		/// <param name="size">Maximum suported value is 128.</param>
		public RandomMP(uint size) 
		{
			// Is it correct? - parhaps it should initialize to maximum of 128.
			if (size > 128) throw new ArgumentOutOfRangeException("size", size, 
												"Maximum size accepted is 128.");
			this.state = Marshal.AllocHGlobal(64);
			// Dispose if initialization failed or throw exception.
			if (__gmp_randinit_lc_2exp_size(this.state, size)== 0) this.Dispose();
		}

		public RandomMP(uint size, IntMP seed) : this(size)
		{
			__gmp_randseed(this.state, seed.number);
		}

		/// <summary>
		/// Initialize RandomMP with state using a linear congruential algorithm 
		/// X = (a*X + c) mod 2^m2exp. The low bits of X in this algorithm are not very random. 
		/// The least significant bit will have a period no more than 2, and the second bit 
		/// no more than 4, etc. For this reason only the high half of each X is actually used. 
		/// When a random number of more than m2exp/2 bits is to be generated, 
		/// multiple iterations of the recurrence are used and the results concatenated. 
		/// </summary>
		/// <param name="a"></param>
		/// <param name="c"></param>
		/// <param name="m2exp"></param>
		public RandomMP(IntMP a, uint c, uint m2exp) 
		{
			this.state = Marshal.AllocHGlobal(64);
			__gmp_randinit_lc_2exp(this.state, a.number, c, m2exp);
		}

		public RandomMP(IntMP a, uint c, uint m2exp, IntMP seed) : this(a, c, m2exp) 
		{
			__gmp_randseed(this.state, seed.number);
		}

		protected RandomMP(SerializationInfo info, StreamingContext context){}
		#endregion Constructors

		#region IDisposable Methods

		
		// Implement IDisposable.
		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			// Check to see if Dispose has already been called.
			if(!this.disposed)
			{
				// If disposing equals true, dispose all managed 
				// and unmanaged resources.
				if(disposing)
				{
					// Dispose managed resources.
				}
             
				// Call the appropriate methods to clean up 
				// unmanaged resources here.
				// If disposing is false, 
				// only the following code is executed.
				if (this.state != IntPtr.Zero) 
				{
					RandomMP.__gmp_randclear(this.state);
					Marshal.FreeHGlobal(this.state);
					this.state = IntPtr.Zero;       
				}
			}
			disposed = true;         
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~RandomMP()      
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}

		#endregion IDisposable Methods

		#endregion Constructors & Dispose Methods

		#region Properties

		public bool IsDisposed
		{
			get {return this.disposed;}
		}

		#endregion Properties

		#region GMP P/Invoke Prototypes

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_urandomb (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_urandomm (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpz_rrandomb (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			uint n);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This function is obsolete. Use gmpz_urandomb or __gmpz_urandomm instead.")]
		internal static extern void __gmpz_random (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint max_size);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		[Obsolete("This function is obsolete. Use gmpz_rrandomb instead.")]
		internal static extern void __gmpz_random2 (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			uint max_size);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmp_randinit_default (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr state);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmp_randinit_lc_2exp (
			[Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr a, 
			uint cc,
			uint exp);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern int __gmp_randinit_lc_2exp_size (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			uint size);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmp_randclear (
			[In, MarshalAs(UnmanagedType.SysInt)]  IntPtr state);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmp_randseed (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr seed);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmp_randseed_ui (
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			uint seed);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_urandomb (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			[In, Out,MarshalAs(UnmanagedType.SysInt)]  IntPtr state, 
			uint nbits);

		[DllImport(Utilities.mp_library, CallingConvention=CallingConvention.Cdecl)]
		internal static extern void __gmpf_random2 (
			[Out, MarshalAs(UnmanagedType.SysInt)]  IntPtr result, 
			int max_size, 
			int exp);

		#endregion GMP P/Invoke Prototypes

		#region Methods

		#region Statics

		public static void RandInit (System.IntPtr state) 
		{
			__gmp_randinit_default(state);
		}

		public static void RandInit (System.IntPtr state, IntMP a, uint c, uint exp) 
		{
			__gmp_randinit_lc_2exp(state, a.number, c, exp);
		}

		public static void RandInit (System.IntPtr state, uint size) 
		{
			__gmp_randinit_lc_2exp_size(state, size);
		}

		public static void RandClear (System.IntPtr state) 
		{
			__gmp_randclear(state);
		}

		public static void Random (IntMP result, System.IntPtr state, uint n) 
		{
			__gmpz_urandomb(result.number, state, n);
		}

		public static void Random (IntMP result, System.IntPtr state, IntMP n) 
		{
			__gmpz_urandomm(result.number, state, n.number);
		}

		public static void Random (FloatMP result, System.IntPtr state, uint bitsNumber)
		{
			__gmpf_urandomb(result.number, state, bitsNumber);
		}
		/// <summary>
		/// Method Random01 generates random number of the size uniformly distributed between
		/// 0 and 2 to power of n - 1 and with long strings of zeros and ones. This type of
		/// integers is useful in testing of functions and algorithms as it is proven that
		/// it is more likely to trigger corner case bugs.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="state"></param>
		/// <param name="n"></param>
		public static void RandomZerosAndOnes (IntMP result, System.IntPtr state, uint n) 
		{
			__gmpz_rrandomb(result.number, state, n);
		}

		/// <summary>
		/// Generate a random float of at most maxSize limbs, with long strings of zeros and ones 
		/// in the binary representation. The exponent of the number is in the interval -exp to exp 
		/// (in limbs). This function is useful for testing functions and algorithms, 
		/// since these kind of random numbers have proven to be more likely to trigger 
		/// corner-case bugs. Negative random numbers are generated when max_size is negative.
		/// </summary>
		/// <param name="result"></param>
		/// <param name="maxSize"></param>
		/// <param name="exp"></param>
		public static void RandomZerosAndOnes (FloatMP result, int maxSize, int exp)
		{
			__gmpf_random2(result.number, maxSize, exp);
		}
		#endregion Statics

		#region Instance

		public void Seed (IntMP seed) 
		{
			__gmp_randseed(this.state, seed.number);
		}

		public void Seed (uint seed) 
		{
			__gmp_randseed_ui(this.state, seed);
		}

		/// <summary>
		/// Generate a uniformly distributed pseudo-random integer IntMP 
		/// in the range 0 to 2^maxExp-1, inclusive.
		/// </summary>
		/// <param name="maxExp"></param>
		/// <returns>Psedu-random IntMP number.</returns>
		public IntMP Next(uint maxExp) 
		{
			IntMP result = new IntMP();
			__gmpz_urandomb(result.number, this.state, maxExp);
			return result;
		}

		/// <summary>
		/// Generate a uniformly distributed pseudo-random integer IntMP 
		/// in the range 0 to maxValue-1, inclusive.
		/// </summary>
		/// <param name="maxValue"></param>
		/// <returns>Psedu-random IntMP number.</returns>
		public IntMP Next(IntMP maxValue) 
		{
			IntMP result = new IntMP();
			__gmpz_urandomm(result.number, this.state, maxValue.number);
			return result;
		}

		/// <summary>
		/// Generate a uniformly distributed pseudo-random float FloatMP, 
		/// such that 1 > return >= 0, 
		/// with bitsNumber significant bits in the mantissa.
		/// </summary>
		/// <param name="bitsNumber">Number of bits in mantissa.</param>
		/// <returns>Pseudo-random FloatMP number.</returns>
		public FloatMP NextFloatMP(uint bitsNumber) 
		{
			FloatMP result = new FloatMP();
			result.Precision = bitsNumber;
			__gmpf_urandomb(result.number, this.state, bitsNumber);
			return result;
		}

		/// <summary>
		/// Generate a pseudo-random IntMP integer with long strings of zeros and ones 
		/// in the binary representation. Useful for testing functions and algorithms, 
		/// since this kind of random numbers have proven to be more likely 
		/// to trigger corner-case bugs. The random number will be in 
		/// the range 0 to 2^maxExp-1, inclusive. 
		/// </summary>
		/// <param name="maxExp"></param>
		/// <returns>Pseudo-random IntMP integer.</returns>
		public IntMP NextOnesAndZeros(uint maxExp) 
		{
			IntMP result = new IntMP();
			__gmpz_rrandomb(result.number, this.state, maxExp);
			return result;
		}

		/// <summary>
		/// Generate a pseudo-random FloatMP float of at most maxSize limbs, 
		/// with long strings of zeros and ones in the binary representation. 
		/// The exponent of the number is in the interval -exp to exp 
		/// (in limbs). This function is useful for testing functions and algorithms, 
		/// since these kind of random numbers have proven to be more likely to trigger 
		/// corner-case bugs. Negative random numbers are generated when max_size is negative.
		/// </summary>
		/// <param name="maxSize">Maximum size of random number expressed in limbs 
		/// (architecture dependent size of processor word).</param>
		/// <param name="exp"></param>
		/// <returns>Pseudo-random FloatMP number with long strings of zeros and ones.</returns>
		public FloatMP NextOnesAndZeros(int maxSize, int exp) 
		{
			FloatMP result = new FloatMP();
			__gmpf_random2 (result.number, maxSize, exp);
			return result;
		}

		#endregion Instance

		#endregion Methods

		#region ISerializable Members

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			// TODO:  Add RandomMP.GetObjectData implementation
		}

		#endregion
	}

	#endregion Class RandomMP

	#region Class RandState

	[StructLayout(LayoutKind.Explicit)]
	public class RandState 
	{
#if x86_64
		[FieldOffset(0)] public mpz_t _mp_seed;
		[FieldOffset(16)] public gmp_randalg_t _mp_alg;
		[FieldOffset(20)] public __gmp_randata_lc _mp_lc;
		[FieldOffset(20)] public __gmp_randata_lc _mp_algdata;
#else
		[FieldOffset(0)] public mpz_t _mp_seed;
		[FieldOffset(12)] public gmp_randalg_t _mp_alg;
		[FieldOffset(16)] public __gmp_randata_lc _mp_lc;
		[FieldOffset(16)] public __gmp_randata_lc _mp_algdata;
#endif

		public RandState () 
		{
			this._mp_algdata = new __gmp_randata_lc();
			this._mp_lc = new __gmp_randata_lc();
			this._mp_seed = new mpz_t();
		}
	}

	#endregion Class RandState

	#region Class __gmp_randata_lc
	
	[StructLayout(LayoutKind.Explicit)]
	public class __gmp_randata_lc 
	{
#if x86_64
		[FieldOffset(0)] public mpz_t _mp_a;		// Multiplier
		[FieldOffset(16)] public ulong _mp_c;		// Adder
		[FieldOffset(24)] public mpz_t _mp_m;		// Modulus (valid only if m2exp == 0)  
		[FieldOffset(40)] public ulong _mp_m2exp;	// If != 0, modulus is 2 ^ m2exp
#else
		[FieldOffset(0)] public mpz_t _mp_a;		// Multiplier
		[FieldOffset(12)] public uint _mp_c;		// Adder
		[FieldOffset(16)] public mpz_t _mp_m;		// Modulus (valid only if m2exp == 0)
		[FieldOffset(28)] public uint _mp_m2exp;	// If != 0, modulus is 2 ^ m2exp
#endif

		public __gmp_randata_lc()
		{
			this._mp_a = new mpz_t();
			this._mp_m = new mpz_t();
		}
	}

	//
	//	[StructLayout(LayoutKind.Explicit)]
	//	public class RandState 
	//	{
	//		// mpz_t == _mp_seed;
	//		[FieldOffset(0)] public int mp_alloc;
	//		[FieldOffset(4)] public int mp_size;
	//		[FieldOffset(8)] public System.UIntPtr mp_d;
	//		[FieldOffset(12)] public gmp_randalg_t _mp_alg;
	//		//		[FieldOffset(0)] public mpz_t _mp_a;		// Multiplier
	//		[FieldOffset(16)] public int mp_alloc_a;
	//		[FieldOffset(20)] public int mp_size_a;
	//		[FieldOffset(24)] public System.UIntPtr mp_d_a;
	//
	//		[FieldOffset(28)] public uint _mp_c;		// Adder
	//
	//		//		[FieldOffset(16)] public mpz_t _mp_m;		// Modulus (valid only if m2exp == 0)
	//		[FieldOffset(32)] public int mp_alloc_m;
	//		[FieldOffset(36)] public int mp_size_m;
	//		[FieldOffset(40)] public System.UIntPtr mp_d_m;
	//
	//		[FieldOffset(44)] public uint _mp_m2exp;	// If != 0, modulus is 2 ^ m2exp
	//		[FieldOffset(48)] public __gmp_randata_lc _mp_lc;
	//		[FieldOffset(48)] public __gmp_randata_lc _mp_algdata;
	//
	//		public RandState () 
	//		{
	//			this._mp_algdata = new __gmp_randata_lc();
	//			this._mp_lc = new __gmp_randata_lc();
	//			//			this._mp_seed = new mpz_t();
	//		}
	//		#region IConvertibleMP Members
	//
	//		#endregion
	//	}


	#endregion Class __gmp_randata_lc

	#region enum gmp_randalg_t

	public enum gmp_randalg_t 
	{
		GMP_RAND_ALG_DEFAULT = 0,
		GMP_RAND_ALG_LC = GMP_RAND_ALG_DEFAULT
	}

	#endregion enum gmp_randalg_t

}
