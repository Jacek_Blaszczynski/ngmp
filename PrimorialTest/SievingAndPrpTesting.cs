/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.IO;
using NGmp.Math;
using NGmp.Math.Prime;

namespace PrimorialTest
{
	/// <summary>
	/// Summary description for SievingAndPrpTesting.
	/// </summary>
	public class SievingAndPrpTesting
	{

		#region Fields

		private ulong[] trialDivisionTiming = new ulong[50];
		private ulong[] prpTestTiming = new ulong[50];
		private uint[] divisionSuccess = new uint[50]; 

		#endregion Fields


		public SievingAndPrpTesting()
		{
		}



	}
}
