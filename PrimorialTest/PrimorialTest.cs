/* 
 
Copyright 2004 - 2006 Jacek Blaszczynski.

This file is part of the NGmp Library.

The NGmp Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; version 2.1 of the License.

The NGmp Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the NGmp Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA. 
 
 */

using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Threading;
using NGmp.Math;
using NGmp.Math.Prime;

namespace PerformanceTests
{
	/// <summary>
	/// PrimorialTest is used to test function timings for IntMP implementation.
	/// </summary>
	class PrimorialTest
	{

		#region Fields

		static double frequency = 0;
		static FileStream fs = null;
		static StreamWriter sw = null;

		static FileStream fsProve = null;
		static StreamWriter swProve = null;

		static uint maxPrime = 0;
		static uint firstPrimorial = 0;
		static uint maxPrimorial = 0;

		static bool testIntMP = true;
		static bool testBigInteger;
		static bool testGCD;
		static bool testNextPrime;
		static bool testRandom;
		static bool onlyTrialDiv;


		#endregion Fields

		#region Main and Command Line Parsing

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			if (args.Length > 1) 
			{

				try 
				{

					// Parse command line - if there are errors return;
					if (!ParseCommandLine(ref args)) return;

					Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.BelowNormal;
					Thread.CurrentThread.Priority = ThreadPriority.Lowest;

					fs = File.Open(Environment.CurrentDirectory + @"/PerformanceTest" + firstPrimorial + ".txt",
						FileMode.Append, FileAccess.Write, FileShare.None);

					sw = new StreamWriter(fs);

					fsProve = File.Open(Environment.CurrentDirectory 
						+ @"/PerformanceTestProve" + firstPrimorial + "-" + maxPrimorial + ".txt",
						FileMode.Append, FileAccess.Write, FileShare.None);

					swProve = new StreamWriter(fsProve);

					frequency = Utilities.GetProcessorFrequency(0);

					double start, end;

					start = Utilities.GetProcessorTicks();
					end = Utilities.GetProcessorTicks();

					Console.WriteLine("Calibration of timing methods: processor frequncy {0}, cycles to measure ticks {1}", frequency, end - start); 

					uint[] primes = GetPrimes(maxPrime);

					//for (int k = 0; primes[k] <= firstPrimorial ; k++ ) Console.WriteLine("{0}, {1}", k, primes[k]);

					Console.WriteLine("Generated {0} primes with largest prime {1}", primes.Length, primes[primes.Length-1]);

					if (testIntMP) 
					{
						Console.WriteLine("\nIntMP implementation tests:\n\n");
						GetPrimorial(firstPrimorial, maxPrimorial, ref primes, false);
					}

					if (testBigInteger) 
					{
						Console.WriteLine("\nMono.BigInteger implementation tests:\n\n");
						GetPrimorialBI(firstPrimorial, maxPrimorial, ref primes, false);
					}

					sw.WriteLine();
					swProve.WriteLine();
				}
				catch (Exception e) 
				{
					Console.WriteLine(e.GetType().ToString() + "\n" + e.Message + "\n" + e.ToString());
					Console.ReadLine();
				}
				finally
				{
					if (sw != null) sw.Close();
					if (fs != null) fs.Close();
					if (swProve != null) swProve.Close();
					if (fsProve != null) fsProve.Close();
				}
			}
			// Console.ReadLine();
		}

		static bool ParseCommandLine(ref string[] args) 
		{
			if (args.Length == 0) return false;
			else 
			{
				for (int i = 0; i < args.Length; i++) 
				{
					string[] command = null;
					if (args[i].IndexOf(":") > 0) command = args[i].Split(new char[] {':'}, 2);
					if (args[i].StartsWith(@"/maxprime")) maxPrime = UInt32.Parse(command[1]);
					if (args[i].StartsWith(@"/firstprim")) firstPrimorial = UInt32.Parse(command[1]);
					if (args[i].StartsWith(@"/lastprim")) maxPrimorial = UInt32.Parse(command[1]);
					if (args[i].StartsWith(@"/random")) testRandom = true;
					if (args[i].StartsWith(@"/gcd")) testGCD = true;
					if (args[i].StartsWith(@"/nextprime")) testNextPrime = true;
					if (args[i].StartsWith(@"/IntMP=No")) testIntMP = false;
					if (args[i].StartsWith(@"/BI=Yes")) testBigInteger = true;
					if (args[i].StartsWith(@"/onlydiv")) onlyTrialDiv = true;

				}
				return true;
			}
		}


		#endregion Main and Command Line Parsing

		#region NGmp.Math Tests

		/// <summary>
		/// Implementation of Erasthotenes sieve.
		/// </summary>
		/// <param name="maxValue">Max value of prime to be included.</param>
		static uint[] GetPrimes(uint maxValue) 
		{
			
			BitArray primes = new BitArray((int) maxValue);

			uint maxSearch = (uint) Math.Ceiling(Math.Sqrt(maxValue));

			for (uint i = 2; i <= maxSearch; i++) 
			{
				if (primes[(int)i] == false) 
				{
					for (uint j =  2*i; j < maxValue ; j += i) 
					{
						primes[(int)j] = true;
					}
				}
			}

			ArrayList prim = new ArrayList();

			// Prime gaps investigation
//			int maxdiff = int.MinValue;
//			IntMP avgdiff = new IntMP();
//			uint lastMaxDiff = 0;

			for (uint k = 2; k < primes.Length; k++) 
			{
				if (primes[(int)k] == false) 
				{
					prim.Add(k);

					// Code for finding max gaps between primes
//					if (k > 2) 
//					{
//						int diff = (int) ((uint)prim[prim.Count-1] - (uint)prim[prim.Count -2]);
//						IntMP.Add (avgdiff, avgdiff, (uint) diff);
//						if ( diff > maxdiff) 
//						maxdiff =(int) ((uint)prim[prim.Count-1] - (uint)prim[prim.Count -2]);
//						lastMaxDiff = (uint)prim[prim.Count-1];
//					}
				}
			}

//			Console.WriteLine("max difference between primes was: {0} at prime value {1}", maxdiff, lastMaxDiff);
//			IntMP.DivT(avgdiff, avgdiff, (uint) (prim.Count-1) );
//			Console.WriteLine("Average Diff between primes : {0}", avgdiff.ToString());

			return (uint[]) prim.ToArray(typeof(uint));
		}

		/// <summary>
		/// Implementation of Erasthotenes sieve with primorials approximate digit counter.
		/// </summary>
		/// <param name="maxValue">Max value of prime to be included.</param>
		static uint[] GetPrimes(out double[] digitCounter, uint maxValue) 
		{
			
			byte[] primes = new byte[maxValue];

			uint maxSearch = (uint) Math.Ceiling(Math.Sqrt(maxValue));

			for (uint i = 2; i <= maxSearch; i++) 
			{
				if (primes[i] == 0) 
				{
					for (uint j =  2*i; j < maxValue ; j += i) 
					{
						primes[j] = 1;
					}
				}
			}

			ArrayList prim = new ArrayList();
			ArrayList dig = new ArrayList();
			double digitCount = 0;

			for (uint k = 2; k < primes.Length; k++) 
			{
				if (primes[k] == 0) 
				{
					prim.Add(k);
					digitCount += Math.Log10(k);
					dig.Add(digitCount);
				}
			}
			digitCounter = (double[]) dig.ToArray(typeof(double));
			return (uint[]) prim.ToArray(typeof(uint));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="primes"></param>
		/// <param name="provePrimality"></param>
		static void GetPrimorial(uint start, uint end, ref uint[] primes, bool provePrimality) 
		{
			// Some preparation for memory savings
			int size = Array.BinarySearch(primes, start);
			if (size < 0) 
			{
				size = ~size;
				if (size > primes.Length) size = primes.Length;
				else if (size == 0) return;
			}

			IntMP first = null;

			if (size > 0) 
			{

				IntMP[] primorials = new IntMP[size+1];
				for (int k = 0; k < size+1 ; k++) primorials[k] = new IntMP(primes[k]);

				double startTicks, endTicks;
				startTicks = Utilities.GetProcessorTicks();
			
				int prim1Index = BalancedMul(0 , size, ref primes, ref primorials);

				endTicks = Utilities.GetProcessorTicks();
				sw.Write((endTicks - startTicks) + ",");

				Console.WriteLine("We got primorial number {0}# which is {1} digits long.", 
					primes[size], IntMP.SizeInBase(primorials[size], 10));
				Console.WriteLine("Calculation took: {0} processor cycles.", endTicks - startTicks);
				Console.WriteLine("Calculation took: {0} ms.", ((endTicks - startTicks)/frequency*1000));

				first = (IntMP) primorials[prim1Index].Clone();

				// We refrain from diposing IntMP for small values in BalancedMul so do it now!
				foreach (IntMP p in primorials) if (!p.IsDisposed) p.Dispose();

			}
			else first = new IntMP(primes[0]);

			TestPrimorials(start, end, first, ref primes, true);
			if (testGCD) TestGCD(first, (uint) (size-1), ref primes);
			if (testRandom) TestRandom(first);
			if (testNextPrime) TestNextPrime(first);
			first.Dispose();
		}

		/// <summary>
		/// Method BalancedMul calculates product of all array values between 
		/// start and end index keeping operands size as close as possible.
		/// This is shortcut solution as unrolling it in for loop instead of using recurence 
		/// would be more efficient with removal of function calling overhead.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="primes"></param>
		/// <param name="primorials"></param>
		/// <returns></returns>
		static int BalancedMul (int start, int end, ref uint[] primes, ref IntMP[] primorials) 
		{
			int length = end - start + 1;

			if (length > 2) 
			{
				int quotient, result1, result2;
				quotient = length / 2;
				result1 = BalancedMul(start, start + quotient, ref primes, ref primorials);
				result2 = BalancedMul(start+ quotient + 1, end, ref primes, ref primorials);
				IntMP.Mul(primorials[result2], primorials[result2], primorials[result1]);
				//primorials[result1].Dispose();
				return result2;
			}
			else 
			{
				if (length == 1) return start;
				else 
				{
					IntMP.Mul(primorials[end], primorials[end], primorials[start]);
					//primorials[start].Dispose();
					return end;
				}
			}
		}

		static void TestPrimorials (uint start, uint end, IntMP first, ref uint[] primes, bool plusOne) 
		{

			if (first.IsDisposed) throw new Exception("Object already disposed.");

			int startIndex = 0;
			int endIndex = primes.Length -1;
			double startTick1, startTick2, startTick3, endTick1, endTick2, endTick3;

			if (start > 2) 
			{
				// Some preparation for calculations
				startIndex = Array.BinarySearch(primes, start);
				if (startIndex < 0) 
				{
					startIndex = ~startIndex;
					if (startIndex > primes.Length) return;
					else if (startIndex == 0) return;
				}
			}
			else startIndex = 0;

			if (end <= primes[primes.Length-1]) 
			{
				endIndex = Array.BinarySearch(primes, end);
				if (endIndex < 0) 
				{
					endIndex = ~endIndex;
					if (endIndex > primes.Length) return;
					else if (endIndex == 0) return;
					else endIndex--;
				}
			}

			IntMP plusOneP = new IntMP();
			IntMP test = new IntMP();
			IntMP numBase = new IntMP(2);
			uint factor = 0;

			startTick1 = Utilities.GetProcessorTicks();

			for (uint i = (uint) startIndex; i <= endIndex ; i++) 
			{

				test.Set(1);

				startTick2 = Utilities.GetProcessorTicks();

				if (plusOne) IntMP.Add(plusOneP, first, 1);
				else IntMP.Sub(plusOneP, first, 1);

				startTick3 = Utilities.GetProcessorTicks();

				bool divisible = TrialDivision(out factor, plusOneP, i, ref primes);

				endTick3 = Utilities.GetProcessorTicks();

				if (divisible) 
				{
					test.Set(5);
				}
				else 
				{

					//if (i > 0) IntMP.DivExact(numBase, first, primes[i]);
                
					//int result = IntMP.ProbablyPrime(plusOneP, 1);

					if (!onlyTrialDiv) IntMP.PowMod(test, numBase, first, plusOneP);
				}

				endTick2 = Utilities.GetProcessorTicks();
				swProve.WriteLine(primes[i] + " ," + (endTick2 - startTick2) + " ," + IntMP.SizeInBase(plusOneP, 2));

//				if (result == 2) Console.WriteLine("Primorial prime {0}# + 1 found: digits {1}", primes[i], IntMP.SizeInBase(first, 10));
//				else if (result == 1) ProvePrimality(i, plusOneP, ref primes);
//				else Console.WriteLine("Primorial number {0}# + 1 is not a prime", primes[i]);

				if (test != 1) 
				{
					Console.Write("{0}\t{1}# + 1 not prime by ", i, primes[i]);
					if (divisible) Console.Write("trial div\t factor\t {0}", factor);
					else Console.Write("2-PRP test\t\t");
				}
				else if (onlyTrialDiv) Console.Write("{0}\t{1}# + 1 passed trial div \t\t\t", i, primes[i]);
				else if (plusOne) ProvePrimality(i, plusOneP, ref primes);

				double time = (double)(endTick3 - startTick3)*100 /( endTick2 - startTick2);
				Console.WriteLine("\t trial div {0} % of PowMod time", time.ToString("G4"));

				IntMP.Mul(first, first, primes[i+1]);
				
			}

			endTick1 = Utilities.GetProcessorTicks();

			swProve.WriteLine("Total time: " + (endTick1 - startTick1));

			plusOneP.Dispose();
			test.Dispose();
			numBase.Dispose();

		}

		static bool ProvePrimality (uint index, IntMP probPrime, ref uint[] primes) 
		{
			Console.Write(
				"{0}\t{1}# + 1 in primality proving\t\t\t", index, primes[index]);

			// Use 

			return true;

		}

		static bool TrialDivision (out uint factor, IntMP testedNumber, uint primorialIndex, ref uint[] primes) 
		{
			factor = 0;
			if (primorialIndex < 20) return false;

//			if (IntMP.FitsUInt32(testedNumber)) 
//			{
//				uint interm = ConvertMP.ToUInt32(testedNumber);
//				interm = Math.Ceiling(Math.Sqrt(interm));
//				if (!primes[primorialIndex] < interm) ;
//			}
//			else 
//			{
//				IntMP sqrtP = new IntMP();
//				IntMP remainder = new IntMP();
//				IntMP.Sqrt(sqrtP, remainder, testedNumber);
//				IntMP.Add(sqrtP, sqrtP, remainder);
//			}

			// Create intelligent algorithm for calculating sieving thresholds 
			// based on estimated PRP timings and probability to find a factor.

			// Currently we use empirical formula giving around 5 - 8 % sieving time
			// out of total testing time - which was tested to be optimal in ranges with
			// primorials up to 5000# - needs further testing - formula is based 
			// on upper bound O(log n) to pow 3 for PowMod and O(n) for trial division
			// with n being number of digits of primorial. 
			double index = Math.Pow(primorialIndex, 2) * 0.1;
			uint endIndex = index < primes.Length ? (uint) index : (uint) primes.Length;
			
			Console.Write("div to: {0}\t", primes[endIndex-1]);

			for (uint i = primorialIndex + 1; i < endIndex; i++) 
			{
				if (!IntMP.Divisible(testedNumber, primes[i])) continue;
				//Console.WriteLine("Factor of {0} primorial found: {1}", primes[primorialIndex], primes[i]);
				factor = primes[i];
				return true;
			}
			return false;
		}
		static void TestGCD (IntMP first, uint index, ref uint[] primes) 
		{
			double start, end;
			double avg = 0;
			IntMP test = new IntMP();
			IntMP result = new IntMP();

			for (int i = 0; i < index && i < 100 ; i++) 
			{
				IntMP.DivExact (test, first, primes[index]);
				start = Utilities.GetProcessorTicks();
				IntMP.GreatestCommonDivisor(result, first, test);
				end = Utilities.GetProcessorTicks();
				if (i > 5) avg += (end - start);
			}
			avg /= (index+1);
			Console.WriteLine("GCD found in {0} us and {1} cycles - size {2} bits", 1000000*avg/frequency, avg, IntMP.SizeInBase(result, 2));
		}

		static void TestRandom(IntMP first) 
		{
			double start, end;
			double avg;
			start = Utilities.GetProcessorTicks();
			RandomMP rand = new RandomMP(128, first);
			IntMP randN = rand.Next(first);
			end = Utilities.GetProcessorTicks();
			Console.WriteLine("RandomMP generation in {0} us {1} cycles - size {2} bits", (end - start) * 1000000 / frequency, end-start, IntMP.SizeInBase(randN, 2));
		}

		static void TestNextPrime(IntMP first) 
		{
			double start, end;
			double avg;
			start = Utilities.GetProcessorTicks();
			IntMP randN = new IntMP();
			IntMP.NextPrime(randN, first);
			end = Utilities.GetProcessorTicks();
			Console.WriteLine("NextPrime generation in {0} us {1} cycles - size {2} bits", (end - start) * 1000000 / frequency, end-start, IntMP.SizeInBase(randN, 2));
		}


		#endregion NGmp.Math Tests

		#region Mono.Math Tests

		/// <summary>
		/// 
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="primes"></param>
		/// <param name="provePrimality"></param>
		static void GetPrimorialBI(uint start, uint end, ref uint[] primes, bool provePrimality) 
		{
			// Some preparation for memory savings
			int size = Array.BinarySearch(primes, start);
			if (size < 0) 
			{
				size = ~size;
				if (size > primes.Length) size = primes.Length;
				else if (size == 0) return;
			}

			BigInteger first = null;

			if (size > 0) 
			{

				BigInteger[] primorials = new BigInteger[size];
				for (int k = 0; k < size ; k++) primorials[k] = new BigInteger(primes[k]);

				double startTicks, endTicks;
				startTicks = Utilities.GetProcessorTicks();

				int prim1Index = BalancedMulBI(0 , size - 1, ref primes, ref primorials);

				endTicks = Utilities.GetProcessorTicks();
				sw.Write((endTicks - startTicks) + ",");

				Console.WriteLine("We got primorial number {0}# using BigInteger class", primes[size-1]);
				Console.WriteLine("Calculation took: {0} processor cycles.", endTicks - startTicks);
				Console.WriteLine("Calculation took: {0} ms.", (endTicks - startTicks)/frequency*1000);
				first = primorials[prim1Index];

			}
			else first = new BigInteger(2);

			TestPrimorialsBI(start, end, first, ref primes, true);
			if (testGCD) TestGCDBI(first, (uint) (size -1), ref primes);
			if (testRandom) TestRandomBI(first);
			if (testNextPrime) TestNextPrimeBI(first);
		}

		static int BalancedMulBI (int start, int end, ref uint[] primes, ref BigInteger[] primorials) 
		{
			int length = end - start + 1;

			if (length > 2) 
			{
				int quotient, result1, result2;
				quotient = length / 2;
				result1 = BalancedMulBI(start, start + quotient, ref primes, ref primorials);
				result2 = BalancedMulBI(start+ quotient + 1, end, ref primes, ref primorials);
				primorials[result2] = BigInteger.Multiply(primorials[result2], primorials[result1]);
				primorials[result1] = null;
				return result2;
			}
			else 
			{
				if (length == 1) return start;
				else 
				{
					primorials[end] = BigInteger.Multiply(primorials[end], primorials[start]);
					primorials[start] = null;
					return end;
				}
			}
		}

		/// <summary>
		/// We test candidates using Fermat's Little Theorem
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="first"></param>
		/// <param name="primes"></param>
		/// <param name="plusOne"></param>
		static void TestPrimorialsBI (uint start, uint end, BigInteger first, ref uint[] primes, bool plusOne) 
		{

			//if (first.IsDisposed) throw new Exception("Object already disposed.");

			int startIndex = 0;
			int endIndex = primes.Length -1;
			double startTick1, startTick2, endTick1, endTick2;

			if (start > 2) 
			{
				// Some preparation for calculations
				startIndex = Array.BinarySearch(primes, start);
				if (startIndex < 0) 
				{
					startIndex = ~startIndex;
					if (startIndex > primes.Length) return;
					else if (startIndex == 0) return;
				}
			}
			else startIndex = 0;

			if (end <= primes[primes.Length-1]) 
			{
				endIndex = Array.BinarySearch(primes, end);
				if (endIndex < 0) 
				{
					endIndex = ~endIndex;
					if (endIndex > primes.Length) return;
					else if (endIndex == 0) return;
					else endIndex --;
				}
			}

			BigInteger plusOneP = new BigInteger();
			BigInteger test = new BigInteger();
			BigInteger numBase = new BigInteger(2);

			swProve.WriteLine("\nBigInteger tests\n");

			startTick1 = Utilities.GetProcessorTicks();

			for (uint i = (uint) startIndex; i <= endIndex ; i++) 
			{

				startTick2 = Utilities.GetProcessorTicks();

				if (plusOne) plusOneP = BigInteger.Add(first, 1);
				else plusOneP = BigInteger.Subtract(first, 1);

				if (i > 0) numBase = BigInteger.Divid(first, primes[i]);

				// Bugs in BigInteger - cannot use IsProbablePrime and other tests
				
				// result = plusOneP.IsProbablePrime();
				// result = PrimalityTests.SmallPrimeSppTest(plusOneP, ConfidenceFactor.ExtraLow);
				
				// Fermat test
				test = numBase.ModPow(first, plusOneP);

				endTick2 = Utilities.GetProcessorTicks();

				swProve.WriteLine(primes[i] + " ," + (endTick2 - startTick2));

				if (test != 1)
					Console.WriteLine("Primorial number {0}# + 1 is not a prime", primes[i]);
				else ProvePrimalityBI(i, plusOneP, ref primes);

				first = BigInteger.Multiply(first, primes[i+1]);
			}



			endTick1 = Utilities.GetProcessorTicks();

			swProve.WriteLine("Total time: " + (endTick1 - startTick1));

		}

		static bool ProvePrimalityBI (uint index, BigInteger probPrime, ref uint[] primes) 
		{
			Console.WriteLine("Primorial number {0}# + 1 was transferred to primality proving function.", primes[index]);
			return true;
		}

		static void TestGCDBI (BigInteger first, uint index, ref uint[] primes) 
		{
			double start, end;
			double avg = 0;
			BigInteger test = new BigInteger();
			BigInteger result = new BigInteger();

			for (int i = 0; i < index && i < 100 ; i++) 
			{
				test = BigInteger.Divid (first, primes[index]);
				start = Utilities.GetProcessorTicks();
				result = test.GCD(first);
				end = Utilities.GetProcessorTicks();
				if (i > 5) avg += (end - start);
			}
			avg /= (index+1);
			Console.WriteLine("GCD BigInteger found in {0} us and {1} cycles - size {2} bits", 1000000*avg/frequency, avg, result.BitCount());
		}


		static void TestRandomBI(BigInteger first) 
		{
			double start, end;
			double avg;
			start = Utilities.GetProcessorTicks();

			BigInteger randN = BigInteger.GenerateRandom(first.BitCount());
			end = Utilities.GetProcessorTicks();
			Console.WriteLine("Random BigInteger generation in {0} us {1} cycles - size {2} bits", (end - start) * 1000000 / frequency, end-start, randN.BitCount());
		}

		static void TestNextPrimeBI(BigInteger first) 
		{
			double start, end;
			double avg;
			start = Utilities.GetProcessorTicks();
			BigInteger randN = BigInteger.NextHighestPrime(first);
			end = Utilities.GetProcessorTicks();
			Console.WriteLine("BigInteger.NextHighestPrime generation in {0} us {1} cycles - size {2} bits", (end - start) * 1000000 / frequency, end-start, randN.BitCount());
		}


		#endregion Mono.Math Tests

	}
}
